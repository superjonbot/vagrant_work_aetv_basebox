<?php
#Environment-specific system settings go here.

$databases = array (
    'default' =>
    array (
        'default' =>
        array (
            'database' => 'aetv',
            'username' => 'root',
            'password' => 'admLn**',
            'host' => 'localhost',
            'port' => '',
            'driver' => 'mysql',
            'prefix' => '',
        ),
        'slave' =>
        array(
            'driver' => 'mysql',
            'database' => 'aetv',
            'username' => 'root',
            'password' => 'admLn**',
            'host' => 'localhost',
        ),
    ),
);

#
$conf['memcache_servers'] = array(
    'localhost:19111' => 'default',
    'localhost:19112' => 'cluster2',
    'localhost:19113' => 'cluster3',
    'localhost:19114' => 'cluster4',
    'localhost:19115' => 'cluster5',
    'localhost:19116' => 'cluster6',
    'localhost:19117' => 'cluster7',
    'localhost:19118' => 'cluster8',
    'localhost:19119' => 'cluster9',
);