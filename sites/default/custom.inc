<?php
#Custom environment-specific settings for custom or contrib module support.

# Error Reporting Level. Remove NOTICE's and DEPRECATED
# ini_set('error_reporting', E_ALL ^ E_DEPRECATED ^ E_NOTICE);

#Fixes a warning on our servers
date_default_timezone_set('America/New_York');

# Increase curl timeout.  Default was 15 sec
$conf['http_request_timeout'] = 900;   // 900 sec
# max number of nodes to be updated
//$conf['video_mpx_outbound_feed_max_num_node'] = 400;   // 400

# $base_url_prod
$conf['prod_base_url'] = 'http://www.aetv.com/';
#inhibit the write back to mpx.
$conf['video_mpx_outbound_feed_enable_mpx_update_default'] = false;

#analytics Confituration
$conf['comscore_c2'] = '3005002';
$conf['comscore_c4'] = '6357325';
$conf['nielsen_clientid'] = 'us-901285';
$conf['nielsen_vcid'] = 'c01';
# PDK Configuration
$conf['pdk_server_base'] = 'devservicesaetn-a.akamaihd.net';
$conf['pdk_server_host'] = 'devservicesaetn-a.akamaihd.net';
$conf['pdk_aetndigital_server_base'] = 'devplayer.aetndigital.com';
$conf['pdk_base_path'] = "/pdk5/pdk5.1.3/";
$conf['pdk_web_dir'] = "web/pdk_5.3.4_beta/";
$conf['pdk_adobe_auth_url'] = 'entitlement.auth-staging.adobe.com';
$conf['pdk_flash_release_url_params'] = 'mbr=true&assetTypes=medium_video_s3&metafile=true&switch=hds';
$conf['pdk_html5_release_url_params'] = 'mbr=true&assetTypes=medium_video_s3&metafile=false&switch=hls';
$conf['adobe_pass_policy'] = '21169';
$conf['fw_flash_section_id'] = 'aetn.aetv.flash.video';
$conf['fw_html5_section_id'] = 'aetn.aetv.video';

# thePlatform VMS accounts
$conf['mpx_video_feed_account'] = array(
    'username' => 'aetvingest1',
    'password' => 'Tv4w0men!',
    // 'network_account_number' => '2200212264',   //  Here goes the network account number "
    // 'network_account_title'  => 'AETN-AETV VMS',
    'network_account_number' => '2200221629',   //  Here goes the network account number "
    'network_account_title'  => 'AETN-AETV Dev VMS',
    );

# Tealium Analytics
$conf['tealium_js_file_location'] = '//tags.tiqcdn.com/utag/aenetworks/aetv/dev/utag.js'; // DEV
// $conf['tealium_js_file_location'] = '//tags.tiqcdn.com/utag/aenetworks/aetv/qa/utag.js';  // QA
// $conf['tealium_js_file_location'] = '//tags.tiqcdn.com/utag/aenetworks/aetv/prod/utag.js';  // PROD

$conf['tealium_sync_js_file_location'] = '//tags.tiqcdn.com/utag/aenetworks/aetv/dev/utag.sync.js'; //DEV
//$conf['tealium_sync_js_file_location'] = '//tags.tiqcdn.com/utag/aenetworks/aetv/qa/utag.sync.js'; //QA
//$conf['tealium_sync_js_file_location'] = '//tags.tiqcdn.com/utag/aenetworks/aetv/prod/utag.sync.js'; //PROD

# Hot Moments
$conf['the_platform_feed'] = 'http://feed.theplatform.com/f/vHOuAC/YtBEQrteO9wZ/';


#Arktan API
$conf['arktan_api_endpoint'] = 'http://cloud53.arktan.com/arktan/api/stream';
$conf['arktan_api_appkey'] = 'dev.aetv';
$conf['arktan_source_name'] = 'www.aetv.com';
$conf['arktan_source_uri'] = 'www.aetv.com';
$conf['arktan_busname'] = 'arktanlogin12';
$conf['arktan_janrain_appname'] = 'arktanlogin12';
$conf['arktan_identity_url'] = 'https://arktanlogin05.rpxnow.com/openid/embed?flags=stay_in_window,no_immediate&token_url=http%3A%2F%2Fsandbox.arktan.com%2Farkjs%2Fwaiting.html&bp_channel=';


$conf['arktan_default_mosaic_streamId'] = 'http://www.aetv.com/shows/id_The_First_48_Repeat_May_23_9PM_Down_in_Overtown';


#Google Search
$conf['google_cse_id'] = '017408630766155012344:vykxfb1fyya';

#Facebook App ID
$conf['fb_app_id'] = '131830536853850';

#Live Streaming
$conf['live_streaming_tppdk_js'] = 'devplayer.aetndigital.com/pservice/player/aetn.video.pdk/web/pdk/tpPdk.js';
$conf['live_streaming_live_js'] = 'devplayer.aetndigital.com/pservice/player/aetn.video.player/js/aetn.video.aetv.live.js';

$conf['watch_app_promo_js'] = "http://devplayer.aetndigital.com/pservice/aetn.services.js/interstitial.js";

$conf["adobePass_page_js"] ='http://devplayer.aetndigital.com/pservice/player/aetn.video.player/js/aetn/video/tve/aetn.aetv.adobepass.page.js';
$conf['adobePass_AESwfUrl'] = 'https://entitlement.auth-staging.adobe.com/entitlement/AccessEnabler.swf';

$conf['quiz_engine_base_url'] = 'http://dev.quiz.aetndigital.com';
$conf['quiz_engine_quiz_template'] = 'http://dev.quiz.aetndigital.com/profiles/aetn_services_quiz/libraries/ae_quiz_fe/templates/ae_quiz_template.tpl';
$conf['quiz_engine_question_template'] = 'http://dev.quiz.aetndigital.com/profiles/aetn_services_quiz/libraries/ae_quiz_fe/templates/ae_question_template.tpl';


$conf['awssdk2_access_key'] = 'AKIAIUQHHAVH37IAOE6Q';
$conf['awssdk2_secret_key'] = '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';
#$conf['aws_account_id'] = '...';
#$conf['aws_canonical_id'] = '...';

