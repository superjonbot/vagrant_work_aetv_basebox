//for Exclusive for Interactives content type, hide/show appropriate fields
jQuery(function () {
  if (
    jQuery('body.node-type-exclusive-for-interactives').length < 1 && 
    jQuery('body.page-node-add-exclusive-for-interactives').length < 1
  ) {
    return;
  }

  var fields = [,
    //flash
    [
      jQuery('#edit-field-interactive-filename'), 
      jQuery('#edit-field-embed-code-override')
    ],
    //html5
    [
      jQuery('#edit-field-open-content')
    ],
    //iframe
    [
      jQuery('#edit-field-iframe-url')
    ]
  ];
  var intTypeSelect = jQuery('#edit-field-interactive-type-und');

  //for the edit page, pre-pop the right set of fields
  showFields(intTypeSelect.val());

  intTypeSelect.bind('change', function () {
    showFields(jQuery(this).val());
  });

  function resetFields() {
    //reset all first
    for (var i=1; i<fields.length; i++) {
      for (var j=0; j<fields[i].length; j++) {
        fields[i][j].hide();
      }
    }
  }

  function showFields(num) {
    resetFields();
    jQuery(fields[num]).each(function (i) {
      //now show relevant
      jQuery(this).show(); 
    });
  }
});;
(function ($) {

    /**
     * @todo Undocumented Code!
     */
    $.extend({
        getUrlVars: function () {
          var vars = [], hash;
          var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
          for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
          }
          return vars;
        },
        getUrlVar: function (name) {
          return $.getUrlVars()[name];
        }
    });

    Drupal.gigya = Drupal.gigya || {};

    Drupal.gigya.hideLogin = function () {
      $('#user-login').hide();
    }

    /**
     * @todo Undocumented Code!
     */
    Drupal.gigya.logoutResponse = function (response) {
      if (response['status'] == 'OK') {
        document.getElementById('logoutMessage').innerHTML = "Successfully logged out, redirecting";
        window.location = Drupal.settings.gigya.logoutLocation;
      }
    };

    Drupal.gigya.addEmail = function (email) {
      if (typeof Drupal.gigya.toPost !== 'undefined') {
        Drupal.gigya.toPost.user.email = email;
        Drupal.gigya.login(Drupal.gigya.toPost);
      }
    }

    Drupal.gigya.login = function (post) {
      var base = post.context.id;
      var element_settings = {};
      element_settings.url = '/socialize-login';
      element_settings.event = 'gigyaLogin';
      var ajax = new Drupal.ajax(base, $('#' + post.context.id), element_settings);
      ajax.options.data = post;
      $(ajax.element).trigger('gigyaLogin');
    }

    /**
     * @todo Undocumented Code!
     */
    Drupal.gigya.loginCallback = function (response) {
      Drupal.gigya.toPost = response;
      if ((response.user.email.length === 0) && (response.user.isSiteUID !== true)) {
        var html = '<div class="form-item form-type-textfield form-item-email"><div class="description">Additional information is required in order to complete your registeration. Please fill-in the following info:</div><label for="email" style="float: none;">Email <span class="form-required" title="This field is required.">*</span></label><input type="text" id="email" name="email" value="" size="20" maxlength="60" class="form-text required"><button type="button" class="button" onClick="Drupal.gigya.addEmail(jQuery(this).prev().val())">' + Drupal.t('Submit') + '</button></div>';
        Drupal.CTools.Modal.show();
        $('#modal-title').html('Please fill-in missing details');
        $('#modal-content').html(html);
        return false;
      }
      if(response.provider !== 'site' ) {
        Drupal.gigya.login(response);
      }
    };

    /**
     * Callback for the getUserInfo function.
     *
     * Takes the getUserInfo object and renders the HTML to display an array
     * of the user object
     *
     * TODO: probably should be removed in production, since its just for dumping
     * user output.
     */
    Drupal.gigya.getUserInfoCallback = function (response) {
      if (response.status == 'OK') {
        var user = response['user'];
        // Variable which will hold property values.
        var str="<pre>";
        for (prop in user) {
          if (prop == 'birthYear' && user[prop] == 2009) {
            user[prop] = '';
          }
          if (prop == 'identities') {
            for (ident in user[prop]) {
              for (provide in user[prop][ident]) {
                str+=provide + " SUBvalue :"+ user[prop][ident][provide]+"\n";
              }
            }
          }
          // Concate prop and its value from object.
          str+=prop + " value :"+ user[prop]+"\n";
        }
        str+="</pre>";

        document.getElementById('userinfo').innerHTML = str;
      }
    };

    /**
     * @todo Undocumented Code!
     */
    Drupal.gigya.showAddConnectionsUI = function (connectUIParams) {
      gigya.services.socialize.showAddConnectionsUI(connectUIParams);
    };

    /**
     * @todo Undocumented Code!
     */
    Drupal.gigya.notifyLoginCallback = function (response) {
      if (response['status'] == 'OK') {
        setTimeout("$.get(Drupal.settings.basePath + 'socialize-ajax/notify-login')", 1000);
      }
    };


    /**
     * @todo Undocumented Code!
     */
    Drupal.gigya.initResponse = function (response) {
      if (null != response.user) {
        if (response.user.UID != Drupal.settings.gigya.notifyLoginParams.siteUID || !response.user.isLoggedIn) {
          gigya.services.socialize.notifyLogin(Drupal.settings.gigya.notifyLoginParams);
        }
      }
    }
    /**
     * this function checks if the user is looged in at gigya if so it renders the Gamification Plugin
     */
    Drupal.gigya.gmInit = function (response) {
      if (response.errorCode === 0) {
        if (typeof response.UID !== 'undefined') {
          Drupal.gigya.gmRender();
        }
      }
      return false;
    };

    /**
     * function that renders the Gamification Plugin
     */
    Drupal.gigya.gmRender = function () {
      $.each(Drupal.settings.gigyaGM, function(key, block) {
        $.each(block, function(name, params) {
          switch(name) {
          case 'challengeStatusParams':
            gigya.gm.showChallengeStatusUI(params);
            break;
          case 'userStatusParams':
            gigya.gm.showUserStatusUI(params);
            break;
          case 'achievementsParams':
            gigya.gm.showAchievementsUI(params);
            break;
          case 'leaderboardParams':
            gigya.gm.showLeaderboardUI(params);
            break;
          }
        });
      });
    }
    /**
     * this function checks if the user is looged in at gigya if so it renders the Gamification notification Plugin
     */
    Drupal.gigya.gmNotiInit = function (response) {
      if (response.errorCode === 0) {
        if (typeof response.UID !== 'undefined') {
          gigya.gm.showNotifications();
        }
      }
      return false;
    };


})(jQuery);

;
(function ($) {
    /**
     * @todo Undocumented Code!
     */
    Drupal.behaviors.gigyaNotifyFriends = {
      attach: function(context, settings) {
        if (typeof gigya !== 'undefined') {
          $('.friends-ui:not(.gigyaNotifyFriends-processed)', context).addClass('gigyaNotifyFriends-processed').each(
            function () {
              gigya.services.socialize.getUserInfo({callback:Drupal.gigya.notifyFriendsCallback});
              gigya.services.socialize.addEventHandlers({ onConnect:Drupal.gigya.notifyFriendsCallback, onDisconnect:Drupal.gigya.notifyFriendsCallback});
            }
          );
        }
      }
    };

    /**
     * @todo Undocumented Code!
     */
    Drupal.behaviors.gigyaInit = {
      attach: function(context, settings) {
        if (typeof gigya !== 'undefined') {
          if (typeof Drupal.settings.gigya.notifyLoginParams !== 'undefined') {
            Drupal.settings.gigya.notifyLoginParams.callback = Drupal.gigya.notifyLoginCallback;
            gigya.services.socialize.getUserInfo({callback: Drupal.gigya.initResponse});
          }

          // Attach event handlers.
          gigya.socialize.addEventHandlers({onLogin:Drupal.gigya.loginCallback});

          // Display LoginUI if necessary.
          if (typeof Drupal.settings.gigya.loginUIParams !== 'undefined') {
            $.each(Drupal.settings.gigya.loginUIParams, function (index, value) {
              value.context = {id: value.containerID};
              gigya.services.socialize.showLoginUI(value);
            });
          }

          // Display ConnectUI if necessary.
          if (typeof Drupal.settings.gigya.connectUIParams !== 'undefined') {
            gigya.services.socialize.showAddConnectionsUI(Drupal.settings.gigya.connectUIParams);
          }

          // Call ShareUI if it exists.
          if (typeof Drupal.settings.gigya.shareUIParams !== 'undefined') {
            //build a media object
            var mediaObj = {type: 'image', href: Drupal.settings.gigya.shareUIParams.linkBack};
            if ((Drupal.settings.gigya.shareUIParams.imageBhev === 'url') && (Drupal.settings.gigya.shareUIParams.imageUrl !== '')) {
              mediaObj.src = Drupal.settings.gigya.shareUIParams.imageUrl;
            }
            else if (Drupal.settings.gigya.shareUIParams.imageBhev === 'default') {
              if ($('meta[property=og:image]').length > 0) {
                mediaObj.src = $('meta[property=og:image]').attr('content');
              }
              else {
                mediaObj.src = $('#block-system-main img').eq(0).attr('src') || $('img').eq(0).attr('src');
              }
            }
            else {
              mediaObj.src = $('#block-system-main img').eq(0).attr('src') || $('img').eq(0).attr('src');
            }
            // Step 1: Construct a UserAction object and fill it with data.
            var ua = new gigya.services.socialize.UserAction();
            if (typeof Drupal.settings.gigya.shareUIParams.linkBack !== 'undefined') {
              ua.setLinkBack(Drupal.settings.gigya.shareUIParams.linkBack);
            }
            if (typeof Drupal.settings.gigya.shareUIParams.title !== 'undefined') {
              ua.setTitle(Drupal.settings.gigya.shareUIParams.title);
            }
            if (typeof Drupal.settings.gigya.shareUIParams.description !== 'undefined') {
              ua.setDescription(Drupal.settings.gigya.shareUIParams.description);
            }
            ua.addMediaItem(mediaObj);
            var params = {};
            if (typeof Drupal.settings.gigya.shareUIParams.extraParams !== 'undefined') {
              params = Drupal.settings.gigya.shareUIParams.extraParams;
            }
            params.userAction = ua;
            gigya.services.socialize.showShareUI(params);
          }
        }
      }
    };

    /**
     * @todo Undocumented Code!
     */
    Drupal.behaviors.gigyaLogut = {
      attach: function (context, settings) {
        if (typeof gigya !== 'undefined') {
          if ($.cookie('Drupal.visitor.gigya') == 'gigyaLogOut') {
            gigya.services.socialize.logout();
            $.cookie('Drupal.visitor.gigya', null);
          }
        }
      }
    };
    /**
     * initiate Gamification
     */

    Drupal.behaviors.gamification = {
      attach: function (context, settings) {
        if (typeof gigya !== 'undefined') {
          if (typeof Drupal.settings.gigyaGM !== 'undefined'){
            gigya.services.socialize.getUserInfo({callback: Drupal.gigya.gmInit});
          }
        }
      }
    };
    Drupal.behaviors.GMnotifications = {
      attach: function (context, settings) {
        if (typeof gigya !== 'undefined') {
          if (typeof Drupal.settings.gigyaGMnotification !== 'undefined'){
            gigya.services.socialize.getUserInfo({callback: Drupal.gigya.gmNotiInit});
          }
        }
      }
    };
    /**
     * initiate Activity Feed
     */
    Drupal.behaviors.gigyaActivityFeed = {
      attach: function (context, settings) {
        if (typeof gigya !== 'undefined') {
          if (typeof Drupal.settings.gigyaActivityFeed !== 'undefined'){
            gigya.socialize.showFeedUI(Drupal.settings.gigyaActivityFeed);
          }
        }
      }
    };
    Drupal.behaviors.addGigyaUidToForm = {
      attach: function (context, settings) {
        if (typeof gigya !== 'undefined') {
          if ($('#gigya-link-accounts-form .gigyaUid').length > 0 ) {
            $('#gigya-link-accounts-form .gigyaUid').val(Drupal.gigya.toPost.user.UID);
          }
          if ($('#modal-content form .gigyaUid').length > 0) {
            $('#modal-content form .gigyaUid').val(Drupal.gigya.toPost.user.UID);
          }
          $('#modal-content form .close-modal').once().click( function (e) {
            e.preventDefault();
            Drupal.CTools.Modal.dismiss();
          });
        }
      }
    };
    Drupal.behaviors.gigyaFillRegForm = {
      attach: function (context, settings) {
        if (typeof gigya !== 'undefined') {
          if ($('.modal-content #user-register-form').length > 0 ) {
            $('.modal-content #user-register-form input:[name=mail]').val(Drupal.gigya.toPost.user.email);
            $('.modal-content #user-register-form input:[name=name]').val(Drupal.gigya.toPost.user.email);
          }
        }
      }
    };



})(jQuery);

;
(function ($) {

/**
 * A progressbar object. Initialized with the given id. Must be inserted into
 * the DOM afterwards through progressBar.element.
 *
 * method is the function which will perform the HTTP request to get the
 * progress bar state. Either "GET" or "POST".
 *
 * e.g. pb = new progressBar('myProgressBar');
 *      some_element.appendChild(pb.element);
 */
Drupal.progressBar = function (id, updateCallback, method, errorCallback) {
  var pb = this;
  this.id = id;
  this.method = method || 'GET';
  this.updateCallback = updateCallback;
  this.errorCallback = errorCallback;

  // The WAI-ARIA setting aria-live="polite" will announce changes after users
  // have completed their current activity and not interrupt the screen reader.
  this.element = $('<div class="progress" aria-live="polite"></div>').attr('id', id);
  this.element.html('<div class="bar"><div class="filled"></div></div>' +
                    '<div class="percentage"></div>' +
                    '<div class="message">&nbsp;</div>');
};

/**
 * Set the percentage and status message for the progressbar.
 */
Drupal.progressBar.prototype.setProgress = function (percentage, message) {
  if (percentage >= 0 && percentage <= 100) {
    $('div.filled', this.element).css('width', percentage + '%');
    $('div.percentage', this.element).html(percentage + '%');
  }
  $('div.message', this.element).html(message);
  if (this.updateCallback) {
    this.updateCallback(percentage, message, this);
  }
};

/**
 * Start monitoring progress via Ajax.
 */
Drupal.progressBar.prototype.startMonitoring = function (uri, delay) {
  this.delay = delay;
  this.uri = uri;
  this.sendPing();
};

/**
 * Stop monitoring progress via Ajax.
 */
Drupal.progressBar.prototype.stopMonitoring = function () {
  clearTimeout(this.timer);
  // This allows monitoring to be stopped from within the callback.
  this.uri = null;
};

/**
 * Request progress data from server.
 */
Drupal.progressBar.prototype.sendPing = function () {
  if (this.timer) {
    clearTimeout(this.timer);
  }
  if (this.uri) {
    var pb = this;
    // When doing a post request, you need non-null data. Otherwise a
    // HTTP 411 or HTTP 406 (with Apache mod_security) error may result.
    $.ajax({
      type: this.method,
      url: this.uri,
      data: '',
      dataType: 'json',
      success: function (progress) {
        // Display errors.
        if (progress.status == 0) {
          pb.displayError(progress.data);
          return;
        }
        // Update display.
        pb.setProgress(progress.percentage, progress.message);
        // Schedule next timer.
        pb.timer = setTimeout(function () { pb.sendPing(); }, pb.delay);
      },
      error: function (xmlhttp) {
        pb.displayError(Drupal.ajaxError(xmlhttp, pb.uri));
      }
    });
  }
};

/**
 * Display errors on the page.
 */
Drupal.progressBar.prototype.displayError = function (string) {
  var error = $('<div class="messages error"></div>').html(string);
  $(this.element).before(error).hide();

  if (this.errorCallback) {
    this.errorCallback(this);
  }
};

})(jQuery);
;
(function($) {

/**
 * Create a DART object to handle tagging functionality
 */
Drupal.DART = {};

/**
 * Overridable settings.
 */
Drupal.DART.settings = {
  "writeTags": true
};

/**
 * Using document.write, add a DART tag to the page
 */
Drupal.DART.tag = function(tag) {
  tag = typeof(tag) == 'string' ? eval('(' + tag + ')') : tag;

  var tagname = tag.settings.options.method == 'adj' ? 'script' : 'iframe';
  var options = tag.settings.options.method == 'adj' ? 'type="text/javascript"' : 'frameborder="0" scrolling="no" width="' + tag.sz.split("x")[0] + '" height="' + tag.sz.split(",")[0].split("x")[1] + '"';

  // Allow other modules to include js that can manipulate the tag object.
  var processed_tag = ($ !== undefined) ? $(document).triggerHandler('dart_tag_process', [tag]) : undefined;
  tag = processed_tag !== undefined ? processed_tag : tag;

  ad = '<' + tagname + ' ' + options + ' src="';
  ad += dart_url + "/";
  ad += tag.network_id !== '' ? tag.network_id + "/" : "";
  ad += tag.settings.options.method + "/";
  ad += tag.prefix + '.' + tag.site + "/" + tag.zone + ";";
  ad += this.keyVals(tag.key_vals);

  // Allow other modules to include js that can manipulate the concatenated tag string.
  rendered_ad = ($ !== undefined) ? $(document).triggerHandler('dart_tag_render', [ad]) : undefined;
  ad = rendered_ad !== undefined ? rendered_ad : ad; ad += '"></' + tagname + '>';

  if (Drupal.DART.settings.writeTags) {
    document.write(ad);
  }

  // console.log('-----------------'+tag.pos+'------------------');
  // console.log(tag);

  return ad;
};

/**
 * Format a key|val pair into a dart tag key|val pair.
 */
Drupal.DART.keyVal = function(key, val, useEval) {
  if (key != "<none>") {
    kvp  = key + "=";
    kvp += useEval ? eval(val) : val;
    kvp += key == "ord" ? "?" : ";";
  }
  else {
    kvp = useEval ? eval(val) : val;
  }

  return(kvp);
};

/**
 * Loop through an object and create kay|val pairs.
 *
 * @param vals
 *   an object in this form:
 *   {
 *     key1 : {{val:'foo', eval:true}, {val:'foo2', eval:false}}
 *     key2 : {{val:'bar', eval:false}},
 *     key3 : {{val:'foobar', eval:true}}
 *   }
 */
Drupal.DART.keyVals = function(vals) {
  var ad = '';
  for(var key in vals) {
    value = vals[key];
    for(var val in value) {
      v = value[val];
      ad += this.keyVal(key, v['val'], v['eval']);
    }
  }
  return ad;
};


/**
 * If there are tags in the loadLastTags, then load them where they belong.
 */
Drupal.DART.display_ads = function () {
    //alert(JSON.stringify(Drupal.DART.settings.loadLastTags));
  ord = Math.round(Math.random()*1000000000000);
    if (typeof(Drupal.DART.settings.loadLastTags) == 'object') {
    $('.dart-tag.dart-processed').each(function () {
      $(this).removeClass('dart-processed');
      $(this).html('');
    });
    var init = false;
    for (var tag in Drupal.DART.settings.loadLastTags) {
        // variables for background ads may be defined in late loaded scripts. Load bg ad if needed.
      if (Drupal.DART.settings.loadLastTags.hasOwnProperty(tag) && tag != null) {
        (function(tag) {
          var name = tag;
          var scriptTag = Drupal.DART.tag(Drupal.DART.settings.loadLastTags[name]);
          if (typeof(postscribe) == 'function') {
            postscribe($('.dart-name-' + name), scriptTag, function () {
              Drupal.DART.loadBgAd(Drupal.settings.DART.bgAdVars);
              $('.dart-name-' + name).addClass('dart-processed');
            });
          }
          else if (typeof(_this.writeCapture) == 'function') {
            $('.dart-name-' + name).writeCapture().append(scriptTag, function () {
                Drupal.DART.loadBgAd(Drupal.settings.DART.bgAdVars);
            }).addClass('dart-processed');
          }
        }(tag));
      }
    }
  }
}

/**
 * Load the background ad as served by DART.
 */
Drupal.DART.loadBgAd = function(bgAdVars) {
  //ensure ads are loaded only once on the page
  if (!Drupal.DART.settings.bgAdLoaded && typeof bgAdVars != 'undefined') {
    var bgAdCSS = {};
    if (window[bgAdVars.bgImg] != undefined) {
      bgAdCSS['background-image'] = 'url(' + window[bgAdVars.bgImg] + ')';
    }
    if (window[bgAdVars.bgColor] != undefined) {
      bgAdCSS['background-color'] = window[bgAdVars.bgColor];
    }
    if (window[bgAdVars.bgRepeat] != undefined) {
      bgAdCSS['background-repeat'] = window[bgAdVars.bgRepeat];
    }
    $(bgAdVars.selector).css(bgAdCSS);

    if (window[bgAdVars.clickThru] != undefined) {
      $(bgAdVars.selector).addClass('background-ad');
      $(bgAdVars.selector).click(function (e) {
      if(e.target != this) return;
        window.open(window[bgAdVars.clickThru]);
      });
    }

    //don't try to load again
    if (window[bgAdVars.bgImg] != undefined) {
      Drupal.DART.settings.bgAdLoaded = true;
    }
  }
};



/**
 * Display Ads.
 */
Drupal.behaviors.DART = {
  attach: function(context) {
    Drupal.DART.display_ads();
    Drupal.DART.loadBgAd(Drupal.settings.DART.bgAdVars);
  }
};

})(jQuery);

;
