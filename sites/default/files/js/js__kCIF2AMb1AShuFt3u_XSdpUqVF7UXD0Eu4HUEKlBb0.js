 /**
  * aetn.site module responsible for calculating layout, pjax injections ....add
  * more here
  *
  */

/*
 * console fix for IE or other browsers.
 *
 */
if ( ! window.console || ! window.console.log) {

    (function() {
      var names = ["log", "debug", "info", "warn", "error",
          "assert", "dir", "dirxml", "group", "groupEnd", "time",
          "timeEnd", "count", "trace", "profile", "profileEnd"],
          i, l = names.length;

      window.console = {};

      for ( i = 0; i < l; i++ ) {
        window.console[ names[i] ] = function() {};
      }
    }());
}

var aetv = aetv || {};

aetv.site = (function (module, $) {

    var footerHeight,
      headerHeight,
      container,
      theWindow,
      wrapper;

    module.containerWidth = null;
    module.containerHeight = null;
    module.loadTouch = Modernizr.touch;

    jQuery(function () {
      init();
      onResize();
      module.scheduler = new Scheduler();
      module.globalNav = new GlobalNav();
    });

    $LAB.setGlobalDefaults({
        Debug: true
    });


    module.showLoader = function() {
      jQuery("#wrapper").append(jQuery('<div class="loader-holder"></div>'));
    }

    module.hideLoader = function() {
      jQuery(".loader-holder").remove();
    }

    var init = function () {
      //check for ios7 ipad - flag to help fix 20px bug
      Modernizr.addTest('ios7_ipad', function () {
        return RegExp("iPad; CPU OS \[7-8]").test(navigator.userAgent);
      });

      jQuery('body').bind("pjax:click", pjaxClicked);
      jQuery('body').bind("pjax:start", pjaxStarted);
      jQuery('body').bind("pjax:end", pjaxEnded);
      jQuery('body').bind("pjax:success", pjaxSuccess);
      jQuery('body').bind("pjax:error", pjaxError);

      jQuery(document).pjax("a[data-pjax-link-container], [data-pjax-link-container] a", "#container", {
       fragment: "#container"
      });

      jQuery(document).pjax("a[data-pjax-link-innerpage], [data-pjax-link-innerpage] a", ".inner-container", {
        fragment: ".inner-container"
      });

      jQuery(".modal-close-btn").click(function () {
        jQuery.modal.close();
      });


      footerHeight = jQuery("#footer").outerHeight();
      headerHeight = jQuery("#site_nav").outerHeight();
      container = jQuery("#container");
      theWindow = jQuery(window);
      wrapper = jQuery("#wrapper");

      jQuery(window).resize(function () {
        onResize();
      });

      /** Reload the page when using the back-button if it was loaded via pjax. */
      jQuery(window).on('pjax:popstate', function(e) {
        if (typeof window.stop == 'function'){
          window.stop();
        } else {
          try{
            document.execCommand('Stop');
          }
          catch(e) {}
        }
        _.defer(function () {
          window.location.reload();
        });
      });

      if (!_.isUndefined(aetv.innerPage) && _.isFunction(aetv.innerPage.loadScripts)) {
        aetv.site.showLoader();

        _.delay(function () {
          aetv.innerPage.loadScripts(false);
        }, 100);

      } else {
        //console.warn("loadInnerScripts function is not defined or is not a function");
        return;
      }

      jQuery(document).ready(function () {
        if(Drupal.settings.watch_app_promo_js){
          jQuery.getScript( Drupal.settings.watch_app_promo_js );
        }
        updateBodyTags();
      });

    }

    function updateBodyTags() {
      jQuery('body').removeClass(function (index, css) {
        return (css.match (/\baetv_showBranding_[^ ]*\b/g) || []).join(' ');
      });
      $css_file_link = jQuery('head link[data-css-custom]');

      if ( (typeof Drupal.settings.aetv_show == 'object' ) && Drupal.settings.aetv_show.css_file){
        if ($css_file_link.length > 0){
          $css_file_link.attr('href', Drupal.settings.aetv_show.css_file);
        } else {
          jQuery("head").append(
              jQuery('<link data-css-custom="custom">').attr('type',
              'text/css').attr('rel', 'stylesheet').attr(
                  'href', Drupal.settings.aetv_show.css_file));
        }
      } else if($css_file_link.length > 0) {
        $css_file_link.remove();
      }
      var innerWrap = jQuery(".inner-wrapper");
      var container = jQuery("#container");
      var classesToAdd = [];
      if (innerWrap.length){
        var classList = innerWrap.attr("class");
        if (typeof classList == "string"){
          classesToAdd = classesToAdd.concat(classList.split(" "));
        }
      }
      if (container.length){
        var classList = container.attr("class");
        if (typeof classList == "string"){
          classesToAdd = classesToAdd.concat(classList.split(" ") );
        }
      }
      var re = /aetv_showBranding_/;

      for (var i=0; i < classesToAdd.length; i++){
        if (re.test(classesToAdd[i]) && !(jQuery("body").hasClass(classesToAdd[i]) ) ){
          jQuery("body").addClass(classesToAdd[i]);
        }
      }
      _.delay(function() {
        jQuery(window).trigger('resize');
      },750);
      _.delay(function() {
        jQuery(window).trigger('resize');
      },1500);

    }

    var onResize = function () {

      // save the width and height values to be globally accessible
      module.containerWidth = theWindow.width();
      module.containerHeight = theWindow.height() - (footerHeight + headerHeight);

      if (!Modernizr.csscalc) {
        // force the width and height of the container
        container.height(module.containerHeight);
        container.width(module.containerWidth);
      } else {
         // else fallback to css calc
      }
    }

    var pjaxClicked = function (event, options) {
       aetv.innerPage.animateOut(options.container, options.target);
       aetv.innerPage.destroy();
    }

    var pjaxStarted = function (event, xhr, options) {
       (function ($) {
           Drupal.detachBehaviors(jQuery(options.target), Drupal.settings);
       })(jQuery);
       //console.log("pjax started");
       //_.defer(function() {
       //aetv.innerPage.animateOut(event.target, options.target);
       //aetv.innerPage.destroy();
       //});
    }

    var pjaxEnded = function (event, xhr, options) {
       //console.log("pjax ended");
       // slideContainerIn();
    }

    var pjaxError = function (event, xhr, textStatus, errorThrown, options) {
       //console.log(event);
       //console.log(xhr, textStatus, errorThrown, options);
    }
    /*
    * PJax success Returns event, data, status, xhr, options We need to
    * re-parse the data for the scripts tags to append.
    *
    */
    var pjaxSuccess = function (event, data, status, xhr, options) {

       var $data = jQuery(data);
       var $container = jQuery(event.target);
       /**
        * Load in the settings and module scripts.
        *
        */

       if (options.fragment === '#container') {
           $data.filter("script[data-pjax-script-include-container]").appendTo($container);
       }
       $data.filter("script[data-pjax-extra-scripts]").appendTo($container);
       $data.filter("script#settings-script").appendTo($container);
       $data.filter("script#module-script").appendTo($container);
       $data.filter("script[data-tealium-vars]").appendTo($container);
       /* Now get the dart ad blocks */
       $dartTag = $data.filter('script.dartTag');

       $dartDiv = jQuery(".dart-tag", $container);
       /* reset the tile and ord */
       window.tile = 1;
       window.ord = 1000000000 + Math.floor(Math.random() * 900000000);
       Drupal.DART.settings.loadLastTags = {};

       $dartDiv.each(function (index, e) {
           jQuery(this).append($dartTag[index]);
       });

       (function ($) {
           Drupal.attachBehaviors($container, Drupal.settings);
       })(jQuery);


       _.delay(function () {

           aetv.innerPage.animateIn(event.target);
           aetv.innerPage.loadScripts(event.target);
           if (typeof utag !== 'undefined' && typeof utag.view === 'function') {
             utag_data = utag_data || {};
             utag_data["dom.referrer"] = eval("document." + "referrer");
             utag_data["dom.title"] = "" + document.title;
             utag_data["dom.domain"] = "" + location.hostname;
             utag_data["dom.query_string"] = "" + (location.search).substring(1);
             utag_data["dom.url"] = "" + document.URL;
             utag_data["dom.pathname"] = "" + location.pathname;
               utag.view(utag_data);
           }
           updateBodyTags();
       }, 100);
    }

  return module;

 }(aetv.site || {}, jQuery));
;
aetv.innerPage = (function(module, $) {

	/*
	 ** method animateIn()
	 ** called by pjax on pjax end event
	 */
	module.animateIn = function(target) {
		//console.log("animateIn start: ", target);
		var cont = $(target)[0];
		var whereTo = 0;
		jQuery(cont).css({
			"visibility" : ""
		});
		jQuery(cont).css({
			"opacity" : ""
		});
		TweenLite.to(cont, 0, {
			left : whereTo,
			ease : Strong.easeOut,
			overwrite: "all"
		});
		//console.log("animateIn end", target);		
	};
	/*
	 ** method animateOut()
	 ** called by pjax on pjax start event
	 */
	module.animateOut = function(target, source) {
		//jQuery('.menuItem').removeClass('active');
		if (window.aetv.aetvModal && window.aetv.aetvModal.modalVisible) {
			aetv.aetvModal.closeModal();
		}
		var cont = $(target)[0];
		var direction = module.transitionDirection;
		var whereTo = 0;

		if (direction === "left")
			whereTo = -4000;
		else
			whereTo = 4000;

		TweenLite.to(cont, 20, {
			left : whereTo,
			ease : Strong.easeOut,
			useFrames : true
		});

		_.delay(function() {

			aetv.site.showLoader();

		}, 500);
	};

	module.destroy = function() {

	};
	return module;
}(aetv.innerPage || {}, jQuery));
;


/*
 * 
 * Anatoliy Gorskiy, 2013.
 *
 * fastScroller is a custom horizontal scroller which uses GPU transforms when available.
 * It responds to defined right and left buttons, touch, and mouse wheel events.
 * You can pass callbacks to capture the change in state of the scroller.
 * 
 * dependencies: jQuery, modernizr, underscore, firmin.js and/or tweenLite.js
 * best used in a require.js module or inside lab.js wait callback
 * 
 * example use:
 * 
 * var onBounce = function(e){
 *     console.log("bounce "+e.direction+" / "+e.currentTarget+" / "+e.type);
 *    //logs: bounce right / [object HTMLDivElement] / click 
 *    //NOTE: e.direction is opposite of direction of the button - 
 *    //right button makes the page scroll left
 * }
 * var onEndTrigged = function(e){
 *     //logs: bounce right / [object HTMLDivElement] / click 
 * }
 * 
 * var scrollConfig = {        
 *     divToScroll      : $(".episode"), //jQuery object of a div to scroll. if not scrolled by means off css matrix transforms, element must be positioned relative
 *     parent           : "parent", // only "parent" is supported now, it must have a width defined implicidly or explicidly
 *     leap             : 300, //leap distance
 *     bounce           : true, //do bounce effect when end is first reached
 *     endFollowsBounce : false, // 
 *     useTouch    : (Modrnizr.touch)
 *     useKeyboard      : true, //allow keyboard arrows to scroll
 *     buttonPrefix     : "data-episode-scroll" // data attribute prefix to identify the buttons that would invoke scrolling
 * };
 * 
 * var theScroller = new FastScroller(scrollConfig, onBounce, onEndTrigged);
 * 
**/



/*
 * Constructor function
 *
 * @param {Object} scrollConfig 
 * @param {Function} bouncedCallback 
 * @param {Function} reachedEndCallback 
 * @param {Function} scrollingCallback 
 * @param {Function} leapFinisedCallback 
 **/

var FastScroller = function(scrollConfig, bouncedCallback, reachedEndCallback, scrollingCallback, leapFinisedCallback) {

  "use strict";



    if(scrollConfig.leap>document.documentElement.clientWidth){scrollConfig.leap=document.documentElement.clientWidth};//dont scroll more than device width
  var content, parent, leftButton, rightButton, callbackBounce, callbackEnd,
    callbackScrolling, callbackLeapFinished, callbackData, leapDistance = 400,
    initialSwipeDirection = "",
    justBouncedSwipe = false,
    swipePunchInTime = 0,
    lastPosition = 0,
    lastPositionFrameBefore = 0,
    swipeVelocity = 0,
    leapTime = .75,
    useFirmin, doEndBounce = true,
    endFollowsBounce = false,
    doWheelScroll = true,
    doTouchScroll = (Modernizr.touch),
    doKeyboardScroll = true,
    doThrottleClicks = true,
    leapTimer, tickTimer, justBounced, justBouncedDirection, that = this,
    touchStartPoint, leftToSwipeLeft, leftToSwipeRight, needToCancelSwipe = false,
    allowPropagation = false,
    $;

  /*
   * Public method - Initializes the scroller
   * by setting variables and attaching listeners
   *
  **/
  this.init = function() {
    setVars();
    addListeners();
  };

  /*
   * set the vars, cache what you can
   *
  **/
  var setVars = function() {

    //remap jquery to avoid conflicts
    $ = jQuery;

    //map jquery objects
    content = scrollConfig.divToScroll;
    parent = content.parent();
    leftButton = $(document).find("[" + scrollConfig.buttonPrefix + "-left]");
    rightButton = $(document).find("[" + scrollConfig.buttonPrefix + "-right]");

    //and callbacks
    callbackBounce = bouncedCallback;
    callbackEnd = reachedEndCallback;
    callbackScrolling = scrollingCallback;
    callbackLeapFinished = leapFinisedCallback;
    callbackData = {};

    //transforms work best on a webkit browser, adding modernizr test
    Modernizr.addTest('webkit', function() {
      return RegExp(" AppleWebKit/").test(navigator.userAgent);
    });

    //if its webkit and supports csstransforms, lets use
    //formin as our primary animation engine
    useFirmin = Modernizr.webkit && Modernizr.csstransforms;

    //override the default values with the passed config object?
    leapDistance = !_.isUndefined(scrollConfig.leap) ? (scrollConfig.leap) : leapDistance;
    leapTime = !_.isUndefined(scrollConfig.leapTime) ? (scrollConfig.leapTime) : leapTime;
    doEndBounce = !_.isUndefined(scrollConfig.bounce) ? (scrollConfig.bounce) : doEndBounce;
    endFollowsBounce = !_.isUndefined(scrollConfig.endFollowsBounce) ? (scrollConfig.endFollowsBounce) : endFollowsBounce;
    doWheelScroll = !_.isUndefined(scrollConfig.useMouseWheel) ? (scrollConfig.useMouseWheel) : doWheelScroll;
    doTouchScroll = !_.isUndefined(scrollConfig.useTouch) ? (scrollConfig.useTouch) : doTouchScroll;
    doKeyboardScroll = !_.isUndefined(scrollConfig.useKeyboard) ? (scrollConfig.useKeyboard) : doKeyboardScroll;
    doThrottleClicks = !_.isUndefined(scrollConfig.throttleClicks) ? (scrollConfig.throttleClicks) : doThrottleClicks;
  };
 
  /*
   * add listeners to click, touch, and
   * wheel, as per the config object passed
   *
  **/
  var addListeners = function() {

    // buttons scrolling
    //get a throttled version of handleClick function
    if (doThrottleClicks) {
      var handleClickThrottled = _.throttle(handleClick, leapTime * 1000, {
        trailing: false
      });
    } else {
      handleClickThrottled = handleClick;
    }
    
    handleButtonHiding();

    // leftButton.click(function(e) {
    //   handleClickThrottled(e);
    //   _.delay(function() {
    //     allowPropagation = false;
    //   }, 10);
    //   return allowPropagation;
    // });

    // rightButton.click(function(e) {
    //   handleClickThrottled(e);
    //   _.delay(function() {
    //     allowPropagation = false;
    //   }, 10);
    //   return allowPropagation;
    // });
    
    leftButton.on({
      'touchstart click': function(e) {
      	if (e.type == "touchstart"){
    		e.stopPropagation();
    		e.stopImmediatePropagation();  
    		return;
    	} 	  
        handleClickThrottled(e);
        _.delay(function() {
          allowPropagation = false;
        }, 10);
        return allowPropagation;
      }

    }) 

    rightButton.on({
      'touchstart click': function(e) {
    	if (e.type == "touchstart"){
    		e.stopPropagation();
    		e.stopImmediatePropagation();  
    		return;
    	}
        handleClickThrottled(e);
        _.delay(function() {
          allowPropagation = false;
        }, 10);
        return allowPropagation;
      }

    }) 



    // keyboard scrolling
    if (doKeyboardScroll) {

      //add keydown listener
      $(document).keydown(function(e) {
        //if left or right arrrow,  invoke handleClick()
        if (e.keyCode === 37 || e.keyCode === 39) {
          handleClick(e);
          return false;
        }
      });
    }

    // touch scrolling
    if (doTouchScroll) {
     
      //add swipe listener via jquery.swipe.js
      content.swipe({
    	excludedElements: ".noSwipe",
        //Default is 75px, set to 0 for demo so any distance triggers swipe
        threshold: 100,
        triggerOnTouchLeave: true,
    	
    	//threshold : 100,
        //Generic swipe handler for all directions
    	tap : function (event,target){
    		console.log("tap");
    		$(target).closest("a").trigger("click");
    		return true;
    	},
    	longTap : function (event,target){
    		console.log("longTap");
    		return true;
    	},   
    	/*
    	swipeLeft : function (e) {
    		console.log("swipeLeft", e);
    	},
    	swipeRight : function (e) {
    		console.log("swipeRight", e);
    	},
    	*/
        swipeStatus: function(event, phase, direction, distance, duration, fingers) {
    	  console.log("swipeStatus: ", phase);        
          handleSwipe(event, phase, direction, distance, duration, fingers);
          if (needToCancelSwipe) {
            return false;
          }
        }

      });
    }

    //wheel scrolling
    //get a throttled version of handle wheel function
    var handleWheelThrottled = _.throttle(handleWheel, 1300, {
      trailing: false
    });

    //attach listener view jquery.mousewheel.js
    if (doWheelScroll) {
      parent.mousewheel(function(event, delta, deltaX, deltaY) {
        event.preventDefault();
        // event.stopImmediatePropagation();
        event.stopPropagation();
        handleWheelThrottled(event, delta, deltaX, deltaY);
      });
    }
  };

  /*
   * handles wheel interaction
   * @param event
   * @param delta
   * @param deltaX
   * @param deltaY
  **/
  var handleWheel = function(event, delta, deltaX, deltaY) {

    //figure out the direction of wheel scroll
    var direction = (deltaY < 0) ? "left" : "right";
    //distance
    var distance = (deltaY) * -1;
    //how much space is left to move in this direction
    var leftToMoveInThisDirection = getSpaceLeftToMove(direction);
    // var leapDistance = 500;
    // //get current offset
    // touchStartPoint = getLeftOffset(content);
    // var whereTo;

    // //figure out what to do, where to move content to or bounce if at end
    // if (leftToMoveInThisDirection >= leapDistance) {
    //   whereTo = (direction == "right") ? (touchStartPoint + (leapDistance)) : (touchStartPoint - (leapDistance));
    // } else {
    //   if (leftToMoveInThisDirection !== 0) {
    //     whereTo = (direction == "right") ? (touchStartPoint + (leftToMoveInThisDirection)) : (touchStartPoint - (leftToMoveInThisDirection));
    //   } else {
    //     bounce(direction, false);
    //     return;
    //   }
    //     //whereTo = touchStartPoint + leftToMoveInThisDirection;
    // }



    //execute callback, notifying that scrolling as occured
    // if (callbackScrolling && typeof callbackScrolling == 'function') {
    //   callbackScrolling();
    // }

    if(direction === "left"){
      rightButton.trigger("click");
    }else {
      leftButton.trigger("click");
    }
    
    handleButtonHiding(direction);
    
    //invoke the offset
    // setLeftOffset(content, whereTo, 1);
  };

  /*
   * handles swipe interaction
   * @param event
   * @param phase
   * @param direction
   * @param distance
   * @param duration
   * @param fingers
  **/
  var handleSwipe = function(event, phase, direction, distance, duration, fingers) {

    
    //check the phase of the swipe event
    if (phase === "start") {
      justBouncedSwipe = false;
      //console.log("initial direction is "+direction);
      needToCancelSwipe = false;
      touchStartPoint = getLeftOffset(content);
      leftToSwipeLeft = getSpaceLeftToMove("left");
      leftToSwipeRight = getSpaceLeftToMove("right");
      //console.log("l "+leftToSwipeLeft + "  r  "+leftToSwipeRight + " direction "+direction);
    } else if (phase === "move") {
      //console.log("DIRECTION "+direction);



      if(direction === "up" || direction === "down"){
        // if( (distance >= leftToSwipeLeft + looseness) || (distance >= leftToSwipeRight + looseness) ){
        //   console.warn("IN THAT ZONE");
        // }

        if(leftToSwipeRight === 0){
          needToCancelSwipe = true;
          bounce("right", true);
          initialSwipeDirection = "";
        }else if(leftToSwipeLeft === 0){

          needToCancelSwipe = true;
          bounce("left", true);
          initialSwipeDirection = "";
        }
        // console.warn("IN THAT ZONE");
        // console.log("left "+leftToSwipeLeft);
        // console.log("right "+leftToSwipeRight);

        swipeVelocity = getLeftOffset(content)
        return;
      }

      if(initialSwipeDirection === ""){
        initialSwipeDirection = direction;
      }
      
      var whereTo;
      var looseness = 100;

      // if(direction !== initialSwipeDirection &&){
      //   needToCancelSwipe = true;
      //   initialSwipeDirection = "";
      //   //console.log
      //   //console.log("other direction");
      //   return;
      // }else{
      //   //console.log("same direction");
      // }
      

      //figure out the direction and whereTo move content
      if (direction === "left") whereTo = touchStartPoint - distance;
      else whereTo = touchStartPoint + distance;


      
      //if needs to bounce
      if (direction === "left" && (distance >= leftToSwipeLeft + looseness)) {
        justBouncedSwipe = true;
        needToCancelSwipe = true;
        bounce("left", true);
        initialSwipeDirection = "";
        return;
        //console.log("reached that point left");
      }
      
      if (direction === "right" && (distance >= leftToSwipeRight + looseness)) {
        justBouncedSwipe = true;
        needToCancelSwipe = true;
        bounce("right", true);
        initialSwipeDirection = "";
        return;
        //console.log("reached that point right");
      }

      //move the content

      lastPositionFrameBefore = lastPosition;
      lastPosition = whereTo;
      //console.log("last position frame before logged as "+lastPositionFrameBefore);
      setLeftOffset(content, whereTo);
      // dconsole.lg
    }else if (phase === "end") {

      var newLeftToSwipeLeft = getSpaceLeftToMove("left");
      var newLeftToSwipeRight = getSpaceLeftToMove("right");


      swipeVelocity = lastPositionFrameBefore - lastPosition;
      var swipeTailDistance = (swipeVelocity * 10);
      
      var timeOfSwipe = Math.abs(swipeVelocity) / 130;

      

      console.warn("______________________");
      //console.log("direction "+direction);
      //console.log("newLeftToSwipeRight "+newLeftToSwipeRight);
      console.log("swipe tail distance "+swipeTailDistance);
      //console.log("newLeftToSwipeRight "+newLeftToSwipeRight);
      //console.log("current offset "+lastPosition);


      if(Math.abs(swipeTailDistance) < 5){
        initialSwipeDirection = "";
        return;
      }
      if( Math.abs(swipeTailDistance) > 800 ){
        //normalize the tail distance
        if(swipeTailDistance > 0){
          swipeTailDistance = 800;
        }else{
          swipeTailDistance = 800 * -1;
        }
        //swipeTailDistance = 
      }

      var newWhereTo = Math.round(lastPosition - swipeTailDistance);

      console.log("swipe tail distance "+swipeTailDistance);

      if(!justBouncedSwipe){

        //normalize the tail distance
        if(swipeTailDistance)

        if(direction === "left"){
          if(swipeTailDistance < newLeftToSwipeLeft ){
            //console.log("tail should fit");
            setLeftOffset(content, newWhereTo, timeOfSwipe );
          }
          else{
            //console.log("tail won't fit");
            setLeftOffset(content, getEndOffset("left"), timeOfSwipe );
          }
        }else{
          if(Math.abs(swipeTailDistance) < newLeftToSwipeRight ){
            console.log("tail should fit");
            setLeftOffset(content, newWhereTo, timeOfSwipe );
          }
          else{
            console.log("tail won't fit");
            setLeftOffset(content, getEndOffset("right"), timeOfSwipe );
          }

        }

        
      }
   

      //console.log("direction "+direction);

      // if(direction === "right"){
      //     if( getLeftOffset(content ) + newLeftToSwipeRight < newWhereTo){
      //       setLeftOffset(content, newWhereTo, timeOfSwipe );
      //     }
      //     else{
      //       setLeftOffset(content, getEndOffset("right") , timeOfSwipe );
      //     }
      // }

      //console.log("on end , position is "+getLeftOffset(content));
      //console.log("therefore, velocity is "+swipeVelocity);
      initialSwipeDirection = "";

      //if(Math.abs(swipeVelocity) )

      //setLeftOffset(content, newWhereTo, timeOfSwipe );
      return;
    }
    
    handleButtonHiding(direction);
    
  };

  /*
   * handles the click or keyboard press 
   * 
   * first determines if scrolling of content is possible, if it's at end or beginning,
   * and if events propagation needs to be stoped. it also calculates where the content 
   * would be moved to and finally calls setLeftOffset() to actually move the content area
   * @param {Object} event
   *
   * TODO need to comment this method
  **/
  var handleClick = function(event) {
    
    var scrollCaller = getScrollCaller(event);
    var callerType = scrollCaller.type;
    var direction = scrollCaller.direction;
    var isEnd = isAtEnd(direction);

    if ($(scrollCaller.currentTarget).hasClass("inactive")) return false;
    //if (content.outerWidth() <= parent.outerWidth()) return false;
    
    handleButtonHiding(direction);

    if (!isEnd) {

      event.preventDefault();
      event.stopPropagation();

      var spaceLeftToMove = getSpaceLeftToMove(direction);
      var currOffset = getLeftOffset(content);
      var whereTo;
      var theLeap;

      if (spaceLeftToMove >= leapDistance) theLeap = leapDistance;
      else theLeap = spaceLeftToMove;

      if (direction === "left") whereTo = currOffset - theLeap;
      else whereTo = currOffset + theLeap;

      setLeftOffset(content, Math.round(whereTo), leapTime);

      if (callbackScrolling && typeof callbackScrolling == 'function') {
        callbackScrolling();
      }

      justBounced = false;
      justBouncedDirection = direction;
      clearTimeout(leapTimer);

      leapTimer = setTimeout(function() {
        if (callbackLeapFinished && typeof callbackLeapFinished == 'function') {
          callbackLeapFinished();
        }
      }, leapTime * 1000);

    } else {


      callbackData.direction = direction;
      callbackData.currentTarget = scrollCaller.currentTarget;
      callbackData.type = callerType;

      //console.dir(callbackData);
      if ((doEndBounce && !justBounced) || (doEndBounce && !endFollowsBounce)) {
        event.preventDefault();
        event.stopPropagation();

        bounce(direction, false);

        if (callbackBounce && typeof callbackBounce == 'function') 
          callbackBounce(callbackData);

        justBounced = true;
        justBouncedDirection = direction;

      } else {

        

        // console.log("")
        // var lastDirection;

        // if( event.currentTarget === leftButton[0] )
        //   lastDirection = "right";
        // else
        //   lastDirection = "left";


        // console

        if( justBouncedDirection === "left" ){
          if(event.currentTarget === rightButton[0]){
            allowPropagation = true;
          }
          else{
            allowPropagation = false;
          }
        }
        else if( justBouncedDirection === "right" ){
          if(event.currentTarget === leftButton[0]){
            allowPropagation = true;
          }
          else{
            allowPropagation = false;
          }
        }



        if (callbackEnd && typeof callbackEnd == 'function') 
          callbackEnd(callbackData);

        justBounced = false;
        justBouncedDirection = direction;


        // if( (event.currentTarget === leftButton[0] && justBouncedDirection === "left") || (event.currentTarget === rightButton[0] && justBouncedDirection === "right") ){
        //   allowPropagation = true;
        // }else{
        //   allowPropagation = false;
        // }

        //allowPropagation = false;
        //if( e.currentTarget === justBouncedDirection === "left" )
       
      }
    }
  };


  //UTILITY FUNCTIONS
  /*
   * hide navigation buttons when content is no longer scrollable
   *
  **/
  var handleButtonHiding = function(direction) {

	  var spaceAvailLeft = getSpaceLeftToMove('left');
	  var spaceAvailRight = getSpaceLeftToMove('right');
	  var buttonLeft = leftButton.find(".scroll-arrow").children('a').length;
	  var buttonRight = rightButton.find(".scroll-arrow").children('a').length;
	  var atEndLeft = isAtEnd('left');
	  var atEndRight = isAtEnd('right');
	  var subSectionL = leftButton[0].className.indexOf('section-scroll-arrow') != -1;
	  var subSectionR = rightButton[0].className.indexOf('section-scroll-arrow') != -1;
	  var goTo = direction;
	  
	  /*
	  console.log('goTo ' + goTo);
	  console.log('isClicked ' + isClicked);
	  console.log('spaceAvailLeft ' + spaceAvailLeft);
	  console.log('spaceAvailRight ' + spaceAvailRight);
	  console.log('buttonLeft ' + buttonLeft);
	  console.log('buttonRight ' + buttonRight);
	  console.log('atEndLeft ' + atEndLeft);
	  console.log('atEndRight ' + atEndRight);
	  console.log('DONE');
	  */
	  
	  // left button
	  if (buttonLeft == 0 && spaceAvailRight <= 0) {
		  if (subSectionL == false) {
			  leftButton.fadeOut('slow');
		  } else {
			  leftButton.addClass('fs-disabled');
			  rightButton.removeClass('fs-disabled');
		  }
	  } else {
		  if (subSectionL == false) {
			  leftButton.fadeIn('slow');
		  } else {
			  leftButton.removeClass('fs-disabled');
			  leftButton.bind( "click", function() {
				  rightButton.removeClass('fs-disabled');
			  });
		  }
	  }
	  
	  // right button
	  if (doTouchScroll) {
		  if (goTo == 'left') {
			  if (buttonRight == 0 && spaceAvailLeft <= 0) {
				  if (subSectionR == false) {
					  rightButton.fadeOut('slow');
				  } else {
					  rightButton.addClass('fs-disabled');
				  }
			  } else {
				  if (subSectionR == false) {
					  rightButton.fadeIn('slow');
				  } else {
					  rightButton.removeClass('fs-disabled');
				  }
			  }
		  }
	  }
	  
	  if (goTo == 'left') {
		  if (atEndLeft == true) {
			  if (subSectionR == false && buttonRight == 0) {
				  rightButton.fadeOut('slow');
			  } else {
				  rightButton.addClass('fs-disabled');
			  }			  
		  } else {
			  if (subSectionR == false) {
				  leftButton.fadeIn('slow');
			  } else {
				  leftButton.removeClass('fs-disabled');
			  }			  
		  }
	  }
	  
	  if (goTo == 'right') {
		  if (atEndRight == true) {
			  if (subSectionL == false && buttonLeft == 0) {
				  leftButton.fadeOut('slow');
			  }
		  } else {
			  if (subSectionL == false) {
				  rightButton.fadeIn('slow');  
			  }
		  }
	  }
	  
  };
  
  /*
   * produces an iOS-like bounce effect
   * @param {String} directon
   * @param {Boolean} snapToEnd
   *
  **/
  var bounce = function(direction, snapToEnd) {

    var currentPosition = getLeftOffset(content);
    var bounceRange = 30;
    var whereTo;

    if (!snapToEnd) {

      if (direction === "right") whereTo = currentPosition + bounceRange;
      else whereTo = currentPosition - bounceRange;
      
      setLeftOffset(content, Math.round(whereTo), 0.2);
      setTimeout(function() {
        setLeftOffset(content, currentPosition, 0.5);
      }, 200);

    } else {

      var endPosition = getEndOffset(direction);
      if (direction === "left") whereTo = endPosition;
      else whereTo = endPosition;
          
      setLeftOffset(content, Math.round(whereTo), 0.2);
    }
  };

  /*
   * determines is the content is at the beggining or end
   * (cannot scroll anymore)
   * @param {String} direction
  **/

  var isAtEnd = function(direction) {

    var isEnd;
    
    if (direction === "right") {
    	
    	if (getLeftOffset(content) === 0) {
    		
    		return true;
    		
    	} else {
    		
    		return false;
    		
    	}

    } else if (direction === "left") {
    	
    	if (parent.outerWidth() > content.outerWidth()) {
    		
    		return true;
    		
      
    	} else if (getLeftOffset(content) === (parent.outerWidth() - content.outerWidth())) {

    		return true;
    	  
    	} else {
    		
    		return false;	
    		
    	}
    	
      
    }
    
    
  };

  /*
   * returns ammount of space left to move for content
   * @param {String} direction
  **/
  var getSpaceLeftToMove = function(direction) {
    if (direction === "left") {
      return (getLeftOffset(content) + content.outerWidth()) - parent.outerWidth();
    } else {
      return 0 - getLeftOffset(content);
    }
  };

  /*
   * determines an offset of the End of the scroll
   * @param {String} direction
  **/
  var getEndOffset = function(direction) {
    if (direction === "right") {
      return 0
    } else {
      return parent.outerWidth() - content.outerWidth();
    }
  }

  /*
   * returns position
   * @param {Object} el
  **/
  var getLeftOffset = function(el) {
    if (!useFirmin) return el.position().left;
    else return getLeftOffsetMatrix(el);
  };

  /*
   * sets position of the content
   * @param {Object} el
   * @param {Number} value
   * @param {Number} time
  **/
  var setLeftOffset = function(el, value, time) {
    if(useFirmin){ 
      Firmin.translateX(el[0], value, time + "s");
    }
    else{
      TweenLite.to(el, time, {
        left: value,
        ease: Strong.easeOut
      });      
    } 
  };

  /*
   * returns a massaged event object 
   * of what action invoked the scroll
   * @param {Event} event
  **/
  var getScrollCaller = function(event) {

    var scrollDirection;
    if (event.type === "click") {

      //determines direction from the target
      if (event.currentTarget === leftButton[0]) scrollDirection = "right";
      else scrollDirection = "left";
      // return false;
    }
    else if (event.type === "mousedown") {

      //determines direction from the target
      if (event.currentTarget === leftButton[0]) scrollDirection = "right";
      else scrollDirection = "left";

    } else if (event.type === "keydown") {

      //determines direction from the target
      if (event.which === 39) scrollDirection = "left";
      else scrollDirection = "right";

    } else if (event.type === "touchstart") {

      //determines direction from the target
      if (event.currentTarget === leftButton[0]) scrollDirection = "right";
      else scrollDirection = "left";

    }


    

    //returns and object with type, direction and currentTarget
    return {
      type: event.type,
      direction: scrollDirection,
      currentTarget: event.target
    };
  };

  /*
   * returns left offset using transform matrix
   * @param {Object} el
  **/
  var getLeftOffsetMatrix = function(el) {
    var style = window.getComputedStyle(el[0]).webkitTransform;
    var theMatrix = style.replace(/^matrix\(/, '').replace(/\)$/, '').split(', ');
    if (theMatrix[0] === "none") return 0;
    else return parseFloat(theMatrix[4]);
  };

  //kick things off
  this.init();


};
/**
 * @file social.js
 *
 * Includes reusable global functions that are not specific to any package.
 */

jQuery(document).ready(function() {

  // Social Share Popup Window.
  jQuery('.twitter-popup, .facebook-popup').click(function(event) {
    console.log('social js clicked link');
    var ahref = this.href;
    var width  = 575,
    height = 400,
    left   = 0,
    top    = 0,
    url    = ahref,
    opts   = 'status=1' +
             ',width='  + width  +
             ',height=' + height +
             ',top='    + top    +
             ',left='   + left;

    var popUp = window.open(url, 'twitter', opts);
    if (popUp == null || typeof(popUp) == 'undefined') {
      //popup didn't work so replace in window. beat the popup blocker.
      document.location = ahref;
    }
    event.preventDefault();
    return false;
  });

});
;

var GlobalNav = function(){

  var $ = jQuery;
//  var openNavButton = $('#header .nav p');
  var theNav = $('#header-nav');
  var that = this;
  var subMenus = $('#header-nav .menuItem > a');
  var navElement = $('#header-nav .menuItem');
  var socialButton = $('#footer .cab_switch');
  var socialNav = $('#footer_social');
  var isMouseOverMenu = false;

  this.init = function() {
    addListeners();
    //this.openNav();

    jQuery('div.hamburger.global').on('click touchstart', function(e) {
    e.preventDefault();
    jQuery('#header-nav > ul').toggle();
    jQuery('#site_nav').removeClass('open');
  });
  jQuery('div.hamburger.show').on('click touchstart', function(e) {
    e.preventDefault();
    jQuery('#site_nav').toggleClass('open');
    jQuery('#header-nav > ul').hide();
  });
  jQuery('#show_nav a').on('click', function() {
    if (jQuery(window).width() <= 1023 ) {
      jQuery('#site_nav').removeClass('open');
      //jQuery('div.hamburger.show').removeClass('open');
      //jQuery('#show_nav').hide();
    }
    return true;
  });
  jQuery('#header-nav > ul a').on('click', function() {
    if (jQuery(window).width() <= 1023 ) {
      jQuery('#header-nav > ul').hide();
    }
    return true;
  });


//  jQuery('#header-nav > ul a, #show_nav a').on('click touchstart', function() {
//    jQuery('#header-nav > ul').hide();
//    jQuery('#show_nav').hide();
//  });
  jQuery(window).resize(function() {
    if (jQuery(window).width() > 1023 ) {
      jQuery('#header-nav > ul').show();
    } else {
      jQuery('#header-nav > ul').hide();
    }
  });

//    _.delay(function(){
//      if(!isMouseOverMenu){
//         that.closelNav();
//      }

//    }, 6000);
  }

//  function closeAllLis(){
//    subMenus.each(function(a, b){
//      $(this).parent().removeClass("open");
//    });

//  }

  function addListeners(){

    socialButton.on("click touchstart", function (e){
      e.preventDefault();
      socialNav.toggleClass('open');
      //aetv.googleSearch2.CloseSearchBar();
    });

    //ROLL OVER, CLICK BASED
    if(!Modernizr.touch){
      if (!Modernizr.cssanimations) {
        //console.log('does not have animations');
//        jQuery('#site_nav #show_time_long').hide();
        //
        // Global nav
        //
        //jQuery('#header .nav > ul > li    > div').hide();
//        jQuery('#header_nav_dd_wrapper').hide();
        //jQuery('#header .nav > ul > li > ul').hide();
//        jQuery('#header .nav > ul > li > div').hide();
        //jQuery('#header .nav p img').css({'rotate': 3.141592654});
//        var time; // setTimeout for nav slideout
//        var menu_copy;
//        var menu_class;
//        jQuery('#header_nav_dd').fadeIn();

        // FB Like button flyout
        jQuery('#footer #footer_social a').mouseenter(function() {
          jQuery(this).addClass('open');
        }).mouseleave(function() {
           jQuery(this).removeClass('open');
        });

        // Menu horizontal movement
//        function slide_menu_out () {

//          jQuery('#header .nav p img').stop(true,false).animate({
            //'rotate': -6.283185307
//            'rotate' : 0
//          }, 1000, function() {
//            socialNav.removeClass('open');
//              aetv.googleSearch2.CloseSearchBar();
//          });

//          jQuery('#header .nav > ul').stop(true,false).animate({
//            'left': '84px'
//          }, 1000);

//        }
//        function slide_menu_in () {
//          jQuery('#header .nav p img').stop(true,false).animate({
//            'rotate' : -3.141592654
//          }, 1000, function() {
//            // Calibrate chevron
//            jQuery(this).css({'rotate': 3.141592654});
//          });
//          jQuery('#header .nav > ul').stop(true,false).animate({
//            'left': '-100%'
//          }, 1000);
//        }

//        jQuery('#header .nav').bind({
//          mouseenter: function () {
//            clearTimeout(time);
//            slide_menu_out();
//            aetv.googleSearch2.CloseSearchBar();
//         }//,
//          mouseleave: function() {
//            time = setTimeout(slide_menu_in, 1500);
//          }
//        });

        // Submenu dropdown
//        jQuery('#header .nav > ul').bind({
//         mouseenter: function() {
//            jQuery('#header_nav_dd_wrapper').stop(true,true).slideDown(200);
//            socialNav.removeClass('open');
//            aetv.googleSearch2.CloseSearchBar();
//          },
//          mouseleave: function() {
//            jQuery('#header_nav_dd_wrapper').stop(true,true).slideUp(200);
//            jQuery('#header .nav > ul > li').removeClass('active');
//          }
//        });

        // Submenu content
//        subMenus.mouseenter(function(e) {

//          var hasNoSubMenu1 = $(this).parent().hasClass("games");
//          var hasNoSubMenu2 = $(this).parent().hasClass("watch-live");


//          if ( hasNoSubMenu1 != true && hasNoSubMenu2 != true) {

//                jQuery('#header_nav_dd').stop();
//                jQuery('#header_nav_dd').removeClass();
//                menu_copy = jQuery(this).siblings('div').html();
//                menu_class = jQuery(this).parent('li').attr('class');
//                jQuery(this).parent('li').siblings().removeClass('active');
//                jQuery(this).parent('li').addClass('active');

//                jQuery('#header_nav_dd').fadeOut(250, function() {
//                  jQuery('#header_nav_dd').html(menu_copy);
//                  jQuery('#header_nav_dd').addClass(menu_class);
//                }).stop(true,true).fadeIn(250);

//                jQuery('#header_nav_dd_wrapper').show();

        // Open nav on page load
//        slide_menu_out();

//          } else {

//            e.stopPropagation();
//            e.stopPropagation();
//            that.closelNav();
//            jQuery('#header .nav > ul > li').removeClass('active');
//            jQuery('#header_nav_dd_wrapper').hide();
//            navElement.removeClass('open');
//            subMenus.removeClass('open');

//          }

//        });


      } //else {

//        theNav.bind({
//          mouseenter: function() {
//            isMouseOverMenu = true;
            //that.openNav();
//            aetv.googleSearch2.CloseSearchBar();
//          },
//          mouseleave: function() {
//            isMouseOverMenu = false;
            //that.closelNav();
//          }
//        });


//        subMenus.each(function(a, b){
//          $(this).on({
//            'mouseenter' : function(e){

              //e.preventDefault();
//              if( $(this).parent().hasClass("open") ) {
//                $(this).parent().removeClass("open");
//              }else{
//                closeAllLis();
//                $(this).parent().addClass("open");
//              }
//            },
//            'mouseleave' : function(e){

              //e.preventDefault();
//              if( $(this).hasClass("open") ) {
//                closeAllLis();
                //$(this).removeClass("open");
//              }else{
                //$(this).addClass("open");
//              }
//            }
//          });


//        });

//        var clickItems = subMenus.find("li a");

//        clickItems.click(function(){
//          closeAllLis();
//          this.closelNav();

//        });
//      }



    //TOUCH BASED
    } else {
//      openNavButton.on({'touchstart': function(e) {

//          if( that.isNavOpen() ){
//            that.closelNav();
//          } else {
//            that.openNav();
//          }

//          aetv.googleSearch2.CloseSearchBar();

//        }

//      });

      if (!swfobject.hasFlashPlayerVersion("9.0.18")) {
      $('#header-nav .games').hide();
      }

      //subMenus.each(function(a, b){

//        $(this).on({'touchstart' : function(e){
//          var hasNoSubMenu1 = $(this).parent().hasClass("watch-live");
//          var hasNoSubMenu2 = $(this).parent().hasClass("games");
//          var changeTextFor = $(this).parent().attr('class').split(' ')[0];
//          var menuItemClone = $(this);
//          var menuItemContainer = $(this).parent().find('.entry-wrapper');
//          var clonedItem = menuItemContainer.find('.clonedMenuItem');
//          var itemText = clonedItem.find('.clonedMenuItem');

//            aetv.googleSearch2.CloseSearchBar();
//            if ( changeTextFor == 'shows' ) {
//              itemText = 'Go to All Shows';
//            } else if ( changeTextFor == 'video' ) {
//              itemText = 'Go to All Videos';
//            } else {
//              itemText = $(this).text();
//            }

//          menuItemClone.clone().addClass('clonedMenuItem').text(itemText).attr('data-pjax-link-container', 'pjax').appendTo(menuItemContainer);

//          if ( !hasNoSubMenu1 && !hasNoSubMenu2 ) {
//              e.preventDefault();
//        }

//            if( $(this).parent().hasClass("open") ) {

//              $(this).parent().removeClass("open");
//              clonedItem.remove();

//            } else {

//              subMenus.each(function(a, b){

//                $(this).parent().removeClass("open");
//                clonedItem.remove();

//              });

//              $(this).parent().addClass("open");

//            }

//          }

//        });

//      });

      $('#header .entry-wrapper').on('touchstart', 'a', function(e) {
      _.delay(function(){
        navElement.removeClass('open');
        theNav.removeClass('open');
      }, 600);
      });
    }



  };



  this.openNav = function(theDelay){

    _.delay(function(){

      theNav.addClass('open');
//      socialNav.removeClass('open');
      navElement.removeClass('open');

    }, theDelay ? theDelay : 0);

  };

  this.closelNav = function(theDelay){

    _.delay(function(){

      theNav.removeClass('open');
      navElement.removeClass('open');

    }, theDelay ? theDelay : 0);

  };

  this.isNavOpen = function(){
    return theNav.hasClass("open");
  }



    // setTimeout (function() {theNav.addClass('open');}, 0);
    // setTimeout (function() {theNav.removeClass('open');}, 10000);

    // prevent partial menu load on hover
     jQuery('#header-nav.nav').mouseenter(function() {
       jQuery('#header-nav.nav').addClass('open');
//       setTimeout (function() {
//         closeGlobalNav();
//       }, 2500);
     });


    // global nav sizing
   /* var menu_area = jQuery('#header_social').offset().left - jQuery('#header .nav > p').outerWidth() - jQuery('#header .nav > p').offset().left;
    var menu_width = 0;
    jQuery('#header .nav > ul > li').each(function() {
      menu_width += jQuery(this).width();
    });
    //globalnav_runover(menu_area, menu_width);
    jQuery(window).resize(function() {
      menu_area = jQuery('#header_social').offset().left - jQuery('#header .nav > p').outerWidth() - jQuery('#header .nav > p').offset().left;
      //globalnav_runover(menu_area, menu_width);
    });*/

 /*   // more social icons
    jQuery('div.cab_switch').click(function() {
      jQuery('#header_social').toggleClass('open');
    });*/



  this.init();

}

//var lockNavOpen = function() {
//  var navList = jQuery('.nav > ul');

//  this.init = function() {
//    navList.css('left','85px');
//  }

//  init();
//}


var Scheduler = function() {

  var live_show = {};
  var next_show_1 = {};
  var next_show_2 = {};

  this.getLiveShow = getLiveShow;
  this.getNextShow = getNextShow;
  this.getNextShow2 = getNextShow2;


  function getLiveShow() { return live_show; }
  function getNextShow() { return next_show_1; }
  function getNextShow2() { return next_show_2; }


  var init = function() {
    setDefaults();
    setVars();
  }

  var setDefaults = function() {
      // Set a default value to all objects
      this.live_show   = set_show_defaults();
      this.next_show_1 = set_show_defaults();
      this.next_show_2 = set_show_defaults();
  }

  var setVars = function() {
    var ds = Drupal.settings;
    if (typeof(ds.aetv_global_schedule) != 'undefined') {
      if (ds.aetv_global_schedule.length > 0) {
        var now = new Date();
        var tabOnNow = jQuery('#header-nav .schedule .column2');
        var tabUpNext = jQuery('#header-nav .schedule .column3');

        for (var key in ds.aetv_global_schedule) {
          var airtime  = new Date(ds.aetv_global_schedule[key].airdate);
          var airtime_end  = new Date(ds.aetv_global_schedule[key].airdate_end);

          if (+now >= airtime.getTime() && +now < airtime_end.getTime()) {
            live_show = ds.aetv_global_schedule[key];
            next_show_1 = ds.aetv_global_schedule[parseInt(key)+1];
            next_show_2 = ds.aetv_global_schedule[parseInt(key)+2];
            break;
          }
        }

        // @todo make this dynamic so that it can display any number of scheduled
        // items.

        // Next On
        if (typeof next_show_1 == 'object') {
          if (next_show_1.airdate) {
            var next_show_1_date    = new Date(next_show_1.airdate);
            var next_show_1_hours   = next_show_1_date.getHours();
            var next_show_1_minutes = next_show_1_date.getMinutes();

            next_show_1.hour    = get_date_hour(next_show_1_hours);
            next_show_1.minute  = get_date_minute(next_show_1_minutes);
            next_show_1.tt      = get_date_tt(next_show_1_hours);
          }
        }

        if (typeof next_show_2 == 'object') {
          // Next On After
          if (next_show_2.airdate) {
            var next_show_2_date    = new Date(next_show_2.airdate);
            var next_show_2_hours   = next_show_2_date.getHours();
            var next_show_2_minutes = next_show_2_date.getMinutes();

            next_show_2.hour    = get_date_hour(next_show_2_hours);
            next_show_2.minute  = get_date_minute(next_show_2_minutes);
            next_show_2.tt      = get_date_tt(next_show_2_hours);
          }
        }
        else {
          next_show_2 = {};
        }
      }
    }

    // On Now
//    if (live_show.show_name) {
//      text = live_show.show_name;
      //if (live_show.show_live_image) {
      //  jQuery('.on_now').append('<a href="' + live_show.show_url + '"><img src="' + live_show.show_live_image + '" alt="' + live_show.show_name + '" /></a>');
      //}
//      jQuery('.on_now').prepend('<h5>ON NOW</h5>');

//      if (live_show.show_url) {
//        text = '<a href="' + live_show.show_url + '">' + live_show.show_name + '</a>';
//      }
//      jQuery('.on_now h5').append('<span>' + text + '</span>');

      //also update the on now link in the Schedule menu tab
//      jQuery(tabOnNow).find('ul li').empty().append(text);
//    }

    // Next On
//    if (typeof(next_show_1) != 'undefined') {
//      if (next_show_1.show_name && (next_show_1.hour || next_show_1.minute)) {
//        text = '';
//        if (next_show_1.hour == 1) {
//          text = text + next_show_1.hour + next_show_1.minute + next_show_1.tt + '/12' + next_show_1.minute + 'C' + ' <span>' + next_show_1.show_name + '</span>';
//        } else {
//          text = text + next_show_1.hour + next_show_1.minute + next_show_1.tt + '/' + (next_show_1.hour - 1) + next_show_1.minute + 'C' + '<span>' + next_show_1.show_name + '</span>';
//        }
//        if (next_show_1.show_url) {
//          text = '<a href="' + next_show_1.show_url + '">' + text + '</a>';
//          tabText = '<a href="' + next_show_1.show_url + '">' + next_show_1.show_name + '</a>';
//        } else {
//          tabText = next_show_1.show_name;
//        }
//        jQuery('.next_on').append('<h6>' + text + '</h6>');

        //also update the next on in the Schedule menu tab
//        jQuery(tabUpNext).find('ul li').empty().append(tabText);
//      }
//    }

    // Next On After
//    if (next_show_2.show_name && (next_show_2.hour || next_show_2.minute)) {
//      text = '';
//      if (next_show_2.hour == 1) {
//        text = text + '<span>' + next_show_2.show_name + '</span> ' + next_show_2.hour + next_show_2.minute + next_show_2.tt + '/12' + next_show_2.minute + 'C';
//      } else {
//        text = text + '<span>' + next_show_2.show_name + '</span> ' + next_show_2.hour + next_show_2.minute + next_show_2.tt + '/' + (next_show_2.hour - 1) + next_show_2.minute + 'C';
//      }
//      if (next_show_2.show_url) {
//       text = '<a href="' + next_show_2.show_url + '">' + text + '</a>';
//      }
//      jQuery('.next_on').append('<h6>' + text + '</h6>');
//    }
  }

  // Returns whether an hour is Ante meridiem and Post meridiem.
  var get_date_tt = function(_hour) {
    return (_hour < 12) ? "AM" : "PM";
  }

  // Returns the 12-hour format of a 24-hour.
  var get_date_hour = function(_hour) {
    _hour = _hour % 12;

    return _hour ? _hour : 12;
  }

  // Returns the minute formatted if is not equal zero.
  var get_date_minute = function(_minute) {
    return (_minute < 10) ? "" : ":" + _minute;
  }

  // Returns an object, Show, with default values.
  var set_show_defaults = function() {
    show = new Object();
    show.show_name = '';
    show.show_url = '';
    show.show_live_image = '';
    show.hour = '';
    show.minute = '';
    show.tt = '';
    show.airdate = '';

    return show;
  }

  init();
}

//end Scheduler class

//Deaden header tab links with #
jQuery(function () {
  jQuery('#header-nav a').click(function (e) {
    var ref = jQuery(this).get(0).href;
    if (ref.charAt(ref.length-1) == '#') {
      e.preventDefault();
      return false;
    }
  });
});

// Detect Retina High Resolution Display and set Cookie
var Retina = window.devicePixelRatio > 1;

// Debug
// Retina = 1;
// console.info("Retina: " + Retina);

if (Retina) {
  // the user has a retina display
  if (jQuery.cookie("DISPLAY-TYPE") != "RETINA") {
    // Set cookie if not set
    jQuery.cookie("DISPLAY-TYPE", "RETINA", {path: '/'});
  }
}
else {
  // the user has a non-retina display
  // console.info("Retina ccokie: " + jQuery.cookie("DISPLAY-TYPE"));
  if (jQuery.cookie("DISPLAY-TYPE") == "RETINA") {
    // Remove cookie if set to Retina
    jQuery.removeCookie("DISPLAY-TYPE", {path: '/'});
    // console.info("Retina ccokie 2: " + jQuery.cookie("DISPLAY-TYPE"));
  }
}
;
