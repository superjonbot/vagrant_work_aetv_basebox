/*
* A+E Television Networks
* http://www.aetv.com/
*
* © 1998-2013, A&E Television Networks, LLC. All Rights Reserved.
*
* Author: Sue Park
*
*/

(function( $ ){

    // LET'S MAKE A NAMESPACE FOR SOME FUNCTIONS.
    $.aetvInt = {};

    $.aetvInt.getCountryCode = function() {
        var url = window.location.href;
        if(url.indexOf("?QA-Country-Code=") != -1) {
            return url.substring(url.indexOf("?QA-Country-Code=") + 17);
        } else {
            var httpCountryCode = readCookie('Country-Code');
            return httpCountryCode;
        }
    }

    $.aetvInt.getMatchingCountryJson = function( countryCode ) {
        var countryJson = null;
        for (var i = 0; i < affiliateCountries.length; i++) {
            if (affiliateCountries[i].countryCode == countryCode) {
                countryJson = affiliateCountries[i];
                break;
            }
        }
        return countryJson;
    }
    
    $.aetvInt.positionOverlay = function() {
		var windowHeight = $(window).height();
		var overlayHeight = $("#overlay_inner").height();
		var overlayMargin = ((windowHeight - overlayHeight) / 2);
		$("#overlay_inner").css({"margin-top":overlayMargin});
		$("#overlay").css({"height":windowHeight});
    }
    
    $.aetvInt.initIntOverlay = function(countryJson) {
    	
    	$('body').append('<style>' + 
	      'body { margin: 0; padding: 0; overflow: hidden; }'+
	      '#overlay { position: absolute; z-index: 9999; top: 0; left: 0; width: 100%; font-family: Arial, Helvetica, sans-serif; background: rgba(0,0,0,0.65); }'+
	      '#overlay #overlay_inner { width: 510px; height: 270px; margin: auto; border-radius: 10px; background-color: #0A63B0; box-shadow: 0 0 50px -3px rgba(255,255,255,0.65); }'+
	      '#overlay #overlay_inner img { diplay: block; float: left; margin: 40px 0 63px 20px; }'+
	      '#overlay #overlay_inner .overlay_content { width: 240px; border-radius: 8px; line-height: 20px; background: rgba(0,0,0,0.3); }'+
	      '#overlay #overlay_inner .overlay_content div { font-size: 14px; color: #d6d6d6; margin: 10px 15px; }'+
	      '#overlay #overlay_inner .overlay_content a { display: block; white-space: nowrap; padding-left: 20px; margin: 15px; height: 20px; font-size: 14px; font-weight: bold; background: url("profiles/aetv/themes/custom/aetv_base/images/intl-pu-arrow.png") no-repeat 0 3px; color: #d6d6d6; text-decoration: none; }'+
	      '#overlay #overlay_inner .overlay_content a:active, #overlay #overlay_inner .overlay_content a:hover { color: #fff; }'+
	      '#overlay #overlay_inner #overlay_copy { float: right; margin: 40px 10px 0 0; }'+
	      '#overlay #overlay_inner #overlay_link_01 { float: left; clear: left; margin: 0 0 0 10px; }'+
	      '#overlay #overlay_inner #overlay_link_02 { float: right; margin: 0 10px 0 0; }'+
				'</style>'+
	    '<div id="overlay">' +
	      '<div id="overlay_inner">' + 	
	        '<img id="overlay_logo" src="profiles/aetv/themes/custom/aetv_base/images/logo-ae.png" />' +
	        '<div class="overlay_content" id="overlay_copy">' +
	          '<div>' + countryJson.welcomeMsg + '</div>' +          
	        '</div>' + 
	        '<div class="overlay_content" id="overlay_link_01">' +
	          '<a id="usaMsg" href="#">' + countryJson.usaMsg + '</a>' + 
	        '</div>' + 
	        '<div class="overlay_content" id="overlay_link_02">' +
	          '<a id="countryMsg" href="' + countryJson.countryUrl +'">' + countryJson.countryMsg + '</a>' +
	        '</div>' +
	      '</div>' +
	    '</div>');
	        
		$.aetvInt.positionOverlay();
					
		$(window).resize(function() {
			$.aetvInt.positionOverlay();
		});
		
	    $('#usaMsg').click(function() {
	    	createCookie('country_code',countryJson.countryCode); 
	        $('#overlay').hide();
			$('body').css('overflow','auto');
	    });
        
    };

    var affiliateCountries = [{
                                    "countryCode": "AR",
                                    "countryName": "Argentina",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "BO",
                                    "countryName": "Bolivia",
                                    "welcomeMsg": " Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "BR",
                                    "countryName": "Brasil",
                                    "welcomeMsg": "Voc� est� prestes a acessar o site�do A&E Estados Unidos. Talvez prefira acessar o site do A&E Brasil.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "EC",
                                    "countryName": "Ecudaor",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "CL",
                                    "countryName": "Chile",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "CO",
                                    "countryName": "Colombia",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "CR",
                                    "countryName": "Costa Rica",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "SV",
                                    "countryName": "El Salvador",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "GT",
                                    "countryName": "Guatemala",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "HN",
                                    "countryName": "Honduras",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "MX",
                                    "countryName": "Mexico ",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "NI",
                                    "countryName": "Nicaragua",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "PA",
                                    "countryName": "Panamá ",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "PY",
                                    "countryName": "Paraguay",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "PE",
                                    "countryName": "Perú ",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "PR",
                                    "countryName": "Puerto Rico",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "DO",
                                    "countryName": "República Dominicana",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "UY",
                                    "countryName": "Uruguay",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "VE",
                                    "countryName": "Venezuela",
                                    "welcomeMsg": "Vas a entrar a la web de A&E  USA. Tal vez est&egrave;s buscando la web de A&E Latinoam&egrave;rica.",
                                    "usaMsg": "Visita A&E USA.",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visita A&E Latinoam&egrave;rica.",
                                    "countryUrl": "http://aeweb.tv"
                                },
                                            {
                                    "countryCode": "AU",
                                    "countryName": "Australia",
                                    "welcomeMsg": "You have reached the US A&E website.  If you would like to visit A&E Australia click the button below.",
                                    "usaMsg": "Visit A&E USA",
                                    "usaUrl": "http://www.aetv.com",
                                    "countryMsg": "Visit A&E Australia",
                                    "countryUrl": "http://aetv.com.au"
                                }];

$(document).ready(function(){


    var httpCountryCode = $.aetvInt.getCountryCode();

    if (httpCountryCode) {  // if an akamai http response cookie exists 'Country-Code' ...

        var affiliateCountryJson = $.aetvInt.getMatchingCountryJson(httpCountryCode);
        if( affiliateCountryJson != null ) {    // if that country code is a valid country ...
            if ( readCookie('country_code') == null) {    // if the client cookie is not set, i.e., the user has not decided to be sent to the www.AETV.com ...
                    $.aetvInt.initIntOverlay(affiliateCountryJson); //  create the overlay markup
            }
        }
    }
});

})( jQuery );

;
(function ($) {
  Drupal.behaviors.mvpdPicker = {
    attach: function (context, settings) {
      $('[data-behind-the-wall="true"] a', context).click(function (event) {
          if (typeof aetn == 'object' &&
                          typeof parent.aetn.adobePass == 'object' &&
                          typeof parent.aetn.adobePass.page == 'object' &&
                          typeof parent.aetn.adobePass.page.handlers == 'object' &&
                          typeof parent.aetn.adobePass.page.handlers.videoThumbClick == 'function'){

              var videoPageURL = $(this).attr("href");
              var mpxID = $(this).parent(['data-mpx-id']).data('mpx-id');
              if(
              typeof(aetn.videoplayer) != "undefined" && aetn.videoplayer !== null) {
                var containerObject = $('#' + aetn.videoplayer.container);
                var playerObject = aetn.videoplayer;
              } else {
                var containerObject = null;
                var playerObject = null;
              }
              if (videoPageURL && mpxID && !aetn.video.adobePass.isAuthenticated()){
                  event.stopImmediatePropagation();
                  event.preventDefault();
                  parent.aetn.adobePass.page.handlers.videoThumbClick( videoPageURL, mpxID, event, playerObject, containerObject );
              }
          }

      });
    }
  };

})(jQuery);
;
