(function() {
	var iframeId = 1;

	function aetn_dart_update_sz(tag){
		var sz = tag.match(/sz=([^;]*)+/i);				
		if (sz[1]) {
			var szArray = sz[1].split(",");
			var szParam = '';
			var offset = 175;
			if (typeof Drupal !== "undefined" && Drupal.settings
					&& Drupal.settings.aetn_dart
					&& Drupal.settings.aetn_dart.maxHeightOffset) {
				offset = Drupal.settings.aetn_dart.maxHeightOffset;
			}
			var bodyHeight = jQuery("body").outerHeight();		   
			var maxHeight = bodyHeight - offset;
			for ( var i = 0; i < szArray.length; i++) {
				var dim = szArray[i].split("x");
				var height = parseInt(dim[1]);
				if (height && !(isNaN(height))) {
					if (height < maxHeight) {
						if((i+1 == szArray.length)) {	
							szParam += szArray[i] 
						}
						else{
							szParam += szArray[i] + ",";
						}
					}
				}
			}
			if (szParam) {
				//remove any extra comma ,
				szParam = szParam.replace(/,$/,'');
				tag = tag.replace(/sz=[^;]*/, "sz=" + szParam);			
			}
		}
		return tag;
	}
	
	jQuery(document).bind('dart_tag_render', function(event, tag) {

		tag = aetn_dart_update_sz(tag);
		var tagArray = tag.match(/^(<)(.+ src=\")(http:\/\/ad.doubleclick.net\/+)(.*)/i);

		if (tagArray) {
			var ad_path = tagArray[4];
			var search = /aetni/;
			var method_found = search.test(ad_path);
			if (method_found) {
				var paths = ad_path.split('aetni/');
				var dfp = paths[0].replace(/\/$/, "");
				var path_split = paths[1]; // contains path
				var divID = "aetn_dart_" + iframeId;

				if (dfp) {
					tag = '<iframe id="' + divID + '" ' + tagArray[2]
					+ Drupal.settings.aetn_dart.aetnIframeUrl + "?dfp=" + dfp
					+ "&path=" + encodeURIComponent(path_split);
				} else {
					tag = '<iframe id="' + divID + '" ' + tagArray[2]
					+ Drupal.settings.aetn_dart.aetnIframeUrl + "?path="
					+ encodeURIComponent(path_split);
				}
			}
		}

		if (tag.match(/op=true/) || tag.match(/op%3Dtrue/)) {
			var iframe_id = iframeId;
			tag = '<iframe id="aetn_dart_' + iframe_id + '" ' + tagArray[2]
			// + Drupal.settings.aetn_dart.aetnIframeUrl + "?path="
			+ Drupal.settings.aetn_dart.aetnIframeUrl + "?dfp=" + dfp + "&path="
			+ encodeURIComponent(path_split);

			var old_tag = tag;
			tag = '<div id="aetn_dart_' + iframe_id
			+ '"></div> <iframe style="display:none;" src="about:blank';
			var intext = {};
			intext[iframe_id] = old_tag + '">';
            jQuery.extend(Drupal.settings.aetn_dart, {
				"inter_ad" : intext
			});
		}

		iframeId++;

		return tag;
	});
})();

(function($) {
	dartWallpaperRender = function(options) {
		$(document).ready(
				function() {
				  
				  $("<style>")
			    .prop("type", "text/css")
			    .html("\
			      .body-background {\
			        background: " + options.background_color + 
			        		" url(" + options.image_url + ") " +
			        		" top center " + options.background_tiling + " fixed !important"
			    ).appendTo("head");
				  
					$("body").addClass("body-background");
					$("body").css("cursor", "pointer");
					$(".masthead > div").css("cursor", "default");
					$("#main > div").css("cursor", "default"); // make the main body
					// section not appear
					// clickable
					$("#wrapper-content-bottom > div").css("cursor", "default");
					$("#footer-wrapper > div").css("cursor", "default");
					$("body").unbind("click"); // remove all body click event
					$("body").bind("click", function(e) {
						if (e.target.tagName.toLowerCase() == "body") {
							window.open(options.link_url);
						}
					});
				});
	}
	Drupal.dartWallpaperRender = dartWallpaperRender;
})(jQuery);
;
