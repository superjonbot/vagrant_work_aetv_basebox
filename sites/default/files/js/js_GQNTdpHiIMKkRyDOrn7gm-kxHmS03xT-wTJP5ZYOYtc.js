/**
 * aetn.lib.ad - module to provide additional ad helper methods including refreshAds
 *  
 */

var aetn = aetn || {};
aetn.lib = aetn.lib || {};

aetn.lib.ad = (function(my, $) {
	/**
	 * @function refreshAds() Function called to refresh an ad or ads.
	 * 
	 *  
	 */
	my.refreshAds = function() {
		var adsToRefresh = $('iframe[id^="aetn_dart_"]');	
		var newRand = Math.floor(Math.random() * 100000000);
		jQuery.each(adsToRefresh, function(i, ifrm) {
			ifrm.src = ifrm.src.replace(/ord%3D([0-9]+)/i, 'ord%3D' + newRand);
		});		
	}
	
  my.getInterAds = function(){
    var intadsToRefresh = $('div[id^="aetn_dart_"]');	
		jQuery.each(intadsToRefresh, function(i, ifrm) {      
		  var id = $(ifrm).attr('id');      
			var inter_id = id.replace('aetn_dart_', '');
			$(ifrm).html(Drupal.settings.aetn_dart.inter_ad[inter_id]);
		});
  }
  
  my.clearInterAds = function(){
    var intadsToRefresh = $('div[id^="aetn_dart_"]');	
		jQuery.each(intadsToRefresh, function(i, ifrm) {            
		  $(ifrm).html('');
		});
  }

  my.getInterAdsContent = function(ad_placeholder){
      //alert('getInterAdsContent');
    var intadsToRefresh = $('div[id^="aetn_dart_"]');

      //alert(intadsToRefresh.length);

		jQuery.each(intadsToRefresh, function(i, ifrm) {
		  var id = $(ifrm).attr('id');

            //alert('whoamI?'+id);
		  var inter_id = id.replace('aetn_dart_', '')

           // alert('whoamInow?'+inter_id);


           // alert(ad_placeholder);
          //$("#"+ad_placeholder).html(Drupal.settings.aetn_dart.inter_ad[inter_id]);

            if(!window.Drupal.settings.dfp_interstitial){
            window.Drupal.settings.dfp_interstitial=$("#dfp-ad-gallery_interstitial").detach();



            };

            $( "#"+ad_placeholder).empty();
            window.Drupal.settings.dfp_interstitial.appendTo( "#"+ad_placeholder );
            googletag.pubads().refresh();
//
//
//            //googletag.pubads().set("adsense_background_color", "FFFFFF");
//            googletag.defineSlot("dfp_300x250_general_2", [300, 250], "dfp-ad-dfp_300x250_general_2")
//                     //.addService(googletag.pubads());
//
//            //googletag.pubads().enableSyncRendering();
//
//
//          $("#"+ad_placeholder).html('<div id="dfp-ad-dfp_300x250_general_2" style="width: 320px; height: 250px">HI</div>');
//            googletag.pubads().enableSingleRequest();
//            googletag.enableServices();
//            googletag.display("dfp-ad-dfp_300x250_general_2");



//            <div id="div-gpt-ad-1389885200465-0" style="width:180px;    height:150px;"><script type="text/javascript">googletag.cmd.push(function() { googletag.display("div-gpt-ad-1389885200465-0"); });</script></div>


        });
	my.refreshAds();
  }
  
  my.ifrmResize = function(w, h, ad_size_id, ad_id, duration) {
      var divId = 'aetn_dart_300x250';
      var targetFrame = jQuery("#" + divId);
      if (targetFrame.size() <= 0) {
        jQuery(window).trigger('resize'); //Trigger window "resize" event since the iframe has been resized.    	  
        return;
      }
      var width = parseInt(w);
      if (isNaN(width) || width < 0) {
        width = targetFrame.width();
      }
      var height = parseInt(h);
      if (isNaN(height) || height < 0) {
        height = targetFrame.height();
      }
      targetFrame.width(width).height(height);   
      jQuery(window).trigger('resize'); //Trigger window "resize" event since the iframe has been resized.
      
  }	

	return my;

}(aetn.lib.ad || {}, jQuery));

//iframeBuster needs a ref in global namespace.
window.ifrmResize = aetn.lib.ad.ifrmResize;
;
/**
 * aetn.lib.ad.token - module to provide additional ad token helper methods including qc_results
 *  
 */


var aetn = aetn || {};
aetn.lib = aetn.lib || {};
aetn.lib.ad = aetn.lib.ad || {};

aetn.lib.ad.token = (function(my, $) {
    var quantSegs='';
    var preQuantSeqs ='';
    my.qc_results = function(result) {
        //preQuantSeqs='Q=MM:Q=100:Q=AA:Q=3549:Q=NC:Q=HHWK:';//test purpose only
        for (var i = 0; i < result.segments.length; i++) {
            if(result.segments[i].id.length >1)
                preQuantSeqs += 'Q=' + result.segments[i].id + ':'; //customizable per your ad server
        }
        createCookie('ckQuantSegs',preQuantSeqs, 1); //set to expire in 24 hours (if the cookie does not already exist)
        quantSegs = preQuantSeqs.replace(/:/g,';'); 
    }
    if(readCookie('ckQuantSegs')==null || readCookie('ckQuantSegs') == ''){
        // Use non-blocking Javascript call
        var js = document.createElement('script');
        js.type = 'text/javascript';
        js.src = 'http://pixel.quantserve.com/api/segments.json?a=p-84eTroxoNX3JE&callback=aetn.lib.ad.token.qc_results';
        document.getElementsByTagName('head')[0].appendChild(js);       
    }else{
        quantSegs = readCookie('ckQuantSegs').replace(/:/g,';');
    }

    my.getQuantSegs = function (){
    	if(quantSegs) {
          quantSegs = ";" + quantSegs;
    	}
        return quantSegs;
    }

    return my;
}(aetn.lib.ad.Dart || {}, jQuery));


    function createCookie(name,value,hours) {
        if (hours) {
            var date = new Date();
            date.setTime(date.getTime()+(hours*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

/* following code : TBD ### Sue Park 05/08/2013
    var pageKeywords = document_keywords();
    var qs = new Querystring();
    var keyword = qs.get('keywords','');
    keyword = cleanupKeywords(keyword);
    var pid = getPid(document.location);
    var mvStr = composeMvString(pageKeywords,pid,keyword,aetn.lib.ad.token.getQuantSegs());
    mvStr = ";"+mvStr;
*/
    function Querystring(qs)
    { // optionally pass a querystring to parse
            this.params = {};

            if (qs == null) qs = location.search.substring(1, location.search.length);
            if (qs.length == 0) return;

            // Turn <plus> back to <space>
            // See: http://www.w3.org/TR/REC-html40/interact/forms.html#h-17.13.4.1
            qs = qs.replace(/\+/g, ' ');
            var args = qs.split('&'); // parse out name/value pairs separated via &

            // split out each name=value pair
            for (var i = 0; i < args.length; i++) {
                var pair = args[i].split('=');
                var name = decodeURIComponent(pair[0]);

                var value = (pair.length==2) ? decodeURIComponent(pair[1]) : "";

                this.params[name] = value;
            }
    }

    Querystring.prototype.get = function(key, default_)
    {
            var value = this.params[key];
            return (value != null) ? value : default_;
    }

    Querystring.prototype.contains = function(key)
    {
            var value = this.params[key];
            return (value != null);
    }

    function document_keywords(){
        var keywords = '';
        var metas = document.getElementsByTagName('meta');
        if (metas) {
            for (var x=0,y=metas.length; x<y; x++) {
                if (metas[x].name.toLowerCase() == "keywords") {
                    keywords += metas[x].content;
                }
            }
        }
        return keywords;
    }


    function cleanupString(str)
    {

        str = str.replace(/&amp;/g,'');
        str = str.replace(/[ ]/g,'_');
        str = str.replace(/[<>"()\[\]=\*\.,#%\/&]/g,'');
        return str;
    }

    function cleanupKeywords(str)
    {
            str = str.replace(/[ ]/g,'+');
            str = cleanupString(str);
            if (str.length > 50)
                str = str.substring(0,45);

            return str;
    }

    function composeMvString(str,pid,keyword,quantSegs)
    {
        if ((pid.length + keyword.length +quantSegs.length) < 140 )
            return parseMvParameters(str,180);
        else
            return parseMvParameters(str,102);
    }

    function parseMvParameters(str,max)
    {
        //alert("in parse mv, str=" + str);
        var mv_array = str.split(',');
        var mvString = '';
        for (var i = 0; i < mv_array.length; i++)
        {
            if (mv_array[i] == '' || mv_array[i] == ' ')
                continue;
            //trim the white space first
            var eachMv = mv_array[i].replace(/^\s+/, '').replace(/\s+$/, '');
            eachMv = cleanupString(eachMv);

            if (mvString.length + eachMv.length + 4 > max)
                break;
            mvString += 'mv=' + eachMv + ';';
        }
        return mvString;
    }
    
    function getPid(str)
    {
     str = decodeURIComponent(str);                
        var pageId = str.replace(/^http[s]?:\/\/(.*?[\/])/, '').replace(/[\.](\w){2,3}($)?/, '').replace(/[\/&?= ]/g, "_");
        return cleanupString(pageId).substring(0, 124);
   }

;
/**
 * 
 * AETV Modal functionality
 * 
 * Usage: jQuery('SELECTOR').aetvModal( { urlPrefix : '/modal', // Prefix to
 * place in front of all URLs which are being "modalized" iframeDivSelector:
 * '#modalIframe', // The div which will contain the target iFrame. If not
 * existing it will be created. iframeContainerSelector: '#container' // Parent
 * of modalIframe div. Used if the iframeDivSelector doesn't exist. });
 * 
 */

(function($) {

    $.fn.aetvModal = function(options) {
        if (typeof $(this).on === "undefined") {
            // incompatible jQUery version.
            return;
        }

        var settings = $.extend({
            urlPrefix : '/modal',
            iframeDivSelector : '#modalIframe',
            iframeContainerSelector : '#container'
        }, options);

        //var visible = false;
        var pathname = window.location.pathname;
        var title = document.getElementsByTagName("title")[0].innerHTML;
        var modalTitle = "Modal";

        var waa = window.aetv.aetvModal || window;
        waa.modalVisible = false;
        waa.modalReferrer = pathname;
        //waa.modalReferrerId = History.getStateId();

        var resizeModal = function() {
            // if $iframeDiv is hidden we should just return right away.
            if (!window.aetv.aetvModal.modalVisible) {
                return;
            }
            $iframeDiv.css({
                "width" : $container.width(),
                //"height" : $container.height() + 8
                "height" : $container.height()
            });
            $.modal.resize();
        };

        $(window).resize(function() {
            resizeModal();
        });

        var getModalSource = function(src) {
            if (src.indexOf('http') == '0') {
                console.log('jQuery.aetvModal currently only works on local paths');
                return false;
                // src = src.replace(/(http(s)?)\/\/\:[^\/])?(\/.+)/, function());
            } 
            else {
                if (src.indexOf("/") != 0) {
                    src = "/" + src;
                }
                src = settings.urlPrefix + src;
            }
            return src;
        };

        var $iframeDiv = $(settings.iframeDivSelector);
        var $container = $(settings.iframeContainerSelector);

        if ($iframeDiv.length != 1) {
            var aetv_modal = $('<div>').addClass('aetv_modal')
            .css("display","none")
            .attr('id', settings.iframeDivSelector.substring(1));  

            aetv_modal.append($('<iframe>').attr('src', 'about:blank').attr('id','modal_iframe')
                            .attr("allowfullscreen", "true")
                            .attr("mozallowfullscreen", "true")
                            .attr("webkitallowfullscreen", "true")
                            .css("width", "100%")
                            .css("height", "100%")
                            .attr("allowtransparency", true));
            /*
  aetv_modal.append($("<div>").addClass("close_modal").on("click", 
        function() {
          $.modal.close();
          $iframe.src = "about:blank";
          window.modalIframe = $iframe;
          window.aetvModalVisible = false;

          if (History) {
            History.pushState({}, title, pathname);
          } 
        }
      ));
             */
            $container.after(aetv_modal);
            $iframeDiv = $(aetv_modal);
        }
        var $iframe = $("iframe", $iframeDiv)[0];
        waa.modalIframe = $iframe;

        return $(this).on("click", "a", function(e) {
            if (this.hostname != location.hostname){
                return;
            }

            e.preventDefault();
            e.stopImmediatePropagation();

            var modalPath = this.pathname.toLowerCase() + this.search;
            var modalSource = getModalSource(modalPath);
            var waa = window.aetv.aetvModal || window;

            if ($('.qtip').length) {
                $('.qtip').hide();
            }

            $iframeDiv.modal({
                fadeDuration : 2000,
                opacity: .93
            });
            $($iframe).css("visibility", "hidden").css("opacity",0);
            $iframe.src = modalSource;
            $($iframe).on("load.aetv_modalIFrame", function(){
                var $this = $(this);
                $this.off('load.aetv_modalIFrame');
                $(this).css('visibility', "visible").fadeTo(500, 1);
            });

            waa.modalIframe = $iframe;
            waa.modalVisible = true;

            resizeModal();

            // pause hero if on homepage
            if (typeof aetv != 'undefined' &&
                            aetv.site &&
                            aetv.site.pages &&
                            aetv.site.pages.HomePage &&
                            aetv.innerPage &&
                            aetv.innerPage.thePage &&
                            aetv.innerPage.thePage.pause_promos == 'function') {
                aetv.innerPage.thePage.pause_promos();
            }

            if (History) {
                if ($('div.landing-episode-tile-video-title', $(this))) { 
                    modalTitle = $('div.landing-episode-tile-video-title', $(this)).html();
                }

                //alert('from jquery aetv modal, state id ='+History.getStateId());
                History.pushState(null, modalTitle, modalPath);
            }
        });
    };
}(jQuery));
;
