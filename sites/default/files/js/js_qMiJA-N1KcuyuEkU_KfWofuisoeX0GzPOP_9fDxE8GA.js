/** GPT lazy loader for AETV 6.23.15 **/
jQuery(document).ready(function ($) {
    window.mobileWrapper=jQuery('#wrapper').hasClass('mobile_wrapper');
    window.isthisAdLoaded=new Array(); //holder for loaded ads
    window.myScrollDebounce=undefined;
    var isSpecialCase= function (){
        var specialcase=undefined;
        var lazyscrollerDATA=undefined;
        var thisURL=window.location.pathname;
        var thisURLobj=thisURL.split('/');
        var arg0 = thisURLobj[1];
        var arg1 = thisURLobj[2];
        var arg2 = thisURLobj[3];

        function isset(value){
            return value!=undefined;
        };

        if( !isset(arg0)||arg0=='index'){
            specialcase='homescreen';
            var lazyscrollerDATA={
                innerContainer:jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page'),
                tileSelector:jQuery('.homepage_ad>.dfp-tag-wrapper')
            };
            if(window.mobileWrapper){
                lazyscrollerDATA.tileSelector=jQuery('.homepage_ad>.dfp-tag-wrapper');
            }
        }
        else if( arg1=='exclusives'){
            specialcase='exclusives';

                lazyscrollerDATA={
                    innerContainer:jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page'),
                    tileSelector:jQuery('.exclusive-panel>.dfp-tag-wrapper')
                };

        }
        else if( arg0=='shows'){
            specialcase='shows_landing';//landing
            var lazyscrollerDATA={
                innerContainer:jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page'),
                tileSelector:jQuery('.allshows_ad>.dfp-tag-wrapper')
            };
        }
        else if( arg0=='video'){
            specialcase='video_landing';//landing
            var lazyscrollerDATA={
                innerContainer:jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page'),
                tileSelector:jQuery('.video-landing-tile>.dfp-tag-wrapper')
            };
        }
        else if( arg0=='games'){
            specialcase='games_landing'; //landing
            var lazyscrollerDATA={
                innerContainer:jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page'),
                tileSelector:jQuery('.games_ad>.dfp-tag-wrapper')
            };
        }
        else if( arg1=='video'){
            specialcase='video';
            var lazyscrollerDATA={
                innerContainer:jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page'),
                tileSelector:jQuery('.video-landing-tile>.dfp-tag-wrapper')
            };



        }
        else if( arg1=='games'){
            specialcase='games';
        }
        else if( arg1=='pictures'){
            specialcase='pictures';
            lazyscrollerDATA={
                innerContainer:jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page'),
                tileSelector:jQuery('.tile>.dfp-tag-wrapper')
            };
            if(window.mobileWrapper){
                specialcase='mobile_pictures';


               // if( !jQuery('.inner-container').hasClass('mobile-gallery-landing')){
                    lazyscrollerDATA.tileSelector=jQuery('.gallery-ad>.dfp-tag-wrapper');
               // lazyscrollerDATA.tileSelector=jQuery('.dfp-tag-wrapper');
               // };


            }


        }
        else if( arg1=='cast'||arg1=='participants-and-experts'){
            specialcase='cast';
            var isaCastBioPage=jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page').hasClass('cast-bio-page');
                lazyscrollerDATA={
                innerContainer:jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page'),
                tileSelector:jQuery('.cast-panel>.dfp-tag-wrapper')
            };

            if(isaCastBioPage){
                lazyscrollerDATA={
                innerContainer:jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page'),
                tileSelector:jQuery('.data-tile>.dfp-tag-wrapper')
            };

                if(window.mobileWrapper){
                    lazyscrollerDATA.tileSelector=jQuery('.tile>.dfp-tag-wrapper');
                }

            }




        }
        else {
            specialcase='not defined';
            var isAnEpisodePage=jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page').hasClass('episode-page')||jQuery('#container').hasClass('mobile-episode-guide');
            var isTheHomepage=jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page').hasClass('homepage')||jQuery('#content_wrap').hasClass('homepage');
            if(isAnEpisodePage){
                console.log(':::isAnEpisodePage')
                lazyscrollerDATA={
                    innerContainer:jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page'),
                    tileSelector:jQuery('.tile>.dfp-tag-wrapper')
                };
            }
            if(isTheHomepage){
                console.log(':::isTheHomepage')
                lazyscrollerDATA={
                    innerContainer:jQuery('#container>div.inner-wrapper>div.inner-container>div.inner-page'),
                    tileSelector:jQuery('.homepage_ad>.dfp-tag-wrapper')
                };
                if(window.mobileWrapper){
                    lazyscrollerDATA.tileSelector=jQuery('.homepage_ad>.dfp-tag-wrapper');
                }
            }
        }



        //catch everything else
        if(lazyscrollerDATA!=undefined&&lazyscrollerDATA.tileSelector.length==0){
            lazyscrollerDATA.tileSelector=jQuery('.dfp-tag-wrapper');
        }

        return {specialcase:specialcase,lazyscrollerDATA:lazyscrollerDATA}
    }  //   returns the special case scenario and lazy loading data if any, if the data is empty then it will not lazy load and load all ads at once

    function cleanThis(getString) {
        getString = getString.replace(' ', ''); // Replaces all spaces with hyphens.
        getString = getString.replace('modal', ''); // remove the word 'modal'
        getString = getString.replace(/[^A-Za-z0-9]/g, ''); // Removes special chars.
        return getString;
    }  //cleans vars, same as php version in the base template

    function getSLOTS(lazyscrollerDATA){

        innerContainer=lazyscrollerDATA.innerContainer
        GPT_slots=new Array();
        lazyscrollerDATA.tileSelector.each(function(){
            var currentTileID=jQuery(this).attr('id')
            var currentTileIDstripped = currentTileID.replace("dfp-ad-", "").replace("-wrapper", ""); //remove wrapper module defined
            GPT_slots.push({
                id:currentTileID,
                tag:currentTileIDstripped,
                elementPos:function(){return (window.mobileWrapper)? (Math.floor(jQuery('#'+currentTileID).offset().top)-Math.abs(window.scrollY)):Math.floor(jQuery('#'+currentTileID).offset().left)},
                size:(window.mobileWrapper)? Math.floor(jQuery('#'+currentTileID).height()) :Math.floor(jQuery('#'+currentTileID).width()),
                load: function(){
                    try {
                        // patch for PJAX - resets some vars for pjax pages

                        var tHisCASE=isSpecialCase();
                        var getPath=window.location.pathname;
                        var getPID=cleanThis(getPath);
                        if(getPID==''||getPID==undefined){getPID='aetvhome'};

                        switch(tHisCASE.specialcase){
                            case 'exclusives':
                                googletag.pubads().setTargeting("ptype",'exclusives');
                                break;
                            case 'pictures':
                                googletag.pubads().setTargeting("ptype",'photos');
                                break;
                            case 'cast':
                                googletag.pubads().setTargeting("ptype",'meet');
                                break;
                            case 'video':
                                googletag.pubads().setTargeting("ptype",'video');
                                break;
                            case 'not defined':
                                googletag.pubads().setTargeting("pid",getPID);
                                googletag.pubads().setTargeting("s1",getPID);
                                break;
                        }

                        googletag.pubads().setTargeting("pid",getPID);

                        googletag.pubads().refresh([googletag.slots[currentTileIDstripped]]);
                    }
                    catch(err) {
                        //pjax patch failed
                    }
                    try {
                        console.log('attempting to fix any isotope');
                        setTimeout(function(){jQuery('.isotope').isotope( 'reloadItems' ).isotope();},0);
                    }
                    catch(err) {
                        //no isotope
                    }
                }

            });
        })
        return GPT_slots;
    };
    function forceLoad(index){
        GPT_slots=getSLOTS(lazyscrollerDATA);
        var value= GPT_slots[index];
        try {
            if(!isthisAdLoaded[index]){
                console.log(':::successfully loaded: '+GPT_slots[index].id);
                isthisAdLoaded[index]=true;
                value.load();
            };
        }
        catch (e) {
            // statements to handle any exceptions
            console.log('::: no ads set on load for this page')
        }
    };

    function lazyLoad (lazyscrollerDATA){
        var innerContainer=lazyscrollerDATA.innerContainer;
        var parentContainer=innerContainer.parent();
        var loadWhoIsinVIEW=function(){
            console.log('::: loadWhoIsinVIEW')
            GPT_slots=getSLOTS(lazyscrollerDATA);
            var containerPos=0;//Math.abs(innerContainer.position().left);
            var containerSize=(window.mobileWrapper)? jQuery(window).height():Math.abs(parentContainer.width());
            jQuery.each(GPT_slots,function(index,value){
                objMin=value.elementPos();
                objMax=(value.elementPos()+value.size);
                bracketMin=(containerPos-(value.size/2));
                bracketMax=(containerPos+containerSize+(value.size/2));
                showThisSlot= (objMin>bracketMin)&&(objMax<bracketMax);

                if(showThisSlot){
                    forceLoad(index);
                }
                console.log('::: '+value.id+' pointA:'+objMin+' pointB:'+objMax+ ' this bracketA:'+bracketMin+' bracketB:'+bracketMax + ' show?'+showThisSlot);
            });
        }   // check what is in view and load it

        //START LISTENERS
        if(window.mobileWrapper){
            jQuery(document).unbind('scroll').on( 'scroll', function(){
                clearTimeout(myScrollDebounce);
                myScrollDebounce= setTimeout(loadWhoIsinVIEW,250);
            });  //starts vertical listener
        } else {
            innerContainer[0].addEventListener('webkitTransitionEnd',loadWhoIsinVIEW);  //starts listener for horizontal scroll
            window.addEventListener("resize", refreshADLOADlogic_debounce, false);   //starts resize listener
        }

        loadWhoIsinVIEW();//check what is in view if any on first load
    }
    window.startADLOADlogic=function(){
        window.specialcase=isSpecialCase().specialcase;
        window.lazyscrollerDATA=isSpecialCase().lazyscrollerDATA;
        console.log('dfp::: case:'+specialcase+' action:'+   (lazyscrollerDATA!=undefined?'lazyloading':'not lazy'));

        if  (window.specialcase=='mobile_pictures'&&!jQuery('.inner-container').hasClass('mobile-gallery-landing')){
            console.log("::: Don't do anything for mobile pictures");

        } else {


        if(lazyscrollerDATA!=undefined ){
            lazyLoad (lazyscrollerDATA);   //start lazyloading
        }  else {
            setTimeout(function(){
                try {
                    //refreshes all GPT ads
                    googletag.pubads().refresh();
                }
                catch(err) {
                    //gpt ads refresh fail
                }
                //clean up isotope if necessary
                setTimeout(function(){jQuery('.isotope').isotope( 'reloadItems' ).isotope();},2000);
                setTimeout(function(){jQuery('.isotope').isotope( 'reloadItems' ).isotope();},4000);

            },1000);
        }

        }


    }
    window.refreshADLOADlogic=function(){
        window.isthisAdLoaded=new Array();
        window.startADLOADlogic();
    } //refresh for pjax loaded pages
    $('body').on('contentLoaded',function(){
        console.log('dfp::: refresh Ads');
        window.refreshADLOADlogic();
    });//listen for load & pjax
    var refreshADLOADlogic_debounce = _.debounce(function(e) {
        console.log('dfp::: refresh Ads bec resize');
        window.startADLOADlogic();
    }, 500);  //debounce function for vertical scrolling


    console.log('dfp::: lazyloadADs.js loaded');






});



;
/*
 * Google CSE glue module. Uses cse js API
 *
 */
var aetv = aetv || {};

aetv.googleSearch2 = (function (module, $, LAB) {

  var debugMode,
      currentlySearching,
      searchOpen,
      searchWrapper,
      searchButton,
      searchArrow,
      searchResumeButton,
      gso,
      clearButton,
      resultsCloseButton,
      blackOut,
      socialNav;

  module.init = function () {
    LAB.script('http://www.google.com/jsapi').wait(function () {
      dlog('jsapi loaded');
      var loadOptions = {
        "callback": function() { dlog('search callback'); }
      };
      google.load('search', '1', loadOptions);
      google.setOnLoadCallback(function () {
        dlog('set on load callback');

        dlog('google cse js version 2 loaded');
        debugMode = true;
        searchOpen = false;
        currentlySearching = false;
        setVars();
        bindControls('onload');
        createWidget();

        //create a structure that I can manipulate by wrapping the box in MY div
        $('.gssb_c').wrap('<div class="aetv-cse-wrapper"></div>');
        $('#search-wrapper').appendTo('.aetv-cse-wrapper');
        $('<div class="search-icon"></div>').prependTo('td.gsc-input');
        $('input.gsc-input').attr('placeholder','Search');
        //$('.aetv-cse-wrapper').appendTo('#wrapper');

        checkForQuery();

        //$(window).click(function (e) { dlog('searchOpen='+searchOpen+':'+e.srcElement); });
      });
    });
  }

  function setVars() {
    searchButton = $('#google-search .search'),
    searchResumeButton = $('#google-search .search-resume'),
    searchWrapper = $('#google-search .search-wrapper'),
    searchArrow = $('.search-arrow'),
    clearButton = $('.gsc-clear-button'),
    blackOut = '<div class="gsc-modal-background-image gsc-modal-background-image-visible" tabindex="0"></div>', 
    resultsCloseButton = '<div class="gsc-results-close-btn gsc-results-close-btn-visible" tabindex="0"></div>',
    searchWrapperMask = '<a class="search-wrapper-mask"></a>';
    socialNav = $("#header_social");
  }

  function searchComplete() {
    var acBox = $('.gsc-results-wrapper-nooverlay');
    acBox.show();
    acBox.css('border', '3px solid #ccc');
//    showArrow();

    var term = gso.getInputQuery();
    if (term != "") {
      saveSearchTerm(term);
    }
    showResultHeader(term);

    //add in the results close button
    if ($('.gsc-results-close-btn').length < 1) {
      $(resultsCloseButton).appendTo('.gsc-results-wrapper-nooverlay');
    }
    
    //add in thickbox overlay
    if ($('.gsc-modal-background-image').length < 1) {
      $(blackOut).insertAfter('.gsc-results-wrapper-nooverlay');
    }

    //mask the input field separately so we can target a click there
    if ($('.search-wrapper-mask').length < 1) {
      $(searchWrapperMask).insertAfter('.gsc-results-wrapper-nooverlay');
      bindControls('searchDisplayed');
    }

    //make all the links go to target _top
    $('.gsc-table-result a').each(
      function () { 
        if (typeof($(this).attr('target')) != 'undefined') { 
          $(this).attr('target', '_top'); 
        } 
      }
    );
  }

  function adjustResultsBoxHeight() {
    var adjHeight = Math.round($('#wrapper').height() * .80);
    $('.gsc-results-wrapper-nooverlay').css('height', adjHeight+'px');
    $('.gsc-wrapper').css('height', adjHeight+'px');
  }

  function searchStarting() {
    //dlog('search starting event');
    //decide how tall the result box will be
    adjustResultsBoxHeight();
  }

  function saveSearchTerm(term, src) {
    //dlog('SAVING '+term);
    $.cookie('gst', term, {'domain':document.location.hostname});
  };

  function hideArrow() {
    $('.search-arrow').hide();
  }

  function showArrow() {
    if ($('.gsc-results-wrapper-nooverlay').length > 0) {
      $('.search-arrow').show();
    }
  };

  function showResultHeader(term) {
    if ($('.gsc-result-info').length > 0) {
      term = 'Search Results for "'+term+'"';
      if ($('.gsc-search-result-hdr').length < 1) {
        $('<div class="gsc-search-result-hdr"><strong>'+term+'</strong></div>').insertBefore('.gsc-result-info');
      }
      else {
        $('.gsc-search-result-hdr strong').text(term);
      }
    }
  }

  function createWidget() {
  	dlog('attempting to create widget');
    var cseId = Drupal.settings.aetv_google_cse.googleSearchCseId;
    var drawOptions = new google.search.DrawOptions();
    drawOptions.setAutoComplete(true);
    gso = new google.search.CustomSearchControl(cseId, {});
    dlog(gso);

    gso.setSearchCompleteCallback({}, searchComplete);
    gso.setSearchStartingCallback({}, searchStarting);
    gso.draw('google-cse', drawOptions);
  }

  function bindControls(which) { 
    switch (which) {
      case 'onload':

          try {
              console.log('attempting to fix any isotope');
              setTimeout(function(){jQuery('.isotope').isotope( 'reloadItems' ).isotope();},0);
          }
          catch(err) {
              //no isotope
          }


        searchButton.click(function (e) {
          dlog('search button clicked');
          $('.aetv-cse-wrapper').addClass('open');
          // nasty fix for unclickable scroller buttons
          $('#header #header_tools').removeClass('search-closed');
          dlog('searchOpen='+searchOpen);
          if (!searchOpen) {
            openSearch();
          }
          else {
            closeSearch();
          }
          e.preventDefault();
          return false;
        });

        searchResumeButton.click(function (e) {
          dlog('search resume button clicked');
          // nasty fix for unclickable scroller buttons
          $('#header #header_tools').removeClass('search-closed');
          var term = getLastSearch();
          if (term) {
            openSearch(term);
            e.preventDefault();
          }
          return false;
        });

        $(window).resize(_.debounce(function () {
          adjustResultsBoxHeight();
        }, 500));
        break;

      case 'searchDisplayed':
        $('.gsc-modal-background-image').bind('click', function () {
          clearSearchResults();
        });

        $('.gsc-results-close-btn').bind('click', function () {
          clearSearchResults();
        });

        $('.search-wrapper-mask').bind('click', function () { 
          clearSearchResults(true);
        });
//        $('.aetv-cse-wrapper').css('z-index', 0);
        $('#google-search .search-wrapper').css('top','6px').css('right','50%');
        break; 
      
      case 'typingSearch':
        var searchInput = $('#gsc-i-id1');
        var clearButton = $('.gsc-clear-button');
        var acBox = $('.gsc-completion-container');

        //acBox.css('border', '1px solid #bbb');
        //acBox.addClass('gs-rounded-corners');

        //move the autocomplete box 
        searchInput.bind('keyup', function (e) {
          var acBox = $('.gsc-completion-container');
          var acRcCRows = $('.gssb_a');
          var adjWidth;
          currentlySearching = true;

          //$('.aetv-cse-wrapper').css('z-index', 1002);
//          $('.aetv-cse-wrapper').css('z-index', 9999);
//          $('#google-search .search-wrapper').css('top','75px').css('right','10px');

          dlog('currentlySearching = true');
          dlog('acBox width='+acRcCRows.width());
//          if (acRcCRows.width()) {
//            adjWidth = acRcCRows.width() + 20;
//          }
//          else {
//            adjWidth = 100;
//          }
          dlog('adjusted width = '+adjWidth);
//          var styles = "width: "+adjWidth+"px !important; display: block; left:auto; right:10px; position: absolute;"
//          $('.gssb_c').get(0).setAttribute('style', "");
          //$('.gssb_c').get(0).setAttribute('style', styles);

//          $('.gssb_c').addClass('gs-rounded-corners');

//          acBox.css('width', adjWidth+'px');

          focusField();
        });

        acBox.bind('mouseleave', function () {
//          $('.aetv-cse-wrapper').css('z-index', -1);
          currentlySearching = false;
        });

//        searchWrapper.bind('mouseleave', function () {
//          if (!currentlySearching && searchOpen || searchInput.val() == "") {
//            closeSearch();
//          }
//        });
        clearButton.bind('click touch', function() {
		  closeSearch();
		});
        break;
    }

    dlog('controls loaded for '+which);
  };

  function clearSearchResults(keepSearching) {
    //dlog('background clicked!');
    $('.gsc-results-wrapper-nooverlay').hide();
    $('.gsc-modal-background-image').remove();
    hideArrow();
    gso.clearAllResults();

    if (!keepSearching) {
      currentlySearching = false;
      closeSearch();
    }
    else {
      currentlySearching = true;
      focusField();
    }
    $('.search-wrapper-mask').unbind('click');
    $('.search-wrapper-mask').remove();
    $('#google-search .search-wrapper').css('top','75px').css('right','10px');
  }

  function showResumeSearch() {
    dlog('show resume search button');
    searchButton.css('left', '-27px');
    searchWrapper.hide();
    searchResumeButton.show();
//    $('.aetv-cse-wrapper').css('z-index', 0);
  };

  function getLastSearch() {
    if ($.cookie('gst') != null) {
      return $.cookie('gst');
    }
    else {
      return false;
    }
  };

  function termIsSaved() {
    return getLastSearch();
  };

  function checkForQuery() {
    var termFound = termIsSaved();
    dlog('termfound='+termFound);
    if (termFound) {
      showResumeSearch();
    }
  };

 function focusField() {
    var searchInput = $('#gsc-i-id1');
    searchInput.focus();
  };

  function executeSearch(term) {
    if (typeof(term) != 'undefined') {
      dlog('term was provided. it is: '+term);
      currentlySearching = true;
      gso.prefillQuery(term);
      focusField();
      dlog('attempting to trigger search');
      gso.execute(term, 1, '');
      searchOpen = true;
    }
  };

  function openSearch(term) {
    dlog('open search. term = '+term);
    var searchInput = $('#gsc-i-id1');
    var clearButton = $('div.gsc-clear-button');
//    clearButton.hide();
	
    searchOpen = true;

    searchInput.hide();
    searchWrapper.show();
    searchResumeButton.hide();
    //searchResumeButton.css('left', '4px'); 
    
    //socialNav.removeClass('open');
    searchWrapper.animate(
      {width: '500px'}, 
      1000, 
      function () {
        //searchInput.css('width','180px');
        searchInput.show();
//        clearButton.fadeIn(1000, function () {
//          $(this).css('display','inline');
//        });

        bindControls('typingSearch');

//        $('.aetv-cse-wrapper').css('z-index', 0);

        if (term) {
          executeSearch(term); 
        }
        else {
          focusField();
        }
      }
    );
  };

  function closeSearch() {
    dlog('close search');
    // nasty fix for unclickable scroller buttons
//    $('#header #header_tools').addClass('search-closed');
    var btnWidth = '0';
    var searchInput = $('#gsc-i-id1');
    var clearButton = $('.gsc-clear-button');
    currentlySearching = false;

//    $('.aetv-cse-wrapper').css('z-index', -1);
	$('.aetv-cse-wrapper').removeClass('open');

//    clearButton.hide();
    if (!_.isUndefined(gso) && gso.getInputQuery() != "") {
      gso.clearAllResults();
    }
    searchOpen = false;
    searchInput.unbind('keypress');
    clearButton.unbind('click');

    if (!_.isUndefined(searchWrapper)) {
      searchWrapper.unbind('mouseleave');
    }
    searchInput.hide();

    if (termIsSaved()) {
      btnWidth = '27';
    }
    else {
      btnWidth = '1';
    }

    if (!_.isUndefined(searchWrapper)) {
      searchWrapper.animate(
        //{ width : btnWidth+'px' },
        //1000,
        function () {
          checkForQuery();
        }
      );
    }
  };

  function dlog(msg) {
    if (typeof(console) != 'undefined' && debugMode) {
      console.log('CSE: '+msg);
    }
  };
  
  module.CloseSearchBar = function(){
	  closeSearch();
  };

  return module;

}(aetv.googleSearch2 || {}, jQuery, $LAB));

(function () {
  aetv.googleSearch2.init();
})();
;
function amzn_ads(t){"use strict";try{amznads.updateAds(t)}catch(e){try{console.log("amzn_ads: "+e)}catch(a){}}}function aax_write(t,e){t.write(e),t.close()}function aax_render_ad(t){if(t.passback)return void aax_write(document,t.html);var e=t.slotSize;if(!e)return void aax_write(document,t.html);var a=e.indexOf("x"),n=e.substring(0,a),o=e.substring(a+1),r="amznad"+Math.round(1e6*Math.random());aax_write(document,'<iframe id="'+r+'" width="'+n+'" height="'+o+'" src="javascript:\'\'" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF" topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0"></iframe>');var i;try{i=document.getElementById(r);var s=i.contentWindow||i.contentDocument;s.document&&(s=s.document),aax_write(s,t.html)}catch(d){i&&(i.style.display="none")}}var amzn_console=function(){"use strict";var t={};return t.log=function(){},t}();window.console&&(amzn_console=window.console);var amznads=function(t,e,a,n){"use strict";function o(n,r,i,s,d,c,l){this.element=n,this.impId=d,this.win=c,this.doc=l,this.start=null,this.end=null,this.area=null,this.cutoff=r,this.cutoffTime=1e3*i,this.focused=null,this.timer=!1,this.timerStarted=!1,this.getDuration=s,this.reported=!1,this.above=null,this.totalTime=0,this.leftPos=null,this.topPos=null,o.prototype.calcArea=function(){try{var n=e.getElementsByTagName("body")[0],o=a.top.innerWidth||e.documentElement.clientWidth||n.clientWidth,r=a.top.innerHeight||e.documentElement.clientHeight||n.clientHeight,i=t.$jQ(a).scrollTop(),s=t.$jQ(this.element).offset().top,d=t.$jQ(this.element).height(),c=t.$jQ(a).scrollLeft(),l=t.$jQ(this.element).offset().left,g=t.$jQ(this.element).width(),u=Math.max(s,i),m=Math.min(s+d,i+r),f=m-u;f=Math.max(0,f);var h=Math.max(l,c),p=Math.min(l+g,c+o),y=p-h;y=Math.max(0,y);var T=f*y,v=g*d;return this.leftPos=l/t.$jQ(e).width(),this.topPos=s/t.$jQ(e).height(),T/v}catch(A){t.log("calcArea failed for ad id="+this.element+"; error="+A)}return 0},o.prototype.displayTime=function(){if(this.getDuration){var e=this.end-this.start;t.log(this.element+" viewed for: "+e),this.totalTime+=e}},o.prototype.adInView=function(){t.log(this.element+" IN VIEW")},o.prototype.adNotInView=function(){t.log(this.element+" NOT IN VIEW")},o.prototype.seenForTime=function(){t.log(this.element+" displayed for "+this.cutoffTime/1e3+" seconds"),t.log(" "),this.reported=!0;try{{encodeURIComponent(location.protocol+location.host+location.pathname)}}catch(e){this.firePixel('/{"adViewability":[{"error": '+JSON.stringify(encodeURIComponent("Error encoding url - "+e))+"}]}")}try{this.firePixel('/{"adViewability":[{"viewable": true }]}')}catch(e){t.log(e),this.firePixel('/{"adViewability":[{"error": '+JSON.stringify(encodeURIComponent("Error sending pixel - "+e))+"}]}")}},o.prototype.firePixel=function(e){(new Image).src=t.protocol+t.host+t.px_svc+this.impId+e+"&cb="+Math.round(1e7*Math.random())},o.prototype.getAreaTime=function(){function a(){r.reported||(r.timer=setTimeout(function(){r.seenForTime()},r.cutoffTime))}function n(){r.adInView(),r.start=new Date,a(),r.timerStarted=!0}function o(){clearTimeout(r.timer),r.timerStarted=!1,r.end=new Date,r.adNotInView(),r.displayTime()}var r=this;if(null==this.area){this.area=this.calcArea(),this.above=this.area>this.cutoff?!0:!1;try{this.firePixel('/{"adViewability":[{"above_the_fold": '+this.above+', "topPos": '+this.topPos+', "leftPos": '+this.leftPos+"}]}")}catch(i){t.log(i),this.firePixel('/{"adViewability":[{"error": '+JSON.stringify(encodeURIComponent("Error sending pixel - "+i))+"}]}")}}var s=this.calcArea();e.hasFocus()&&(0==this.focused||null==this.focused)&&s>this.cutoff&&0==this.timerStarted&&n(),e.hasFocus()?this.focused=!0:this.area>this.cutoff&&1==this.focused&&(o(),this.focused=!1),(this.area>=this.cutoff&&s<this.cutoff||this.area<this.cutoff&&s>=this.cutoff)&&(s>=this.cutoff&&0==this.timerStarted?n():s<this.area&&o()),this.area=s},o.prototype.collectData=function(){var e=this;e.getAreaTime(),t.$jQ(a).on("scroll resize focus blur",function(){e.reported||e.getAreaTime()})}}var r="https:"===e.location.protocol;return t.protocol=r?"https://":"http://",t.DEFAULT_HOST="aax.amazon-adsystem.com",t.host=t.DEFAULT_HOST,t.dtb_svc="/e/dtb/bid",t.pb_svc="/x/getad",t.px_svc="/x/px/",t.debug_mode=t.debug_mode||!1,t.MIN_TIMEOUT=0,t.DEFAULT_TIMEOUT=1e3,t.targetingKey="amznslots",t.vidKey="amzn_vid",t.tasks=t.tasks||[],t.$jQ=t.$jQ||null,t.VIEWABILITY_CUTOFF_AREA=t.VIEWABILITY_CUTOFF_AREA||.5,t.VIEWABILITY_CUTOFF_DURATION_SEC=t.VIEWABILITY_CUTOFF_DURATION_SEC||1,t.CSM_JS="//c.amazon-adsystem.com/aax2/csm.js.gz",t.isjQueryPresent=function(){if("function"==typeof t.$jQ)return!0;try{if(a.top.jQuery&&a.top.jQuery.fn.on&&a.top.jQuery.fn.scrollTop)return t.$jQ=a.top.jQuery,!0}catch(e){}return!1},t.log=function(e){try{t.debug_mode&&n.log(e)}catch(a){}},t.log("Initiating amznads"),t.ads||(t.ads={}),t.updateAds=function(e){if(t.ads=e.ads,t.ev=e.ev,e.vads){t.ads||(t.ads={});var a;for(a in e.vads)e.vads.hasOwnProperty(a)&&(t.ads[a]=e.vads[a],t.amzn_vid=e.vads[a])}t.log("Updated ads. Executing rest of the task queue"),t.doAllTasks(),t.tasks.push=function(e){t.doTask(e)}},t.saveAds=function(e){t.saved_ads=e.ads,t.updateAds(e)},t.getAdForSlot=function(a,n,o){t.src_id=a;var r=r||{},i=r.u;i||(i=t.getReferrerURL()),i&&-1!==i.indexOf("amzn_debug_mode")&&(t.debug_mode=!0),t.log("amznads.getAdForSlot: Using url="+i);var s="src="+t.src_id+"&slot_uuid="+n+"&c=100&u="+i+"&cb="+Math.round(1e7*Math.random()),d=t.protocol+t.host+t.pb_svc+"?jsd=1&"+s;t.log("amznads.getAdAdForSlot: "+(o?"Async ":"")+"Call to: "+d),o?t.appendScriptTag(d):aax_write(e,"<script type='text/javascript' src='"+d+"'></script>")},t.getAdsCallback=function(e,a,n,o,r){var i=null,r=r||{};try{a&&"function"==typeof a&&(i=t.handleCallBack(a,n))}catch(s){t.log("amznads.getAdsAsyncWithCallback(): Error occured in setting up callback functionality."+s)}r.to||(r.to=n),t.doGetAdsAsync(e,o,r,i)},t.getVideoAdsCallback=function(e,a,n,o,r){var r=r||{};r.mt="V",t.getAdsCallback(e,a,n,o,r)},t.getDisplayAdsCallback=function(e,a,n,o,r){var r=r||{};r.mt="D",t.getAdsCallback(e,a,n,o,r)},t.getAdsAsync=function(e,a,n){t.doGetAdsAsync(e,a,n)},t.getVideoAdsAsync=function(e,a,n){var n=n||{};n.mt="V",t.getAdsAsync(e,a,n)},t.getDisplayAdsAsync=function(e,a,n){var n=n||{};n.mt="D",t.getAdsAsync(e,a,n)},t.handleCallBack=function(e,n){var o=!1,r=null,i=function(t){if(!o){try{e(t),r&&clearTimeout(r)}catch(a){}o=!0}},s=t.getValidMilliseconds(n);return r=s?a.setTimeout(i,s):a.setTimeout(i,t.DEFAULT_TIMEOUT),i},t.getValidMilliseconds=function(e){if(!e)return!1;try{var a=~~Number(e);if(a>t.MIN_TIMEOUT)return a}catch(n){return t.log("amznads.isValidMilliseconds(): Invalid timeout specified."+n),!1}return!1},t.getAds=function(a,n,o,r){if(r)return void t.doGetAdsAsync(a,n,o);var i=t.getScriptSource(a,n,o);t.log("amznads.getAds: Call to: "+i),aax_write(e,"<script type='text/javascript' src='"+i+"'></script>")},t.getVideoAds=function(e,a,n,o){var n=n||{};n.mt="V",t.getAds(e,a,n,o)},t.getDisplayAds=function(e,a,n,o){var n=n||{};n.mt="D",t.getAds(e,a,n,o)},t.doGetAdsAsync=function(e,a,n,o){var r=t.getScriptSource(e,a,n);t.log("amznads.getAds: Async Call to: "+r),t.appendScriptTag(r,o)},t.getScriptSource=function(a,n,o){t.src_id=a;var o=o||{},r=o.u,i=o.d,s=o.to,d=o.mt;if(r||(r=t.getReferrerURL()),r&&-1!==r.indexOf("amzn_debug_mode")&&(t.debug_mode=!0),t.ads&&(t.log("amznads.getAds(): clear out existing ads"),t.clearTargetingFromGPTAsync(),t.ads={}),t.saved_ads&&(t.ads=t.saved_ads),i)try{e.domain=i,t.log("amznads.getAds(): Using domain="+i)}catch(c){t.log("amznads.getAds(): Unable to override document domain with '"+i+"'; exception="+c)}t.log("amznads.getAds(): Using url="+r);var l="src="+a+"&u="+r+"&cb="+Math.round(1e7*Math.random());n&&(l+="&sz="+n),s&&(l+="&t="+s),d&&(l+="&mt="+d);var g=t.protocol+t.host+t.dtb_svc+"?"+l;return g},t.getReferrerURL=function(){var n=encodeURIComponent(e.documentURI);try{n=encodeURIComponent(a.top.location.href),n&&"undefined"!=n||(n=t.detectIframeAndGetURL())}catch(o){n=t.detectIframeAndGetURL()}return n},t.detectIframeAndGetURL=function(){try{if(a.top!==a.self)return t.log("Script is loaded within iFrame"),encodeURIComponent(e.referrer)}catch(n){return encodeURIComponent(e.documentURI)}},t.appendScriptTag=function(a,n){var o=e.getElementsByTagName("script")[0],r=e.createElement("script");r.type="text/javascript",r.async=!0,r.src=a;try{n&&"function"==typeof n&&(r.readyState?(r.onreadystatechange=function(){("loaded"==r.readyState||"complete"==r.readyState)&&(r.onreadystatechange=null,n(!0))},t.log("amznads.appendScriptTag: setting callBack function for < IE-8 ")):(r.onload=function(){n(!0)},t.log("amznads.appendScriptTag: setting callBack function for all other browsers exluding  < IE-8")))}catch(i){t.log("amznads.appendScriptTag: Could not associate callBack function; "+i)}o.parentNode.insertBefore(r,o)},t.renderAd=function(e,a){if(t.log("amznads.renderAd: key="+a+"; ad-tag="+t.ads[a]),t.ads[a]){var n=t.ads[a];if(t.ev){var o="amznad"+Math.round(1e6*Math.random());n=n.replace("window.top.amznads.detectViewability","window.amzncsm.rmD"),n=n.replace(/id=[^ ]*/,'id="'+o+'"');var r="<script type='text/javascript' src='"+t.CSM_JS+"'></script>\n";t.host!=t.DEFAULT_HOST&&(r+="<script type='text/javascript'>try{amzncsm.host=window.top.amznads.host;}catch(e){amzncsm.host=amznads.host;}</script>\n"),n=r+n}aax_write(e,n)}else{var i=new Object;i.c="dtb",i.src=t.src_id,i.kvmismatch=1,i.pubReturnedKey=a,i.aaxReturnedKeys=t.getTokens(),i.cb=Math.round(1e7*Math.random());try{i.u=encodeURIComponent(location.host+location.pathname),navigator&&(i.ua=encodeURIComponent(navigator.userAgent))}catch(s){}var d=encodeURIComponent(JSON.stringify(i)),c=t.protocol+t.host+"/x/px/p/0/"+d;t.log("amznads.renderAd: keyValueMismatch detected, pubReturnedKey="+a+", aaxReturnedKeys="+t.getTokens()),aax_write(e,"<img src='"+c+"'/>")}},t.detectViewability=function(e,n,r,i){t.isjQueryPresent()&&(r===a.top?new o(e,t.VIEWABILITY_CUTOFF_AREA,t.VIEWABILITY_CUTOFF_DURATION_SEC,!0,n,r,i).collectData():new o(r.frameElement,t.VIEWABILITY_CUTOFF_AREA,t.VIEWABILITY_CUTOFF_DURATION_SEC,!0,n,r,i).collectData())},t.hasAds=function(e){var a;if(!e)try{return Object.keys(t.ads).length>0}catch(n){t.log("amznads.hasAds: looks like IE 8 (and below): "+n);for(a in t.ads)if(t.ads.hasOwnProperty(a))return!0}for(a in t.ads)if(t.ads.hasOwnProperty(a)&&a.indexOf(e)>0)return!0;return!1},t.getTargeting=function(){var e={};return t.getTokens()&&t.getTokens().length>0&&(e[t.targetingKey]=t.getTokens(),t.amzn_vid&&(e[t.vidKey]=t.amzn_vid)),e},t.setTargeting=function(e,a){var n;for(n in t.ads)if(t.ads.hasOwnProperty(n)){if(a&&n.indexOf(a)<0)continue;e(n,"1")}},t.setTargetingForGPTAsync=function(e){try{if(e){t.targetingKey=e;var a=t.getTokens();"undefined"!=typeof a&&a.length>0&&googletag.cmd.push(function(){googletag.pubads().setTargeting(e,a),t.amzn_vid&&googletag.pubads().setTargeting(t.vidKey,t.amzn_vid)})}else{var n;for(n in t.ads)t.ads.hasOwnProperty(n)&&!function(){var e=n;t.log("amznads.setTargetingForGPTAsync: pushing localKey="+e),googletag.cmd.push(function(){amznads.debug_mode&&amznads.log("amznads.setTargetingForGPTAsync: localKey="+e),googletag.pubads().setTargeting(e,"1"),t.amzn_vid&&googletag.pubads().setTargeting(t.vidKey,t.amzn_vid)})}()}t.log("amznads.setTargetingForGPTAsync: Completed successfully. Number of ads returned by Amazon: "+Object.keys(t.ads).length)}catch(o){t.log("amznads.setTargetingForGPTAsync: ERROR - "+o)}},t.setTargetingForGPTSync=function(e){try{if(e){t.targetingKey=e;var a=t.getTokens();"undefined"!=typeof a&&a.length>0&&(googletag.pubads().setTargeting(e,a),t.amzn_vid&&googletag.pubads().setTargeting(t.vidKey,t.amzn_vid))}else{var n;for(n in t.ads)t.ads.hasOwnProperty(n)&&(googletag.pubads().setTargeting(n,"1"),t.amzn_vid&&googletag.pubads().setTargeting(t.vidKey,t.amzn_vid))}t.log("amznads.setTargetingForGPTSync: Completed successfully. Number of ads returned by Amazon: "+Object.keys(t.ads).length)}catch(o){t.log("amznads.setTargetingForGPTSync: ERROR - "+o)}},t.clearTargetingFromGPTAsync=function(){try{googletag&&googletag.pubads()&&googletag.pubads().clearTargeting(t.targetingKey),googletag.pubads().clearTargeting(t.vidKey)}catch(e){}},t.appendTargetingToAdServerUrl=function(e){var a=e;try{-1===e.indexOf("?")&&(e+="?");var n;for(n in t.ads)t.ads.hasOwnProperty(n)&&(e+="&"+n+"=1");t.log("amznads.appendTargetingToAdServerUrl: Completed successfully. Number of ads returned by Amazon: "+t.ads.length)}catch(o){t.log("amznads.appendTargetingToAdServerUrl: ERROR - "+o)}return t.log("amznads.appendTargetingToAdServerUrl: input url: "+a+"\nreturning url: "+e),e},t.appendTargetingToQueryString=function(e){var a=e;try{var n;for(n in t.ads)t.ads.hasOwnProperty(n)&&(e+="&"+n+"=1")}catch(o){t.log("amznads.appendTargetingToQueryString: ERROR - "+o)}return t.log("amznads.appendTargetingToQueryString: input query-string:"+a+"\nreturning query-string:"+e),e},t.getTokens=function(e){var a,n=[];try{for(a in t.ads)if(t.ads.hasOwnProperty(a)){if(e&&a.indexOf(e)<0)continue;n.push(a)}}catch(o){t.log("amznads.getTokens: ERROR - "+o)}return t.log("amznads.getTokens: returning tokens = "+n),n},t.getKeys=t.getTokens,t.getVid=function(){return t.amzn_vid},t.doAllTasks=function(){for(;t.tasks.length>0;){var e=t.tasks.shift();t.doTask(e)}},t.doTask=function(e){try{e.call()}catch(a){t.log("Failed calling task: "+a)}},t.tryGetAdsAsync=function(){t.asyncParams&&t.getAdsCallback(t.asyncParams.id,t.asyncParams.callbackFn,t.asyncParams.timeout,t.asyncParams.size,t.asyncParams.data)},t}(amznads||{},document,window,amzn_console);amznads.tryGetAdsAsync(),window.amzn_ads=amzn_ads,window.amznads=amznads;
;
