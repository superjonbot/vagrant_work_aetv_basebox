var aetv = aetv || {};

aetv.aetvModal = (function (module, $) {

  module.init = function () {    
    if (History) {
      var waa = window.aetv.aetvModal || window;
      var _self = this;

      History.Adapter.bind(window, 'statechange', function () {
        var State = History.getState(); 
        //alert('state changed');

        console.log('in modal init state = ', State);
        console.log(aetv.aetvModal);

        if (window.location.pathname == waa.modalReferrer && waa.modalVisible) {
          console.log('FROM ADAPTER version - attempt to close');
          _self.closeModal();
        }
      });
    }

  };

  //use for custom modal buttons created in content templates
  module.closeModal = function () {
    console.log('close modal function called');
    var waa = window.aetv.aetvModal || window;

    jQuery.modal.close();
    $(waa.modalIframe).attr('src', 'about:blank');
    waa.modalVisible = false;
    

    if (History) {
      var currTitle = document.getElementsByTagName("title")[0].innerHTML,
          currReferrer = waa.modalReferrer;
      History.pushState(
        {},
        currTitle, 
        currReferrer
      );

      // if returning to homepage, resume hero
      if (currReferrer == '/') {
        aetv.innerPage.thePage.resume_promos();  
      }
    }
  };

  return module;

}(aetv.aetvModal || {}, jQuery));


(function ($) {
  aetv.aetvModal.init();

  Drupal.behaviors.aetv_modal = {
    attach: function (context, settings) {
      jQuery(Drupal.settings.aetv_modal.aetv_modal_class).aetvModal();
    }
  };
})(jQuery);;
