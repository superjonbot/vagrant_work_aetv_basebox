/*
 * Google CSE glue module. Uses cse js API
 *
 */
var aetv = aetv || {};

aetv.googleSearch2 = (function (module, $, LAB) {

  var debugMode,
      currentlySearching,
      searchOpen,
      searchWrapper,
      searchButton,
      searchArrow,
      searchResumeButton,
      gso,
      clearButton,
      resultsCloseButton,
      blackOut,
      socialNav;

  module.init = function () {
    LAB.script('http://www.google.com/jsapi').wait(function () {
      dlog('jsapi loaded');
      var loadOptions = {
        "callback": function() { dlog('search callback'); }
      };
      google.load('search', '1', loadOptions);
      google.setOnLoadCallback(function () {
        dlog('set on load callback');

        dlog('google cse js version 2 loaded');
        debugMode = true;
        searchOpen = false;
        currentlySearching = false;
        setVars();
        bindControls('onload');
        createWidget();

        //create a structure that I can manipulate by wrapping the box in MY div
        $('.gssb_c').wrap('<div class="aetv-cse-wrapper"></div>');
        $('#search-wrapper').appendTo('.aetv-cse-wrapper');
        $('<div class="search-icon"></div>').prependTo('td.gsc-input');
        $('input.gsc-input').attr('placeholder','Search');
        //$('.aetv-cse-wrapper').appendTo('#wrapper');

        checkForQuery();

        //$(window).click(function (e) { dlog('searchOpen='+searchOpen+':'+e.srcElement); });
      });
    });
  }

  function setVars() {
    searchButton = $('#google-search .search'),
    searchResumeButton = $('#google-search .search-resume'),
    searchWrapper = $('#google-search .search-wrapper'),
    searchArrow = $('.search-arrow'),
    clearButton = $('.gsc-clear-button'),
    blackOut = '<div class="gsc-modal-background-image gsc-modal-background-image-visible" tabindex="0"></div>', 
    resultsCloseButton = '<div class="gsc-results-close-btn gsc-results-close-btn-visible" tabindex="0"></div>',
    searchWrapperMask = '<a class="search-wrapper-mask"></a>';
    socialNav = $("#header_social");
  }

  function searchComplete() {
    var acBox = $('.gsc-results-wrapper-nooverlay');
    acBox.show();
    acBox.css('border', '3px solid #ccc');
//    showArrow();

    var term = gso.getInputQuery();
    if (term != "") {
      saveSearchTerm(term);
    }
    showResultHeader(term);

    //add in the results close button
    if ($('.gsc-results-close-btn').length < 1) {
      $(resultsCloseButton).appendTo('.gsc-results-wrapper-nooverlay');
    }
    
    //add in thickbox overlay
    if ($('.gsc-modal-background-image').length < 1) {
      $(blackOut).insertAfter('.gsc-results-wrapper-nooverlay');
    }

    //mask the input field separately so we can target a click there
    if ($('.search-wrapper-mask').length < 1) {
      $(searchWrapperMask).insertAfter('.gsc-results-wrapper-nooverlay');
      bindControls('searchDisplayed');
    }

    //make all the links go to target _top
    $('.gsc-table-result a').each(
      function () { 
        if (typeof($(this).attr('target')) != 'undefined') { 
          $(this).attr('target', '_top'); 
        } 
      }
    );
  }

  function adjustResultsBoxHeight() {
    var adjHeight = Math.round($('#wrapper').height() * .80);
    $('.gsc-results-wrapper-nooverlay').css('height', adjHeight+'px');
    $('.gsc-wrapper').css('height', adjHeight+'px');
  }

  function searchStarting() {
    //dlog('search starting event');
    //decide how tall the result box will be
    adjustResultsBoxHeight();
  }

  function saveSearchTerm(term, src) {
    //dlog('SAVING '+term);
    $.cookie('gst', term, {'domain':document.location.hostname});
  };

  function hideArrow() {
    $('.search-arrow').hide();
  }

  function showArrow() {
    if ($('.gsc-results-wrapper-nooverlay').length > 0) {
      $('.search-arrow').show();
    }
  };

  function showResultHeader(term) {
    if ($('.gsc-result-info').length > 0) {
      term = 'Search Results for "'+term+'"';
      if ($('.gsc-search-result-hdr').length < 1) {
        $('<div class="gsc-search-result-hdr"><strong>'+term+'</strong></div>').insertBefore('.gsc-result-info');
      }
      else {
        $('.gsc-search-result-hdr strong').text(term);
      }
    }
  }

  function createWidget() {
  	dlog('attempting to create widget');
    var cseId = Drupal.settings.aetv_google_cse.googleSearchCseId;
    var drawOptions = new google.search.DrawOptions();
    drawOptions.setAutoComplete(true);
    gso = new google.search.CustomSearchControl(cseId, {});
    dlog(gso);

    gso.setSearchCompleteCallback({}, searchComplete);
    gso.setSearchStartingCallback({}, searchStarting);
    gso.draw('google-cse', drawOptions);
  }

  function bindControls(which) { 
    switch (which) {
      case 'onload':

          try {
              console.log('attempting to fix any isotope');
              setTimeout(function(){jQuery('.isotope').isotope( 'reloadItems' ).isotope();},0);
          }
          catch(err) {
              //no isotope
          }


        searchButton.click(function (e) {
          dlog('search button clicked');
          $('.aetv-cse-wrapper').addClass('open');
          // nasty fix for unclickable scroller buttons
          $('#header #header_tools').removeClass('search-closed');
          dlog('searchOpen='+searchOpen);
          if (!searchOpen) {
            openSearch();
          }
          else {
            closeSearch();
          }
          e.preventDefault();
          return false;
        });

        searchResumeButton.click(function (e) {
          dlog('search resume button clicked');
          // nasty fix for unclickable scroller buttons
          $('#header #header_tools').removeClass('search-closed');
          var term = getLastSearch();
          if (term) {
            openSearch(term);
            e.preventDefault();
          }
          return false;
        });

        $(window).resize(_.debounce(function () {
          adjustResultsBoxHeight();
        }, 500));
        break;

      case 'searchDisplayed':
        $('.gsc-modal-background-image').bind('click', function () {
          clearSearchResults();
        });

        $('.gsc-results-close-btn').bind('click', function () {
          clearSearchResults();
        });

        $('.search-wrapper-mask').bind('click', function () { 
          clearSearchResults(true);
        });
//        $('.aetv-cse-wrapper').css('z-index', 0);
        $('#google-search .search-wrapper').css('top','6px').css('right','50%');
        break; 
      
      case 'typingSearch':
        var searchInput = $('#gsc-i-id1');
        var clearButton = $('.gsc-clear-button');
        var acBox = $('.gsc-completion-container');

        //acBox.css('border', '1px solid #bbb');
        //acBox.addClass('gs-rounded-corners');

        //move the autocomplete box 
        searchInput.bind('keyup', function (e) {
          var acBox = $('.gsc-completion-container');
          var acRcCRows = $('.gssb_a');
          var adjWidth;
          currentlySearching = true;

          //$('.aetv-cse-wrapper').css('z-index', 1002);
//          $('.aetv-cse-wrapper').css('z-index', 9999);
//          $('#google-search .search-wrapper').css('top','75px').css('right','10px');

          dlog('currentlySearching = true');
          dlog('acBox width='+acRcCRows.width());
//          if (acRcCRows.width()) {
//            adjWidth = acRcCRows.width() + 20;
//          }
//          else {
//            adjWidth = 100;
//          }
          dlog('adjusted width = '+adjWidth);
//          var styles = "width: "+adjWidth+"px !important; display: block; left:auto; right:10px; position: absolute;"
//          $('.gssb_c').get(0).setAttribute('style', "");
          //$('.gssb_c').get(0).setAttribute('style', styles);

//          $('.gssb_c').addClass('gs-rounded-corners');

//          acBox.css('width', adjWidth+'px');

          focusField();
        });

        acBox.bind('mouseleave', function () {
//          $('.aetv-cse-wrapper').css('z-index', -1);
          currentlySearching = false;
        });

//        searchWrapper.bind('mouseleave', function () {
//          if (!currentlySearching && searchOpen || searchInput.val() == "") {
//            closeSearch();
//          }
//        });
        clearButton.bind('click touch', function() {
		  closeSearch();
		});
        break;
    }

    dlog('controls loaded for '+which);
  };

  function clearSearchResults(keepSearching) {
    //dlog('background clicked!');
    $('.gsc-results-wrapper-nooverlay').hide();
    $('.gsc-modal-background-image').remove();
    hideArrow();
    gso.clearAllResults();

    if (!keepSearching) {
      currentlySearching = false;
      closeSearch();
    }
    else {
      currentlySearching = true;
      focusField();
    }
    $('.search-wrapper-mask').unbind('click');
    $('.search-wrapper-mask').remove();
    $('#google-search .search-wrapper').css('top','75px').css('right','10px');
  }

  function showResumeSearch() {
    dlog('show resume search button');
    searchButton.css('left', '-27px');
    searchWrapper.hide();
    searchResumeButton.show();
//    $('.aetv-cse-wrapper').css('z-index', 0);
  };

  function getLastSearch() {
    if ($.cookie('gst') != null) {
      return $.cookie('gst');
    }
    else {
      return false;
    }
  };

  function termIsSaved() {
    return getLastSearch();
  };

  function checkForQuery() {
    var termFound = termIsSaved();
    dlog('termfound='+termFound);
    if (termFound) {
      showResumeSearch();
    }
  };

 function focusField() {
    var searchInput = $('#gsc-i-id1');
    searchInput.focus();
  };

  function executeSearch(term) {
    if (typeof(term) != 'undefined') {
      dlog('term was provided. it is: '+term);
      currentlySearching = true;
      gso.prefillQuery(term);
      focusField();
      dlog('attempting to trigger search');
      gso.execute(term, 1, '');
      searchOpen = true;
    }
  };

  function openSearch(term) {
    dlog('open search. term = '+term);
    var searchInput = $('#gsc-i-id1');
    var clearButton = $('div.gsc-clear-button');
//    clearButton.hide();
	
    searchOpen = true;

    searchInput.hide();
    searchWrapper.show();
    searchResumeButton.hide();
    //searchResumeButton.css('left', '4px'); 
    
    //socialNav.removeClass('open');
    searchWrapper.animate(
      {width: '500px'}, 
      1000, 
      function () {
        //searchInput.css('width','180px');
        searchInput.show();
//        clearButton.fadeIn(1000, function () {
//          $(this).css('display','inline');
//        });

        bindControls('typingSearch');

//        $('.aetv-cse-wrapper').css('z-index', 0);

        if (term) {
          executeSearch(term); 
        }
        else {
          focusField();
        }
      }
    );
  };

  function closeSearch() {
    dlog('close search');
    // nasty fix for unclickable scroller buttons
//    $('#header #header_tools').addClass('search-closed');
    var btnWidth = '0';
    var searchInput = $('#gsc-i-id1');
    var clearButton = $('.gsc-clear-button');
    currentlySearching = false;

//    $('.aetv-cse-wrapper').css('z-index', -1);
	$('.aetv-cse-wrapper').removeClass('open');

//    clearButton.hide();
    if (!_.isUndefined(gso) && gso.getInputQuery() != "") {
      gso.clearAllResults();
    }
    searchOpen = false;
    searchInput.unbind('keypress');
    clearButton.unbind('click');

    if (!_.isUndefined(searchWrapper)) {
      searchWrapper.unbind('mouseleave');
    }
    searchInput.hide();

    if (termIsSaved()) {
      btnWidth = '27';
    }
    else {
      btnWidth = '1';
    }

    if (!_.isUndefined(searchWrapper)) {
      searchWrapper.animate(
        //{ width : btnWidth+'px' },
        //1000,
        function () {
          checkForQuery();
        }
      );
    }
  };

  function dlog(msg) {
    if (typeof(console) != 'undefined' && debugMode) {
      console.log('CSE: '+msg);
    }
  };
  
  module.CloseSearchBar = function(){
	  closeSearch();
  };

  return module;

}(aetv.googleSearch2 || {}, jQuery, $LAB));

(function () {
  aetv.googleSearch2.init();
})();
;
(function ($) {

/**
 * Attaches the autocomplete behavior to all required fields.
 */
Drupal.behaviors.autocomplete = {
  attach: function (context, settings) {
    var acdb = [];
    $('input.autocomplete', context).once('autocomplete', function () {
      var uri = this.value;
      if (!acdb[uri]) {
        acdb[uri] = new Drupal.ACDB(uri);
      }
      var $input = $('#' + this.id.substr(0, this.id.length - 13))
        .attr('autocomplete', 'OFF')
        .attr('aria-autocomplete', 'list');
      $($input[0].form).submit(Drupal.autocompleteSubmit);
      $input.parent()
        .attr('role', 'application')
        .append($('<span class="element-invisible" aria-live="assertive"></span>')
          .attr('id', $input.attr('id') + '-autocomplete-aria-live')
        );
      new Drupal.jsAC($input, acdb[uri]);
    });
  }
};

/**
 * Prevents the form from submitting if the suggestions popup is open
 * and closes the suggestions popup when doing so.
 */
Drupal.autocompleteSubmit = function () {
  return $('#autocomplete').each(function () {
    this.owner.hidePopup();
  }).length == 0;
};

/**
 * An AutoComplete object.
 */
Drupal.jsAC = function ($input, db) {
  var ac = this;
  this.input = $input[0];
  this.ariaLive = $('#' + this.input.id + '-autocomplete-aria-live');
  this.db = db;

  $input
    .keydown(function (event) { return ac.onkeydown(this, event); })
    .keyup(function (event) { ac.onkeyup(this, event); })
    .blur(function () { ac.hidePopup(); ac.db.cancel(); });

};

/**
 * Handler for the "keydown" event.
 */
Drupal.jsAC.prototype.onkeydown = function (input, e) {
  if (!e) {
    e = window.event;
  }
  switch (e.keyCode) {
    case 40: // down arrow.
      this.selectDown();
      return false;
    case 38: // up arrow.
      this.selectUp();
      return false;
    default: // All other keys.
      return true;
  }
};

/**
 * Handler for the "keyup" event.
 */
Drupal.jsAC.prototype.onkeyup = function (input, e) {
  if (!e) {
    e = window.event;
  }
  switch (e.keyCode) {
    case 16: // Shift.
    case 17: // Ctrl.
    case 18: // Alt.
    case 20: // Caps lock.
    case 33: // Page up.
    case 34: // Page down.
    case 35: // End.
    case 36: // Home.
    case 37: // Left arrow.
    case 38: // Up arrow.
    case 39: // Right arrow.
    case 40: // Down arrow.
      return true;

    case 9:  // Tab.
    case 13: // Enter.
    case 27: // Esc.
      this.hidePopup(e.keyCode);
      return true;

    default: // All other keys.
      if (input.value.length > 0 && !input.readOnly) {
        this.populatePopup();
      }
      else {
        this.hidePopup(e.keyCode);
      }
      return true;
  }
};

/**
 * Puts the currently highlighted suggestion into the autocomplete field.
 */
Drupal.jsAC.prototype.select = function (node) {
  this.input.value = $(node).data('autocompleteValue');
};

/**
 * Highlights the next suggestion.
 */
Drupal.jsAC.prototype.selectDown = function () {
  if (this.selected && this.selected.nextSibling) {
    this.highlight(this.selected.nextSibling);
  }
  else if (this.popup) {
    var lis = $('li', this.popup);
    if (lis.length > 0) {
      this.highlight(lis.get(0));
    }
  }
};

/**
 * Highlights the previous suggestion.
 */
Drupal.jsAC.prototype.selectUp = function () {
  if (this.selected && this.selected.previousSibling) {
    this.highlight(this.selected.previousSibling);
  }
};

/**
 * Highlights a suggestion.
 */
Drupal.jsAC.prototype.highlight = function (node) {
  if (this.selected) {
    $(this.selected).removeClass('selected');
  }
  $(node).addClass('selected');
  this.selected = node;
  $(this.ariaLive).html($(this.selected).html());
};

/**
 * Unhighlights a suggestion.
 */
Drupal.jsAC.prototype.unhighlight = function (node) {
  $(node).removeClass('selected');
  this.selected = false;
  $(this.ariaLive).empty();
};

/**
 * Hides the autocomplete suggestions.
 */
Drupal.jsAC.prototype.hidePopup = function (keycode) {
  // Select item if the right key or mousebutton was pressed.
  if (this.selected && ((keycode && keycode != 46 && keycode != 8 && keycode != 27) || !keycode)) {
    this.input.value = $(this.selected).data('autocompleteValue');
  }
  // Hide popup.
  var popup = this.popup;
  if (popup) {
    this.popup = null;
    $(popup).fadeOut('fast', function () { $(popup).remove(); });
  }
  this.selected = false;
  $(this.ariaLive).empty();
};

/**
 * Positions the suggestions popup and starts a search.
 */
Drupal.jsAC.prototype.populatePopup = function () {
  var $input = $(this.input);
  var position = $input.position();
  // Show popup.
  if (this.popup) {
    $(this.popup).remove();
  }
  this.selected = false;
  this.popup = $('<div id="autocomplete"></div>')[0];
  this.popup.owner = this;
  $(this.popup).css({
    top: parseInt(position.top + this.input.offsetHeight, 10) + 'px',
    left: parseInt(position.left, 10) + 'px',
    width: $input.innerWidth() + 'px',
    display: 'none'
  });
  $input.before(this.popup);

  // Do search.
  this.db.owner = this;
  this.db.search(this.input.value);
};

/**
 * Fills the suggestion popup with any matches received.
 */
Drupal.jsAC.prototype.found = function (matches) {
  // If no value in the textfield, do not show the popup.
  if (!this.input.value.length) {
    return false;
  }

  // Prepare matches.
  var ul = $('<ul></ul>');
  var ac = this;
  for (key in matches) {
    $('<li></li>')
      .html($('<div></div>').html(matches[key]))
      .mousedown(function () { ac.select(this); })
      .mouseover(function () { ac.highlight(this); })
      .mouseout(function () { ac.unhighlight(this); })
      .data('autocompleteValue', key)
      .appendTo(ul);
  }

  // Show popup with matches, if any.
  if (this.popup) {
    if (ul.children().length) {
      $(this.popup).empty().append(ul).show();
      $(this.ariaLive).html(Drupal.t('Autocomplete popup'));
    }
    else {
      $(this.popup).css({ visibility: 'hidden' });
      this.hidePopup();
    }
  }
};

Drupal.jsAC.prototype.setStatus = function (status) {
  switch (status) {
    case 'begin':
      $(this.input).addClass('throbbing');
      $(this.ariaLive).html(Drupal.t('Searching for matches...'));
      break;
    case 'cancel':
    case 'error':
    case 'found':
      $(this.input).removeClass('throbbing');
      break;
  }
};

/**
 * An AutoComplete DataBase object.
 */
Drupal.ACDB = function (uri) {
  this.uri = uri;
  this.delay = 300;
  this.cache = {};
};

/**
 * Performs a cached and delayed search.
 */
Drupal.ACDB.prototype.search = function (searchString) {
  var db = this;
  this.searchString = searchString;

  // See if this string needs to be searched for anyway.
  searchString = searchString.replace(/^\s+|\s+$/, '');
  if (searchString.length <= 0 ||
    searchString.charAt(searchString.length - 1) == ',') {
    return;
  }

  // See if this key has been searched for before.
  if (this.cache[searchString]) {
    return this.owner.found(this.cache[searchString]);
  }

  // Initiate delayed search.
  if (this.timer) {
    clearTimeout(this.timer);
  }
  this.timer = setTimeout(function () {
    db.owner.setStatus('begin');

    // Ajax GET request for autocompletion. We use Drupal.encodePath instead of
    // encodeURIComponent to allow autocomplete search terms to contain slashes.
    $.ajax({
      type: 'GET',
      url: db.uri + '/' + Drupal.encodePath(searchString),
      dataType: 'json',
      success: function (matches) {
        if (typeof matches.status == 'undefined' || matches.status != 0) {
          db.cache[searchString] = matches;
          // Verify if these are still the matches the user wants to see.
          if (db.searchString == searchString) {
            db.owner.found(matches);
          }
          db.owner.setStatus('found');
        }
      },
      error: function (xmlhttp) {
        alert(Drupal.ajaxError(xmlhttp, db.uri));
      }
    });
  }, this.delay);
};

/**
 * Cancels the current autocomplete request.
 */
Drupal.ACDB.prototype.cancel = function () {
  if (this.owner) this.owner.setStatus('cancel');
  if (this.timer) clearTimeout(this.timer);
  this.searchString = '';
};

})(jQuery);
;
(function ($) {

  var $window = $(window);

  Drupal.behaviors.referencesDialog = {
    attach: function (context, settings) {
      // Add appropriate classes on all fields that should have it. This is
      // necessary since we don't actually know what markup we are dealing with.
      if (typeof settings.ReferencesDialog !== 'undefined') {
        $.each(settings.ReferencesDialog.fields, function (key, widget_settings) {
          $('.' + key + ' a.references-dialog-activate', context).click(function (e) {
            e.preventDefault();
            Drupal.ReferencesDialog.open($(this).attr('href'), $(this).html());
            Drupal.ReferencesDialog.entityIdReceived = function (entity_type, entity_id, label) {
              if (typeof widget_settings.format !== 'undefined') {
                var value = widget_settings.format
                  .replace('$label', label)
                  .replace('$entity_id', entity_id)
                  .replace('$entity_type', entity_type);
              }
              // If we have a callback path, let's invoke that.
              if (typeof widget_settings.callback_path !== 'undefined') {
                var entity_info = {
                  label: label,
                  entity_id: entity_id,
                  entity_type: entity_type
                };
                Drupal.ReferencesDialog.invokeCallback(widget_settings.callback_path, entity_info, widget_settings.callback_settings);
              }
              // If we have a target, use that.
              else if (typeof widget_settings.target !== 'undefined') {
                var target = $('#' + widget_settings.target);
                target.val(value);
                target.change();
              }
              // If we have none of the above, we just insert the value in the item
              // that invoked this.
              else {
                var key_el = $('#' + key);
                key_el.val(value);
                key_el.change();
              }
            }
            return false;
          });
        });
      }
    }
  };

  /**
   * Our dialog object. Can be used to open a dialog to anywhere.
   */
  Drupal.ReferencesDialog = {
    dialog_open: false,
    open_dialog: null
  }

  Drupal.ReferencesDialog.invokeCallback = function (callback, entity_info, settings) {
    if (typeof settings == 'object') {
      entity_info.settings = settings;
    }
    $.post(callback, entity_info);
  }

  /**
   * If this property is set to be a function, it
   * will be called when an entity is recieved from an overlay.
   */
  Drupal.ReferencesDialog.entityIdReceived = null;

  /**
   * Open a dialog window.
   * @param string href the link to point to.
   */
  Drupal.ReferencesDialog.open = function (href, title) {
    if (!this.dialog_open) {
      // Add render references dialog, so that we know that we should be in a
      // dialog.
      href += (href.indexOf('?') > -1 ? '&' : '?') + 'render=references-dialog';
      // Get the current window size and do 75% of the width and 90% of the height.
      // @todo Add settings for this so that users can configure this by themselves.
      var window_width = $window.width() / 100*75;
      var window_height = $window.height() / 100*90;
      this.open_dialog = $('<iframe class="references-dialog-iframe" src="' + href + '"></iframe>').dialog({
        width: window_width,
        height: window_height,
        modal: true,
        resizable: false,
        position: ['center', 50],
        title: title,
        close: function () { Drupal.ReferencesDialog.dialog_open = false; }
      }).width(window_width-10).height(window_height);
      $window.bind('resize scroll', function () {
        // Move the dialog the main window moves.
        if (typeof Drupal.ReferencesDialog == 'object' && Drupal.ReferencesDialog.open_dialog != null) {
          Drupal.ReferencesDialog.open_dialog.
            dialog('option', 'position', ['center', 10]);
          Drupal.ReferencesDialog.setDimensions();
        }
      });
      this.dialog_open = true;
    }
  }

  /**
   * Set dimensions of the dialog dependning on the current winow size
   * and scroll position.
   */
  Drupal.ReferencesDialog.setDimensions = function () {
    if (typeof Drupal.ReferencesDialog == 'object') {
      var window_width = $window.width() / 100*75;
      var window_height = $window.height() / 100*90;
      this.open_dialog.
        dialog('option', 'width', window_width).
        dialog('option', 'height', window_height).
        width(window_width-10).height(window_height);
    }
  }

  /**
   * Close the dialog and provide an entity id and a title
   * that we can use in various ways.
   */
  Drupal.ReferencesDialog.close = function (entity_type, entity_id, title) {
    this.open_dialog.dialog('close');
    this.open_dialog.dialog('destroy');
    this.open_dialog = null;
    this.dialog_open = false;
    // Call our entityIdReceived function if we have one.
    // this is used as an event.
    if (typeof this.entityIdReceived == 'function') {
      this.entityIdReceived(entity_type, entity_id, title);
    }
  }

}(jQuery));
;
(function ($) {

Drupal.behaviors.textarea = {
  attach: function (context, settings) {
    $('.form-textarea-wrapper.resizable', context).once('textarea', function () {
      var staticOffset = null;
      var textarea = $(this).addClass('resizable-textarea').find('textarea');
      var grippie = $('<div class="grippie"></div>').mousedown(startDrag);

      grippie.insertAfter(textarea);

      function startDrag(e) {
        staticOffset = textarea.height() - e.pageY;
        textarea.css('opacity', 0.25);
        $(document).mousemove(performDrag).mouseup(endDrag);
        return false;
      }

      function performDrag(e) {
        textarea.height(Math.max(32, staticOffset + e.pageY) + 'px');
        return false;
      }

      function endDrag(e) {
        $(document).unbind('mousemove', performDrag).unbind('mouseup', endDrag);
        textarea.css('opacity', 1);
      }
    });
  }
};

})(jQuery);
;
;
