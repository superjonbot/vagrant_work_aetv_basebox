/*
 * Google CSE glue module. Uses cse js API
 *
 */
var aetv = aetv || {};

aetv.googleSearch2 = (function (module, $, LAB) {

  var debugMode,
      currentlySearching,
      searchOpen,
      searchWrapper,
      searchButton,
      searchArrow,
      searchResumeButton,
      gso,
      clearButton,
      resultsCloseButton,
      blackOut,
      socialNav;

  module.init = function () {
    LAB.script('http://www.google.com/jsapi').wait(function () {
      dlog('jsapi loaded');
      var loadOptions = {
        "callback": function() { dlog('search callback'); }
      };
      google.load('search', '1', loadOptions);
      google.setOnLoadCallback(function () {
        dlog('set on load callback');

        dlog('google cse js version 2 loaded');
        debugMode = true;
        searchOpen = false;
        currentlySearching = false;
        setVars();
        bindControls('onload');
        createWidget();

        //create a structure that I can manipulate by wrapping the box in MY div
        $('.gssb_c').wrap('<div class="aetv-cse-wrapper"></div>');
        $('#search-wrapper').appendTo('.aetv-cse-wrapper');
        $('<div class="search-icon"></div>').prependTo('td.gsc-input');
        $('input.gsc-input').attr('placeholder','Search');
        //$('.aetv-cse-wrapper').appendTo('#wrapper');

        checkForQuery();

        //$(window).click(function (e) { dlog('searchOpen='+searchOpen+':'+e.srcElement); });
      });
    });
  }

  function setVars() {
    searchButton = $('#google-search .search'),
    searchResumeButton = $('#google-search .search-resume'),
    searchWrapper = $('#google-search .search-wrapper'),
    searchArrow = $('.search-arrow'),
    clearButton = $('.gsc-clear-button'),
    blackOut = '<div class="gsc-modal-background-image gsc-modal-background-image-visible" tabindex="0"></div>', 
    resultsCloseButton = '<div class="gsc-results-close-btn gsc-results-close-btn-visible" tabindex="0"></div>',
    searchWrapperMask = '<a class="search-wrapper-mask"></a>';
    socialNav = $("#header_social");
  }

  function searchComplete() {
    var acBox = $('.gsc-results-wrapper-nooverlay');
    acBox.show();
    acBox.css('border', '3px solid #ccc');
//    showArrow();

    var term = gso.getInputQuery();
    if (term != "") {
      saveSearchTerm(term);
    }
    showResultHeader(term);

    //add in the results close button
    if ($('.gsc-results-close-btn').length < 1) {
      $(resultsCloseButton).appendTo('.gsc-results-wrapper-nooverlay');
    }
    
    //add in thickbox overlay
    if ($('.gsc-modal-background-image').length < 1) {
      $(blackOut).insertAfter('.gsc-results-wrapper-nooverlay');
    }

    //mask the input field separately so we can target a click there
    if ($('.search-wrapper-mask').length < 1) {
      $(searchWrapperMask).insertAfter('.gsc-results-wrapper-nooverlay');
      bindControls('searchDisplayed');
    }

    //make all the links go to target _top
    $('.gsc-table-result a').each(
      function () { 
        if (typeof($(this).attr('target')) != 'undefined') { 
          $(this).attr('target', '_top'); 
        } 
      }
    );
  }

  function adjustResultsBoxHeight() {
    var adjHeight = Math.round($('#wrapper').height() * .80);
    $('.gsc-results-wrapper-nooverlay').css('height', adjHeight+'px');
    $('.gsc-wrapper').css('height', adjHeight+'px');
  }

  function searchStarting() {
    //dlog('search starting event');
    //decide how tall the result box will be
    adjustResultsBoxHeight();
  }

  function saveSearchTerm(term, src) {
    //dlog('SAVING '+term);
    $.cookie('gst', term, {'domain':document.location.hostname});
  };

  function hideArrow() {
    $('.search-arrow').hide();
  }

  function showArrow() {
    if ($('.gsc-results-wrapper-nooverlay').length > 0) {
      $('.search-arrow').show();
    }
  };

  function showResultHeader(term) {
    if ($('.gsc-result-info').length > 0) {
      term = 'Search Results for "'+term+'"';
      if ($('.gsc-search-result-hdr').length < 1) {
        $('<div class="gsc-search-result-hdr"><strong>'+term+'</strong></div>').insertBefore('.gsc-result-info');
      }
      else {
        $('.gsc-search-result-hdr strong').text(term);
      }
    }
  }

  function createWidget() {
  	dlog('attempting to create widget');
    var cseId = Drupal.settings.aetv_google_cse.googleSearchCseId;
    var drawOptions = new google.search.DrawOptions();
    drawOptions.setAutoComplete(true);
    gso = new google.search.CustomSearchControl(cseId, {});
    dlog(gso);

    gso.setSearchCompleteCallback({}, searchComplete);
    gso.setSearchStartingCallback({}, searchStarting);
    gso.draw('google-cse', drawOptions);
  }

  function bindControls(which) { 
    switch (which) {
      case 'onload':

          try {
              console.log('attempting to fix any isotope');
              setTimeout(function(){jQuery('.isotope').isotope( 'reloadItems' ).isotope();},0);
          }
          catch(err) {
              //no isotope
          }


        searchButton.click(function (e) {
          dlog('search button clicked');
          $('.aetv-cse-wrapper').addClass('open');
          // nasty fix for unclickable scroller buttons
          $('#header #header_tools').removeClass('search-closed');
          dlog('searchOpen='+searchOpen);
          if (!searchOpen) {
            openSearch();
          }
          else {
            closeSearch();
          }
          e.preventDefault();
          return false;
        });

        searchResumeButton.click(function (e) {
          dlog('search resume button clicked');
          // nasty fix for unclickable scroller buttons
          $('#header #header_tools').removeClass('search-closed');
          var term = getLastSearch();
          if (term) {
            openSearch(term);
            e.preventDefault();
          }
          return false;
        });

        $(window).resize(_.debounce(function () {
          adjustResultsBoxHeight();
        }, 500));
        break;

      case 'searchDisplayed':
        $('.gsc-modal-background-image').bind('click', function () {
          clearSearchResults();
        });

        $('.gsc-results-close-btn').bind('click', function () {
          clearSearchResults();
        });

        $('.search-wrapper-mask').bind('click', function () { 
          clearSearchResults(true);
        });
//        $('.aetv-cse-wrapper').css('z-index', 0);
        $('#google-search .search-wrapper').css('top','6px').css('right','50%');
        break; 
      
      case 'typingSearch':
        var searchInput = $('#gsc-i-id1');
        var clearButton = $('.gsc-clear-button');
        var acBox = $('.gsc-completion-container');

        //acBox.css('border', '1px solid #bbb');
        //acBox.addClass('gs-rounded-corners');

        //move the autocomplete box 
        searchInput.bind('keyup', function (e) {
          var acBox = $('.gsc-completion-container');
          var acRcCRows = $('.gssb_a');
          var adjWidth;
          currentlySearching = true;

          //$('.aetv-cse-wrapper').css('z-index', 1002);
//          $('.aetv-cse-wrapper').css('z-index', 9999);
//          $('#google-search .search-wrapper').css('top','75px').css('right','10px');

          dlog('currentlySearching = true');
          dlog('acBox width='+acRcCRows.width());
//          if (acRcCRows.width()) {
//            adjWidth = acRcCRows.width() + 20;
//          }
//          else {
//            adjWidth = 100;
//          }
          dlog('adjusted width = '+adjWidth);
//          var styles = "width: "+adjWidth+"px !important; display: block; left:auto; right:10px; position: absolute;"
//          $('.gssb_c').get(0).setAttribute('style', "");
          //$('.gssb_c').get(0).setAttribute('style', styles);

//          $('.gssb_c').addClass('gs-rounded-corners');

//          acBox.css('width', adjWidth+'px');

          focusField();
        });

        acBox.bind('mouseleave', function () {
//          $('.aetv-cse-wrapper').css('z-index', -1);
          currentlySearching = false;
        });

//        searchWrapper.bind('mouseleave', function () {
//          if (!currentlySearching && searchOpen || searchInput.val() == "") {
//            closeSearch();
//          }
//        });
        clearButton.bind('click touch', function() {
		  closeSearch();
		});
        break;
    }

    dlog('controls loaded for '+which);
  };

  function clearSearchResults(keepSearching) {
    //dlog('background clicked!');
    $('.gsc-results-wrapper-nooverlay').hide();
    $('.gsc-modal-background-image').remove();
    hideArrow();
    gso.clearAllResults();

    if (!keepSearching) {
      currentlySearching = false;
      closeSearch();
    }
    else {
      currentlySearching = true;
      focusField();
    }
    $('.search-wrapper-mask').unbind('click');
    $('.search-wrapper-mask').remove();
    $('#google-search .search-wrapper').css('top','75px').css('right','10px');
  }

  function showResumeSearch() {
    dlog('show resume search button');
    searchButton.css('left', '-27px');
    searchWrapper.hide();
    searchResumeButton.show();
//    $('.aetv-cse-wrapper').css('z-index', 0);
  };

  function getLastSearch() {
    if ($.cookie('gst') != null) {
      return $.cookie('gst');
    }
    else {
      return false;
    }
  };

  function termIsSaved() {
    return getLastSearch();
  };

  function checkForQuery() {
    var termFound = termIsSaved();
    dlog('termfound='+termFound);
    if (termFound) {
      showResumeSearch();
    }
  };

 function focusField() {
    var searchInput = $('#gsc-i-id1');
    searchInput.focus();
  };

  function executeSearch(term) {
    if (typeof(term) != 'undefined') {
      dlog('term was provided. it is: '+term);
      currentlySearching = true;
      gso.prefillQuery(term);
      focusField();
      dlog('attempting to trigger search');
      gso.execute(term, 1, '');
      searchOpen = true;
    }
  };

  function openSearch(term) {
    dlog('open search. term = '+term);
    var searchInput = $('#gsc-i-id1');
    var clearButton = $('div.gsc-clear-button');
//    clearButton.hide();
	
    searchOpen = true;

    searchInput.hide();
    searchWrapper.show();
    searchResumeButton.hide();
    //searchResumeButton.css('left', '4px'); 
    
    //socialNav.removeClass('open');
    searchWrapper.animate(
      {width: '500px'}, 
      1000, 
      function () {
        //searchInput.css('width','180px');
        searchInput.show();
//        clearButton.fadeIn(1000, function () {
//          $(this).css('display','inline');
//        });

        bindControls('typingSearch');

//        $('.aetv-cse-wrapper').css('z-index', 0);

        if (term) {
          executeSearch(term); 
        }
        else {
          focusField();
        }
      }
    );
  };

  function closeSearch() {
    dlog('close search');
    // nasty fix for unclickable scroller buttons
//    $('#header #header_tools').addClass('search-closed');
    var btnWidth = '0';
    var searchInput = $('#gsc-i-id1');
    var clearButton = $('.gsc-clear-button');
    currentlySearching = false;

//    $('.aetv-cse-wrapper').css('z-index', -1);
	$('.aetv-cse-wrapper').removeClass('open');

//    clearButton.hide();
    if (!_.isUndefined(gso) && gso.getInputQuery() != "") {
      gso.clearAllResults();
    }
    searchOpen = false;
    searchInput.unbind('keypress');
    clearButton.unbind('click');

    if (!_.isUndefined(searchWrapper)) {
      searchWrapper.unbind('mouseleave');
    }
    searchInput.hide();

    if (termIsSaved()) {
      btnWidth = '27';
    }
    else {
      btnWidth = '1';
    }

    if (!_.isUndefined(searchWrapper)) {
      searchWrapper.animate(
        //{ width : btnWidth+'px' },
        //1000,
        function () {
          checkForQuery();
        }
      );
    }
  };

  function dlog(msg) {
    if (typeof(console) != 'undefined' && debugMode) {
      console.log('CSE: '+msg);
    }
  };
  
  module.CloseSearchBar = function(){
	  closeSearch();
  };

  return module;

}(aetv.googleSearch2 || {}, jQuery, $LAB));

(function () {
  aetv.googleSearch2.init();
})();
;
(function(a){a.tiny=a.tiny||{};a.tiny.scrollbar={options:{axis:"y",wheel:40,scroll:true,lockscroll:true,size:"auto",sizethumb:"auto",invertscroll:false}};a.fn.tinyscrollbar=function(d){var c=a.extend({},a.tiny.scrollbar.options,d);this.each(function(){a(this).data("tsb",new b(a(this),c))});return this};a.fn.tinyscrollbar_update=function(c){return a(this).data("tsb").update(c)};function b(q,g){var k=this,t=q,j={obj:a(".viewport",q)},h={obj:a(".overview",q)},d={obj:a(".scrollbar",q)},m={obj:a(".track",d.obj)},p={obj:a(".thumb",d.obj)},l=g.axis==="x",n=l?"left":"top",v=l?"Width":"Height",r=0,y={start:0,now:0},o={},e="ontouchstart" in document.documentElement;function c(){k.update();s();return k}this.update=function(z){j[g.axis]=j.obj[0]["offset"+v];h[g.axis]=h.obj[0]["scroll"+v];h.ratio=j[g.axis]/h[g.axis];d.obj.toggleClass("disable",h.ratio>=1);m[g.axis]=g.size==="auto"?j[g.axis]:g.size;p[g.axis]=Math.min(m[g.axis],Math.max(0,(g.sizethumb==="auto"?(m[g.axis]*h.ratio):g.sizethumb)));d.ratio=g.sizethumb==="auto"?(h[g.axis]/m[g.axis]):(h[g.axis]-j[g.axis])/(m[g.axis]-p[g.axis]);r=(z==="relative"&&h.ratio<=1)?Math.min((h[g.axis]-j[g.axis]),Math.max(0,r)):0;r=(z==="bottom"&&h.ratio<=1)?(h[g.axis]-j[g.axis]):isNaN(parseInt(z,10))?r:parseInt(z,10);w()};function w(){var z=v.toLowerCase();p.obj.css(n,r/d.ratio);h.obj.css(n,-r);o.start=p.obj.offset()[n];d.obj.css(z,m[g.axis]);m.obj.css(z,m[g.axis]);p.obj.css(z,p[g.axis])}function s(){if(!e){p.obj.bind("mousedown",i);m.obj.bind("mouseup",u)}else{j.obj[0].ontouchstart=function(z){if(1===z.touches.length){i(z.touches[0]);z.stopPropagation()}}}if(g.scroll&&window.addEventListener){t[0].addEventListener("DOMMouseScroll",x,false);t[0].addEventListener("mousewheel",x,false);t[0].addEventListener("MozMousePixelScroll",function(z){z.preventDefault()},false)}else{if(g.scroll){t[0].onmousewheel=x}}}function i(A){a("body").addClass("noSelect");var z=parseInt(p.obj.css(n),10);o.start=l?A.pageX:A.pageY;y.start=z=="auto"?0:z;if(!e){a(document).bind("mousemove",u);a(document).bind("mouseup",f);p.obj.bind("mouseup",f)}else{document.ontouchmove=function(B){B.preventDefault();u(B.touches[0])};document.ontouchend=f}}function x(B){if(h.ratio<1){var A=B||window.event,z=A.wheelDelta?A.wheelDelta/120:-A.detail/3;r-=z*g.wheel;r=Math.min((h[g.axis]-j[g.axis]),Math.max(0,r));p.obj.css(n,r/d.ratio);h.obj.css(n,-r);if(g.lockscroll||(r!==(h[g.axis]-j[g.axis])&&r!==0)){A=a.event.fix(A);A.preventDefault()}}}function u(z){if(h.ratio<1){if(g.invertscroll&&e){y.now=Math.min((m[g.axis]-p[g.axis]),Math.max(0,(y.start+(o.start-(l?z.pageX:z.pageY)))))}else{y.now=Math.min((m[g.axis]-p[g.axis]),Math.max(0,(y.start+((l?z.pageX:z.pageY)-o.start))))}r=y.now*d.ratio;h.obj.css(n,-r);p.obj.css(n,y.now)}}function f(){a("body").removeClass("noSelect");a(document).unbind("mousemove",u);a(document).unbind("mouseup",f);p.obj.unbind("mouseup",f);document.ontouchmove=document.ontouchend=null}return c()}}(jQuery));;
;
