if (!aetv) {
    // init aetv namespace
    var aetv = {};
}

aetv.omniture = function() {

  var module = {};
  module.sendOmnitureEvent = function(eventName, eventDescription) {
            //alert('sendOmnitureEvent called with eventName="' + eventName + '", eventDescription="' + eventDescription + '"');
            var isAuth = (readCookie('aetv-tve-auth') == '1' ? '1' : '0');
            var mvpdName = readCookie('aetv-tve-mvpd');
            s.linkTrackVars = 'events,prop55,prop56,eVar55,eVar56';
            s.linkTrackEvents = eventName;
            s.events = eventName;
            s.eVar55 = isAuth;
            s.prop55 = isAuth;
            s.eVar56 = mvpdName;
            s.prop56 = mvpdName;
            s.tl(true, 'o', eventDescription);
            s.linkTrackVars = 'None';
            s.linkTrackEvents = 'None';
  };

  return module;

}();;
(function(doc) {

    var addEvent = 'addEventListener',
        type = 'gesturestart',
        qsa = 'querySelectorAll',
        scales = [1, 1],
        meta = qsa in doc ? doc[qsa]('meta[name=viewport]') : [];

    function fix() {
        meta.content = 'width=device-width,minimum-scale=' + scales[0] + ',maximum-scale=' + scales[1];
        doc.removeEventListener(type, fix, true);
    }

    if ((meta = meta[meta.length - 1]) && addEvent in doc) {
        fix();
        scales = [.25, 1.6];
        doc[addEvent](type, fix, true);
    }

}(document));




jQuery(window).on( 'orientationchange', function (event) {
    //console.log( event );
});




// BUG orientation portrait/lanscape IOS //
if (window.navigator.userAgent.match(/iPhone/i) || window.navigator.userAgent.match(/iPad/i)) {
var viewportmeta = document.querySelector('meta[name="viewport"]');
if (viewportmeta) {
    viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
    document.addEventListener('orientationchange', function () {
        viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1';
    }, false);
  }
}






var aetn;
if( ! aetn ) { aetn = {}; }
if( ! aetn.adobePass ) { aetn.adobePass = {}; }


aetn.adobePass.page = function () {

    var showVideoInfo  = true,
        mvpdframe      = 'mvpdframe',
        mvpddiv        = 'mvpddiv',
        mvpdframeClose = 'mvpdframeClose',
        videoDiv,
        tve            = {
            requestorId  : null,
            flashVersion : {},
            hostname     : 'dev',
            env          : 'dev'
        };



    tve.init = function( inRequestorId, inConfig ) {

        var that = this;

        this.requestorId = inRequestorId;
        this.flashVersion = swfobject.getFlashPlayerVersion();

        if ( !inConfig ) var inConfig = {};

        var toolPrefix    = '';
        var hostnameParts = window.location.hostname.split('.');        
        // var hostname      = hostnameParts.reverse()[2].toLowerCase(); // WHAT !!??
        var hostname      = hostnameParts[0].toLowerCase();

        this.hostname = hostname;

        var inAeSwfUrl = 'https://entitlement.auth.adobe.com/entitlement/AccessEnabler.swf';

        if ( /dev/i.test( hostname ) && hostnameParts.length > 2 ) {

            toolPrefix = 'qa.';
            this.env = 'dev';
            inAeSwfUrl = 'https://entitlement.auth-staging.adobe.com/entitlement/AccessEnabler.swf';

        } else if ( /qa/i.test( hostname ) && hostnameParts.length > 2 ) {

            toolPrefix = 'qa.';
            this.env = 'qa';
            inAeSwfUrl = 'https://entitlement.auth-staging.adobe.com/entitlement/AccessEnabler.swf';

        } else if ( /www/i.test( hostname ) || hostnameParts.length == 2) {

            this.env = 'prod';
            inAeSwfUrl = 'https://entitlement.auth.adobe.com/entitlement/AccessEnabler.swf';

        }

        inAeSwfUrl = inConfig.inAeSwfUrl ? inConfig.inAeSwfUrl : inAeSwfUrl;

        var inSwfId = inConfig.inSwfId ? inConfig.inSwfId : 'ae-swf';
        var inCallbacks = inConfig.callbacks ? inConfig.callbacks : {};


        if ( this.flashVersion.major < 0 && this.isMobile() ) {  // HTML5


                videoDiv = jQuery('#video_modal');

                var videoLoginElement = jQuery('.videoLogin', videoDiv);

                if(jQuery('#login-screen',videoLoginElement)){
                    jQuery('#login-screen',videoLoginElement).remove();
                }

                console.log( videoLoginElement );


                jQuery( 'body' ).append( that.picker_markup( inRequestorId ) );
                jQuery( 'head' ).append( '<style type="text/css" id="mvpd-picker-css">' + that.picker_css( inRequestorId)  + '</style>');
                jQuery( 'head' ).append( "<link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>" );
                jQuery( 'head' ).append( "<link id='google-font-open-sans' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>" );
                jQuery( 'head' ).append( "<link id='google-font-roboto-condensed' href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>" );
                jQuery( 'head' ).append( "<link id='google-font-roboto' href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>" );


                // Clicking the 'X' (close) button on the deeplink prompt
                jQuery('#mvpd-picker-deeplink-prompt-close').click( function () {

                    $this = jQuery( this );

                    if( aetn.videoplayer || $this.data('player') ) {

                        var playerContainer = $this.data('playercontainer'),
                            deepLink        = $this.data('deeplink'),
                            storeLink       = $this.data('storelink'),
                            mpxId           = $this.data('mpxid'),
                            storeDelay      = $this.data('delay');

                        playerContainer.children().css( 'width', '1px').css('height', '1px');

                        jQuery( '#mvpd-picker-deeplink-overlay', playerContainer).remove();
                        playerContainer.append( aetn.adobePass.page.deeplink_overlay_markup( null, deepLink, storeLink, mpxId ) );

                        playerContainer.find('#mvpd-picker-deeplink-overlay-button').data('deeplink', deepLink);
                        playerContainer.find('#mvpd-picker-deeplink-overlay-button').data('storelink', storeLink);
                        playerContainer.find('#mvpd-picker-deeplink-overlay-button').data('delay', storeDelay);

                        playerContainer.find('#mvpd-picker-deeplink-overlay-button').attr('data-deeplink', deepLink);
                        playerContainer.find('#mvpd-picker-deeplink-overlay-button').attr('data-storelink', storeLink);
                        playerContainer.find('#mvpd-picker-deeplink-overlay-button').attr('data-delay', storeDelay);


                        jQuery('#mvpd-picker-deeplink-overlay-button', playerContainer ).on('click', function () {
                            aetn.adobePass.page.handlers.deepLinkClick(this);
                        });
                    }

                    jQuery('#mvpd-picker-overlay').hide();
                    jQuery('#mvpd-picker-deeplink-prompt').hide();
                    jQuery('#mvpd-picker').hide();

                    $this.removeData('playercontainer')
                         .removeData('deeplink')
                         .removeData('storelink')
                         .removeData('mpxid')
                         .removeData('player')
                         .removeData('delay');

                });


                // Clicking the 'WATCH VIDEO' button on the deeplink prompt
                jQuery('#mvpd-picker-deeplink-prompt-button').click( function () {

                    jQuery('#mvpd-picker-overlay').hide();
                    jQuery('#mvpd-picker-deeplink-prompt').hide();
                    jQuery('#mvpd-picker').hide();

                    //aetn.adobePass.page.handlers.deepLinkClick(this);
                    that.handlers.deepLinkClick(this);
                    console.log( '#mvpd-picker-deeplink-prompt-button : click',
                        event
                    );
                    event.preventDefault();

                });



        } else { // Flash

            jQuery.getScript( "http://" + toolPrefix + "cdn.history.com/api/mvpds/" + inRequestorId + ".js", function () {

                that.updateLogos();

                if (jQuery("#" + mvpddiv).length < 1) {
                    jQuery('body')
                        .append(
                            '<div id="' + mvpddiv + '" style="display: none;">' +
                            '<iframe id="' + mvpdframe +'" name="' + mvpdframe + '"></iframe></div>'
                        );
                }

                if (jQuery("#" + mvpdframeClose).length < 1){
                    jQuery("#" + mvpddiv).prepend('<div id="' + mvpdframeClose + '">close</div>');
                }

                aetn.video.adobePass.init({
                    access_enabler_url: inAeSwfUrl,
                    requestor_id:       inRequestorId ,
                    access_enabler_id:  inSwfId,
                    mvpdframe:          '#' + mvpdframe,
                    mvpddiv:            '#' + mvpddiv,
                    mvpdframeClose:     '#' + mvpdframeClose,
                    callbacks:           {
                        closeIFrame:        [
                            function() {
                                jQuery('#mvpd-picker').show();
                            }
                        ]
                    }
                });

                aetn.video.adobePass.addEventListener( 'displayProviderDialog', tve.showLoginScreen );
                aetn.video.adobePass.addEventListener( 'tokenRequestFailed', tve.handlers.errorHandler );

                
                aetn.video.adobePass.addEventListener( 'displayUserAsAuthenticated', tve.displayUserAsAuthenticated );
                if ( that.requestorId === 'AETV' || that.requestorId === 'LIFETIME') {
                    aetn.video.adobePass.addEventListener( 'displayUserAsAuthenticated', tve.showNewsletterForm );
                }
                aetn.video.adobePass.addEventListener( 'displayUserAsUnAuthenticated', tve.displayUserAsUnAuthenticated );

                if ( that.requestorId === 'LIFETIME') {
                    aetn.video.adobePass.addEventListener( 'tokenRequestFailed', tve.showAuthError );
                }


                jQuery.each( inCallbacks, function ( callbacksFor, myCallbacks ) {

                    jQuery.each( inCallbacks[callbacksFor], function( index, callBack ) {
                        aetn.video.adobePass.addEventListener( callbacksFor, callBack );
                    });

                });



                // TODO: Move picker markup rendition here


                videoDiv = jQuery('#video_modal');

                var videoLoginElement = jQuery('.videoLogin', videoDiv);

                if(jQuery('#login-screen',videoLoginElement)){
                    jQuery('#login-screen',videoLoginElement).remove();
                }

                console.log( videoLoginElement );



                //jQuery( videoLoginElement ).append( that.picker_markup( inRequestorId ) );
                jQuery( 'body' ).append( that.picker_markup( inRequestorId ) );
                jQuery( 'head' ).append( '<style type="text/css" id="mvpd-picker-css">' + that.picker_css( inRequestorId)  + '</style>');
                jQuery( 'head' ).append( "<link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>" );
                jQuery( 'head' ).append( "<link id='google-font-open-sans' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>" );
                jQuery( 'head' ).append( "<link id='google-font-roboto-condensed' href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>" );
                jQuery( 'head' ).append( "<link id='google-font-roboto' href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>" );



                // Set click event handlers for behind-the-wall video thumbs/links
                // jQuery( '.video-landing-tile a' ).each( function( i, element ) {

                //   if ( jQuery(this).parents('li.video-landing-tile').data('behind-the-wall') === true ) {

                //     jQuery(this).click( function (event) {
                //         //console.log(' thumbClick0: ', event);
                //         that.handlers.thumbClick(event);
                //     });

                //   }

                // });



                // focus on the MVPD search text field when spy glass is clicked
                jQuery('#mvpd-picker-tier2-top-search').click( function () {
                    jQuery('#mvpd-picker-tier2-top-search-field').focus();
                });


                //  MVPD serch field handlers
                jQuery( '#mvpd-picker-tier2-top-search-field' ).bind( 'keyup', function() {

                    var matches = 0;
                    var filter = jQuery(this).val().trim();

                    if ( filter == 'Search for your TV provider' ) filter = '';

                    var myexp = new RegExp(filter, 'gi');

                    jQuery('#mvpd-picker-tier2-list .mvpd-picker-tier2-entry').each( function() {

                        if ( jQuery(this).text().match(myexp) ) {
                            jQuery(this).show();
                            matches++;
                        } else {
                            jQuery(this).hide();
                        }
                    });

                    //console.log('Matches : ' + matches);
                    jQuery('#mvpd-picker-tier2-top-matches').text(matches + ' match'+ ( matches != 1 ? 'es' : '' ) + ' found');

                })
                .focus( function () {
                    if ( jQuery(this).val() == 'Search for your TV provider' ) {
                        jQuery(this).val('');
                        jQuery('#mvpd-picker-tier2-top-matches').text('');
                    }
                })
                .blur( function () {
                    if ( jQuery(this).val() === '' ) {
                        jQuery(this).val('Search for your TV provider');
                        jQuery('#mvpd-picker-tier2-top-matches').text('');

                    } else if ( jQuery('#mvpd-picker-tier2-top-matches').text() == '0 matches found' ) {

                        jQuery(this).val('');
                        jQuery('#mvpd-picker-tier2-top-matches').text('');
                        jQuery('#mvpd-picker-tier2-list .mvpd-picker-tier2-entry').show();
                    }
                });



                // clicking the 'More TV Providers' button
                jQuery('#mvpd-picker-tier1-bottom-more').click( function() {
                    jQuery('#mvpd-picker-tier1').hide();
                    jQuery('#mvpd-picker-tier2').show();
                    jQuery('#mvpd-picker-faq').hide();
                    jQuery('#mvpd-picker-foot-caption').show();
                });



                // Clicking the 'Back' button on Tier 2 screen
                jQuery('#mvpd-picker-tier2-bottom-back').click( function() {
                    jQuery('#mvpd-picker-tier1').show();
                    jQuery('#mvpd-picker-tier2').hide();
                    jQuery('#mvpd-picker-faq').hide();
                    jQuery('#mvpd-picker-foot-caption').show();
                });



                // Clicking the FAQ button
                jQuery('#mvpd-picker-foot-caption').click( function() {

                    if ( jQuery('#mvpd-picker-tier2').is(':visible') ) {
                        jQuery('#mvpd-picker-faq-bottom-back').data('back-where', 'tier2');
                    } else {
                        jQuery('#mvpd-picker-faq-bottom-back').data('back-where', 'tier1');
                    }

                    jQuery('#mvpd-picker-tier1').hide();
                    jQuery('#mvpd-picker-tier2').hide();
                    jQuery('#mvpd-picker-faq').show();

                    jQuery(this).hide();
                });



                // Clicking the 'Back' button on FAQ screen
                jQuery('#mvpd-picker-faq-bottom-back').click( function() {
                    jQuery('.mvpd-picker-screen').hide();
                    jQuery('#mvpd-picker-' + jQuery(this).data('back-where') ).show();
                    jQuery('#mvpd-picker-foot-caption').show();
                });



                // Clicking the 'X' (close) button on the deeplink prompt
                jQuery('#mvpd-picker-deeplink-prompt-close').click( function () {

                    $this = jQuery( this );

                    if( aetn.videoplayer || $this.data('player') ) {

                        var playerContainer = $this.data('playercontainer'),
                            deepLink        = $this.data('deeplink'),
                            storeLink       = $this.data('storelink'),
                            mpxId           = $this.data('mpxid'),
                            storeDelay      = $this.data('delay');

                        playerContainer.children().css( 'width', '1px').css('height', '1px');

                        jQuery( '#mvpd-picker-deeplink-overlay', playerContainer).remove();
                        playerContainer.append( aetn.adobePass.page.deeplink_overlay_markup( null, deepLink, storeLink, mpxId ) );

                        playerContainer.find('#mvpd-picker-deeplink-overlay-button').data('deeplink', deepLink);
                        playerContainer.find('#mvpd-picker-deeplink-overlay-button').data('storelink', storeLink);
                        playerContainer.find('#mvpd-picker-deeplink-overlay-button').data('delay', storeDelay);

                        playerContainer.find('#mvpd-picker-deeplink-overlay-button').attr('data-deeplink', deepLink);
                        playerContainer.find('#mvpd-picker-deeplink-overlay-button').attr('data-storelink', storeLink);
                        playerContainer.find('#mvpd-picker-deeplink-overlay-button').attr('data-delay', storeDelay);


                        jQuery('#mvpd-picker-deeplink-overlay-button', playerContainer ).on('click', function () {
                            aetn.adobePass.page.handlers.deepLinkClick(this);
                        });
                    }

                    jQuery('#mvpd-picker-overlay').hide();
                    jQuery('#mvpd-picker-deeplink-prompt').hide();
                    jQuery('#mvpd-picker').hide();

                    $this.removeData('playercontainer')
                         .removeData('deeplink')
                         .removeData('storelink')
                         .removeData('mpxid')
                         .removeData('player')
                         .removeData('delay');

                });


                // Clicking the 'WATCH VIDEO' button on the deeplink prompt
                jQuery('#mvpd-picker-deeplink-prompt-button').click( function () {

                    jQuery('#mvpd-picker-overlay').hide();
                    jQuery('#mvpd-picker-deeplink-prompt').hide();
                    jQuery('#mvpd-picker').hide();

                    that.handlers.deepLinkClick(this);
                    console.log( '#mvpd-picker-deeplink-prompt-button : click',
                        event
                    );
                    event.preventDefault();

                });

                // temporary picker "close" handler (if flash is not supported on desktop)
                jQuery('#mvpd-picker').on('click', '#mvpd-picker-head-exit', function () {

                    jQuery('#mvpd-picker-overlay').hide();
                    jQuery('#mvpd-picker-tier1').show();
                    jQuery('#mvpd-picker-tier2').hide();
                    jQuery('#mvpd-picker-faq').hide();
                    jQuery("#mvpd-picker-flash-support-prompt-wrapper").hide();

                });

                jQuery('#mvpd-picker-flash-support-prompt-close').on('click', function() {
                    jQuery('#mvpd-picker-flash-support-prompt-wrapper').hide();
                    jQuery('#mvpd-picker-flash-disabled-prompt-wrapper').hide();
                    jQuery('#mvpd-picker-overlay').hide();
                    jQuery('#mvpd-picker-deeplink-prompt').hide();
                    jQuery('#mvpd-picker').hide();
                })


                jQuery('#mvpd-picker-flash-disabled-prompt-close').on('click', function() {
                    jQuery('#mvpd-picker-flash-support-prompt-wrapper').hide();
                    jQuery('#mvpd-picker-flash-disabled-prompt-wrapper').hide();
                    jQuery('#mvpd-picker-overlay').hide();
                    jQuery('#mvpd-picker-deeplink-prompt').hide();
                    jQuery('#mvpd-picker').hide();
                })


                jQuery('#mvpd-picker-auth-error-close').on('click', function() {
                    jQuery('#mvpd-picker-auth-error-wrapper').hide();
                    jQuery('#mvpd-picker-flash-support-prompt-wrapper').hide();
                    jQuery('#mvpd-picker-flash-disabled-prompt-wrapper').hide();
                    jQuery('#mvpd-picker-overlay').hide();
                    jQuery('#mvpd-picker-deeplink-prompt').hide();
                    jQuery('#mvpd-picker').hide();
                })



                jQuery('#mvpd-picker-newsletter-form-close').on('click', function() {
                    jQuery('#mvpd-picker-newsletter-form-wrapper').hide();
                    jQuery('#mvpd-picker-auth-error-wrapper').hide();
                    jQuery('#mvpd-picker-flash-support-prompt-wrapper').hide();
                    jQuery('#mvpd-picker-flash-disabled-prompt-wrapper').hide();
                    jQuery('#mvpd-picker-overlay').hide();
                    jQuery('#mvpd-picker-deeplink-prompt').hide();
                    jQuery('#mvpd-picker').hide();

                    try {
                        $pdk.controller.clickPlayButton();
                    } catch(err) { /**/ }
                })


                window.onmessage = function( event ) {
                    //console.log( event );
                    if (event.data === "newsletters_closed") {
                        jQuery('#mvpd-picker-newsletter-form-wrapper').hide();
                        jQuery('#mvpd-picker-auth-error-wrapper').hide();
                        jQuery('#mvpd-picker-flash-support-prompt-wrapper').hide();
                        jQuery('#mvpd-picker-flash-disabled-prompt-wrapper').hide();
                        jQuery('#mvpd-picker-overlay').hide();
                        jQuery('#mvpd-picker-deeplink-prompt').hide();
                        jQuery('#mvpd-picker').hide();

                        try {
                            $pdk.controller.clickPlayButton();
                        } catch(err) { /**/ }

                    } else if (event.data === "newsletters_submitted") {

                        aetn.adobePass.page.omniture.registerOptin();

                    }
                };


            });

        }



    };


    tve.isIphone = function () {
      return window.navigator.userAgent.indexOf("iPhone") > 0;
    };


    tve.isIpod = function () {
      return window.navigator.userAgent.indexOf("iPod") > 0;
    };

    tve.isIpad = function () {
      return window.navigator.userAgent.indexOf("iPad") > 0;
    };

    tve.isAndroid = function () {
      return window.navigator.userAgent.toLowerCase().indexOf("android") > -1;
    };

    tve.isMobileAgent = function () {
      return window.navigator.userAgent.toLowerCase().indexOf("mobile") > -1;
    };

    tve.isRetinaDisplay = function () {
      if (window.matchMedia) {
        var mq = window.matchMedia("only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen  and (min-device-pixel-ratio: 1.3), only screen and (min-resolution: 1.3dppx)");
        if (mq && mq.matches || (window.devicePixelRatio > 1)) {
          return true;
        } else {
          return false;
        }
      }
    };

    tve.isMobile = function () {
      return this.isIphone() || this.isIpod() || this.isIpad() || this.isAndroid();
    };


    tve.cookie = function () {

        var module = {};

        module.create = function ( name, value, hours ) {

            var expires = "";

            if ( hours ) {
                var date = new Date();
                date.setTime( date.getTime() + ( hours * 60 * 60 * 1000 ) );
                expires = "; expires=" + date.toGMTString();
            }

            document.cookie = name + "=" + value + expires + "; path=/";
        };


        module.read = function ( name ) {

            var nameEQ = name + "=";
            var ca = document.cookie.split(';');

            for ( var i=0; i < ca.length; i++ ) {

                var c = ca[i];
                while ( c.charAt(0) == ' ' ) c = c.substring( 1, c.length );
                if ( c.indexOf(nameEQ) === 0 ) return c.substring( nameEQ.length, c.length );
            }

            return null;
        };

      return module;

    }();


    tve.handlers = function (tveObject) {

        var that = tveObject;

        var module = {};

        module.thumbClick =  function ( event ) {

            var $this = jQuery( event.currentTarget );

            var mpxId = $this.parent('li.video-landing-tile').data('mpx-id');

            var flashVersion = swfobject.getFlashPlayerVersion();
            // swfobject.hasFlashPlayerVersion("1.0.0");

            if ( flashVersion.major < 1 ) {

                event.stopImmediatePropagation();
                event.preventDefault();
                that.showDeeplinkPrompt ( mpxId, that.requestorId );

            } else {

                event.stopImmediatePropagation();

                if ( aetn.video.adobePass.isAuthenticated() === true ) {
                    return true;
                }

                event.preventDefault();
                jQuery('#mvpd-picker-overlay').show();

                var pat = /^https?:\/\//i;
                var videoPageUrl = $this.attr('href');

                if ( ! pat.test( videoPageUrl ))
                {
                    videoPageUrl = window.location.protocol + '//' + window.location.host + videoPageUrl;
                }

                //aetn.video.adobePass.login( videoPageUrl + '?newsletter_form=true' );
                aetn.video.adobePass.login( videoPageUrl );

            }

        };



        module.videoThumbClick =  function ( videoPageURL, mpxID, inEvent, inPlayer, inContainer ) {

            var mpxId               = ((typeof mpxID != 'undefined') ? mpxID : '0000000'),
                flashVersion        = swfobject.getFlashPlayerVersion(),
                mvpdPickerOverlay   = jQuery("#mvpd-picker-overlay"),
                mvpdPicker          = jQuery("#mvpd-picker"),
                flashPrompt         = jQuery("#mvpd-picker-flash-support-prompt-wrapper"),
                flashDisabledPrompt = jQuery("#mvpd-picker-flash-disabled-prompt-wrapper"),
                player              = ( ( inPlayer !== null && inPlayer !== undefined ) ? inPlayer : undefined ),
                container           = ( ( inContainer !== null && inContainer !== undefined ) ? inContainer : undefined );


            // swfobject.hasFlashPlayerVersion("1.0.0");

            if ( flashVersion.major < 1 && that.isMobile() ) {

                //event.stopImmediatePropagation();
                //event.preventDefault();
                that.showDeeplinkPrompt ( mpxId, that.requestorId, player, container, '.page.js : .handlers.videoThumbClick() ' );

            } else if ( that.flashVersion.major < 1 ) { // No Flash on desktop

                mvpdPicker.hide();
                flashDisabledPrompt.hide();
                mvpdPickerOverlay.show();
                flashPrompt.show();
                return true;

            } else {

                //event.stopImmediatePropagation();

                if ( parent.aetn.video.adobePass.isAuthenticated() === true && parent.aetn.video.adobePass.access_enabler !== null) {

                    return false;
                } else if ( parent.aetn.video.adobePass.access_enabler == null ) {

                    mvpdPicker.hide();
                    mvpdPickerOverlay.show();
                    flashDisabledPrompt.show();
                    flashPrompt.hide();

                    return true;
                }

                //event.preventDefault();
                jQuery('#mvpd-picker-overlay').show();

                var pattern = /^https?:\/\//i;
                var videoPageUrl = (typeof videoPageURL != 'undefined') ? videoPageURL : window.location.href;

                if ( ! pattern.test( videoPageUrl ))
                {
                    videoPageUrl = window.location.protocol + '//' + window.location.host + videoPageUrl;
                }

                //aetn.video.adobePass.login( videoPageUrl + '?newsletter_form=true' );
                aetn.video.adobePass.login( videoPageUrl );

            }

            return true;

        };



        module.deepLinkClick = function ( element ) {

            var $this = jQuery( element );
            var delay = $this.data('delay') || $this.attr('data-delay');
            var deepLink = $this.data('deeplink') || $this.attr('data-deeplink');
            var storeLink = $this.data('storelink') || $this.attr('data-storelink');
            var mpxId = $this.data('mpxid') || $this.attr('data-mpxid');

            console.log( 'AETN:VIDEO:TVE:handlers:deepLinkClick: ',
                {
                    deepLink   : deepLink,
                    storeLink  : storeLink,
                    delay      : delay
                }
            );

            setTimeout( function () {
                window.location = storeLink;
            }, delay);

            window.location = deepLink;

        };


        module.errorHandler = function( inRequestedResourceID, inRequestErrorCode, inRequestDetails ) {

            // TODO make it case insesitive
            aetn.log('Handling error inRequestErrorCode="' + inRequestErrorCode + '", inRequestDetails="' + inRequestDetails + '"');
            if(jQuery('#modal_iframe').attr('src') != "about:blank"){
                videoDiv = jQuery('#modal_iframe').contents().find('#video_modal');
            }else{
                videoDiv = jQuery('#video_modal');
            }
            // Hide and pause the video player
            jQuery('#video-player',videoDiv).css('height', '1px');
            jQuery('#video-player',videoDiv).css('width', '1px');

            if (typeof aetn != "undefined" && aetn.videoplayer){
                    aetn.videoplayer.pauseVideo(true);
            }
            // Display error screen
            jQuery('#parental_control-screen',videoDiv).show();
            jQuery('.parental_control-error',videoDiv).html(inRequestDetails); //"Your parental control settings do not allow you to view this content.");
            jQuery('#cable_control_screen', videoDiv).css('height', jQuery('#cable_control_screen p.parental_control-error', videoDiv).height() + 40);

            if ( that.cookie.read('aetv-tve-omniture_login_complete') != '1' ) {
                //aetn.adobePass.page.omniture.sendOmnitureEvent('event76', 'TV Everywhere Authentication Error');
                that.cookie.create('aetv-tve-omniture_login_complete', '1', 0);
            }

        };


        return module;

    }(tve);


    tve.showLoginScreen = function (tveObject) {

        var that = tveObject;

        return function ( ap, featureProviders, allProviders ) {


            //ap.setSelectedProvider(null);

            var mvpdPickerOverlay = jQuery("#mvpd-picker-overlay"),
                mvpdPicker        = jQuery("#mvpd-picker"),
                flashPrompt       = jQuery("#mvpd-picker-flash-support-prompt-wrapper");

            mvpdPicker.show();
            mvpdPickerOverlay.show();
            flashPrompt.hide();


            jQuery.each( allProviders, function( index, value )  {
                if (value.displayName == 'TWC') allProviders[index].displayName = value.displayName + ' - Time Warner Cable';
                if (value.displayName == 'Optimum') allProviders[index].displayName = value.displayName + ' / Cablevision';
            });


            // #login-screen contains data
            console.log("showLoginScreeen", ap, featureProviders, allProviders);



            // if(jQuery('#modal_iframe').attr('src') != "about:blank"){
            //     videoDiv = jQuery('#modal_iframe').contents().find('#video_modal');
            //     jQuery('#modal_iframe').contents().find('.close-modal').click(function(){
            //         ap.setSelectedProvider('');
            //     });
            // }else{
                videoDiv = jQuery('#video_modal');
            // }

            var videoLoginElement = jQuery('.videoLogin', videoDiv);

            if(jQuery('#login-screen',videoLoginElement)){
                jQuery('#login-screen',videoLoginElement).remove();
            }


            // jQuery('#video-player',videoDiv).css('height', '1px');
            // jQuery('#video-player',videoDiv).css('width', '1px');

            //jQuery('#login',window.parent.document).hide();         // Shut the log-in msg

            jQuery('#cable_control_dropdown_ul li.mvpd-p',videoDiv).remove();// Remove earlier providers
            jQuery('#cable_control_img .icons',videoDiv).remove();// Remove earlier providers

            // closeIFrame
            // aetn.adobepass.page.closeIFrame();
            // or to pause video
            if (typeof aetn != "undefined" && aetn.videoplayer) {
                //aetn.videoplayer.pauseVideo(true);
                try {
                    $pdk.controller.pause(true);
                } catch(err) {

                    try {
                        document.getElementById('modal_iframe').contentWindow.$pdk.controller.pause(true);
                    } catch (err) {
                        console.error( 'Error caught when calling document.getElementById(\'modal_iframe\').contentWindow.$pdk.controller.pause(true)' );
                        console.error( err );

                    };

                }
            }


            // Clear Tier 2 providers
            jQuery('#mvpd-picker-tier2-list .mvpd-picker-tier2-entry').remove();

            for(var index in allProviders) {
                if(allProviders[index].ID == 'Adobe'
                        || allProviders[index].ID == 'Adobe_Redirect'
                        || allProviders[index].displayName == 'undefined') {
                    continue;
                }
                //console.log("Added provider to dropdown: " + allProviders[index].displayName);
                jQuery('#cable_control_dropdown_ul',videoDiv).append('<li data-mvpd-id="' + allProviders[index].ID + '" class="mvpd-p"><a href="javascript:void(0);">' + allProviders[index].displayName + '</a></li>' );
                jQuery('#mvpd-picker-tier2-list').append('<div data-mvpd-id="' + allProviders[index].ID + '" class="mvpd-picker-tier2-entry mvpd-p">' + allProviders[index].displayName + '</div>');
            }



            // Clear Tier 1 providers
            jQuery('#mvpd-picker-tier1-list .mvpd-picker-tier1-entry').remove();

            for(var index in featureProviders) {
                if(featureProviders[index].ID == 'Adobe'
                        || featureProviders[index].ID == 'Adobe_Redirect'
                        || allProviders[index].displayName == 'undefined') {
                    continue;
                }
                //console.log("Added provider to img: " + featureProviders[index].displayName);
                 jQuery('#cable_control_img',videoDiv).append('<div class="icons"><a href="javascript:void(0);"><img class="mvpd-p" data-mvpd-id="' + featureProviders[index].ID + '" src="' + featureProviders[index].logoURL + '" /></a></div>');
                 jQuery('#mvpd-picker-tier1-list').append('<div data-mvpd-id="' + featureProviders[index].ID + '" title="' + featureProviders[index].displayName + '" data-displayName="' + featureProviders[index].displayName +'" class="mvpd-picker-tier1-entry mvpd-p" style="background-image: url(' + featureProviders[index].logoUrl + ')"></div>');
            }


            jQuery('.mvpd-p').each(function(index, node) {

                jQuery(this).click(function() {

                    console.log( 'AETN:VIDEO:TVE:showLoginScreen:<click @.mvpd-p >: ',
                        {
                            that   : that
                        }
                    );

                    jQuery('#mvpd-picker').hide();

                    var mvpdId = jQuery(this).data('mvpd-id');
                    that.cookie.create('aetv-tve-mvpd', mvpdId, 0);
                    that.cookie.create('tveProvider', mvpdId, 0);
                    that.cookie.create('tveAuthStarted', 1, 0);

                    aetn.adobePass.page.omniture.sendOmnitureEvent('event72', 'TV Everywhere Authentication Start');
                    ap.setSelectedProvider(mvpdId);

                });
            });


            jQuery(".ccd_button",videoDiv).click(function() {
                jQuery("#cable_control_dropdown",videoDiv).toggle();
                jQuery("#cable_control_img",videoDiv).toggle();
                jQuery(".ccd_button",videoDiv).toggleClass("ccd_buttonStyle");
            });


            jQuery("#play_full_episode",videoDiv).click(function(evt) {
                jQuery('#login_overlay',videoDiv).remove();
                jQuery('#video-player',videoDiv).css('height', '');
                jQuery('#video-player',videoDiv).css('width', '');
                if (typeof aetn != "undefined" && aetn.videoplayer){
                    aetn.videoplayer.pauseVideo(false);
                }

                jQuery(window.document).trigger({
                  type:"loadNewVideo"
                });

                ap.setSelectedProvider('');

                if ( window.navigator.userAgent.indexOf("iPad") > 0 || window.navigator.userAgent.indexOf("iPhone") > 0
                        || window.navigator.userAgent.toLowerCase().indexOf("android") > -1) {

                    jQuery('#login',window.parent.document).hide();  //  the log-in msg

                }else{

                    jQuery('#login',window.parent.document).show();
                }

            });




            // Clicking the 'X' (close) button on the whole picker
            if ( jQuery('#mvpd-picker').data('bound') !== true ) {

                jQuery('#mvpd-picker').off('click', '#mvpd-picker-head-exit');

                jQuery('#mvpd-picker-head-exit').click( function () {

                    var player = null;
                    var playerContext = null;
                    var modal_iframe = document.getElementById('modal_iframe');

                    if ( modal_iframe !== null &&
                         typeof modal_iframe.contentWindow.aetn !== 'undefined' &&
                         typeof modal_iframe.contentWindow.aetn.videoplayer !== 'undefined' ) {

                        player = modal_iframe.contentWindow.aetn.videoplayer;
                        playerContext = jQuery( modal_iframe.contentWindow.document );

                    } else if ( (typeof aetn !== 'undefined') && (typeof aetn.videoplayer !== 'undefined') ) {

                        player = aetn.videoplayer;
                        playerContext = jQuery(document);

                    }

                    if( player !== null && player.runtime() == 'Flash' && ap.login_called === false ) {
                        jQuery( '#' + player.container, playerContext ).children().css( 'width', '1px').css('height', '1px');


                        try {
                            player.pauseVideo(true);
                        } catch (err) {
                            console.error( 'Error caught when calling player.pauseVideo(true)' );
                            console.error( err );
                        };

                        jQuery( '#mvpd-flow-auth-cancelled-overlay', playerContext ).remove();
                        //jQuery( '#' + aetn.videoplayer.container ).append( that.closed_overlay_markup( that.requestorId) );
                        jQuery( '#' + player.container, playerContext ).append( aetn.adobePass.page.closed_overlay_markup( null ) );
                    } else if ( player && player !== null && ap.login_called === true ) {

                        try {
                            player.pauseVideo(false);
                        } catch (err) {
                            console.error( 'Error caught when calling player.pauseVideo(false)' );
                            console.error( err );
                        };
                    }


                    ap.setSelectedProvider( null );
                    that.cookie.create('preventAutoplay', '', -1);

                    jQuery('#mvpd-picker-overlay').hide();
                    jQuery('#mvpd-picker-tier1').show();
                    jQuery('#mvpd-picker-tier2').hide();
                    jQuery('#mvpd-picker-faq').hide();

                });
                jQuery('#mvpd-picker').data('bound', true);
            }



            jQuery('#close_login',videoDiv).click(function(){

                jQuery('#login_overlay',videoDiv).remove();
                jQuery('#video-player',videoDiv).css('height', '');
                jQuery('#video-player',videoDiv).css('width', '');

                if (typeof aetn != "undefined" && aetn.videoplayer){
                    aetn.videoplayer.pauseVideo(false);
                }

                ap.setSelectedProvider('');

                if (window.navigator.userAgent.indexOf("iPad") > 0 || window.navigator.userAgent.indexOf("iPhone") > 0
                    || window.navigator.userAgent.toLowerCase().indexOf("android") > -1) {

                    jQuery('#login',window.parent.document).hide();  //  the log-in msg

                } else {

                    jQuery('#login',window.parent.document).show();
                }
            });
        };

    }(tve);



    tve.tokenRequestFailed = function ( inRequestedResourceID, inRequestErrorCode, inRequestDetails ) {
        //if ( that.requestor_id === 'LIFETIME') {
            alert( inRequestDetails );
        //}
    };


    tve.showNewsletterForm = function ( tveObject ) {

        var that = tveObject;

        return function ( ap, provider ) {

            console.log ('showNewsletterForm', ap, provider );


            var mvpdPickerOverlay   = jQuery("#mvpd-picker-overlay"),
                mvpdPicker          = jQuery("#mvpd-picker"),
                flashPrompt         = jQuery("#mvpd-picker-flash-support-prompt-wrapper"),
                flashDisabledPrompt = jQuery("#mvpd-picker-flash-disabled-prompt-wrapper"),
                deeplinkPrompt      = jQuery("#mvpd-picker-deeplink-prompt"),
                newsletterForm      = jQuery("#mvpd-picker-newsletter-form-wrapper"),
                errorPrompt         = jQuery("#mvpd-picker-auth-error-wrapper");

            if ( that.cookie.read('newsletter-form-shown') != '1' ) {

                var targetFrame = jQuery('#mvpd-picker-newsletter-form-iframe')[0];

                /* Show newsletter form */

                mvpdPicker.hide();
                flashPrompt.hide();
                flashDisabledPrompt.hide();
                deeplinkPrompt.hide();
                errorPrompt.hide();
                newsletterForm.show();
                mvpdPickerOverlay.show();

                that.cookie.create('tveAuth-ShowNewsletterForm', '', -1);
                that.cookie.create('newsletter-form-shown', '1', 2160);

                targetFrame.contentWindow.postMessage('mvpd_provider:' + provider.ID + ':' + provider.displayName, '*');

            } else {

                try {
                    $pdk.controller.clickPlayButton();
                } catch(err) { /**/ }
            }


        };


    }(tve);


    tve.displayUserAsAuthenticated = function ( tveObject ) {

        var that = tveObject;

        return function( ap, provider ) {

            if ( window.location.search.indexOf("newsletter_form=reset") > -1 ) {

                that.cookie.create('newsletter-form-shown', '', -1);
                console.log('######################### displayUserAsAuthenticated #########################');

            }


            console.log( 'AETN:VIDEO:TVE:x: ',
                {
                    that   : that
                }
            );

            aetn.log('displayUserAsAuthenticated');

            jQuery('body').addClass('aetvUserAuthenticated');

            jQuery('#login',window.parent.document).hide();
            jQuery('#logout',window.parent.document).show();
            jQuery('#logout',window.parent.document).click(function() {
                 aetn.video.adobePass.logout();
                 return false;
            });

            var logoURLBL;
            var providerURL;
            var allProviders = aetn.video.adobePass.provider_list;

            for( var index in allProviders) {

                if(allProviders[index].ID == provider.ID) {

                    // Determine logoURLBL
                    logoURLBL = allProviders[index].displayLogoURLAE_black_bg;

                    // fallback to displayLogoURLAE
                    if(logoURLBL === '' || logoURLBL === null) {
                        logoURLBL = allProviders[index].displayLogoURLAE;
                    }

                    // fallback to displayLogoURL
                    if(logoURLBL === '' || logoURLBL === null) {
                        logoURLBL = allProviders[index].displayLogoURL;
                    }

                    // Determine provider URL
                    providerURL = allProviders[index].providerURLAE;

                    if(providerURL === '' || providerURL === null) {
                        providerURL = allProviders[index].providerURL;
                    }

                    break;
                }
            }

            aetn.log('DEBUG: Setting aetv-tve-auth=1 | aetv-tve-mvpd=' + provider.ID + ' | tveProvider=' + provider.ID );
            that.cookie.create('aetv-tve-auth', '1', 0);
            that.cookie.create('TVEAuthenticated', '1', 0);
            that.cookie.create('aetv-tve-mvpd', provider.ID, 0);
            that.cookie.create('tveProvider', provider.ID, 0);

            if(logoURLBL !== '' && logoURLBL !== null) {

                aetn.log('DEBUG: Setting aetv-tve-mvpdlogo-bl=' + logoURLBL + ' | aetv-tve-mvpdlink=' + providerURL );
                that.cookie.create('aetv-tve-mvpdlogo-bl', logoURLBL, 0);
                that.cookie.create('aetv-tve-mvpdlink', providerURL, 0);

                jQuery(document.body).addClass('logged_in');
                if(typeof(providerURL) == 'undefined' || providerURL == 'undefined' || providerURL === ''){
                    jQuery('#provider .black',window.parent.document).html('<img ' + ' src="' + logoURLBL + '" />' );
                }else{
                    jQuery('#provider .black',window.parent.document).html('<a target="_blank" href="' + providerURL + '"><img ' + ' src="' + logoURLBL + '" /></a>' );
                }
                jQuery('#provider .black',window.parent.document).show();
            }

            // avoid duplicating the auth complete event everytime the updatePageUI callback is called.
            if( that.cookie.read('aetv-tve-omniture_login_complete') != '1' &&
                that.cookie.read('tveAuthStarted') == 1 )
            {
                //alert('TV Everywhere Authentication Complete');
                aetn.adobePass.page.omniture.sendOmnitureEvent('event73', 'TV Everywhere Authentication Complete');
                aetn.log('DEBUG: Setting aetv-tve-omniture_login_complete cookie to value "1"');
                that.cookie.create('tveAuthStarted', '', -1);
                that.cookie.create('aetv-tve-omniture_login_complete', '1', 0);
                that.cookie.create('tveAuth-ShowNewsletterForm', '1', 0);
            }

            if ( window.location.search.indexOf("newsletter_form=reset") > -1 ) {
                that.cookie.create('newsletter-form-shown', '', -1);
            } 

        };

    }(tve);


    tve.getDocument = function () {
        return jQuery(document);

    };


    tve.displayUserAsUnAuthenticated = function ( tveObject ) {

        var that = tveObject;

        return function( ap ) {

            //alert('in displayUserAsUnAuthenticated');

            if(jQuery('#modal_iframe').attr('src') != "about:blank") {
                videoDiv = jQuery('#modal_iframe').contents().find('#video_modal');
                if(aetn.videoplayer.runtime() == 'Flash'){
                    jQuery('#login',window.parent.document).show();
                } else {
                    jQuery('#login',window.parent.document).hide();
                }
            } else {
                videoDiv = jQuery('#video_modal');
            }

            aetn.log('displayUserAsUnAuthenticated');

            jQuery('body').removeClass('aetvUserAuthenticated');
            jQuery('#logout',window.parent.document).hide();
            jQuery('#provider .black',window.parent.document).hide();
            // undo the other style changes made when logo exists and user is newly Auth'ed
            jQuery(document.body).removeClass('logged_in');
            jQuery('div.search',window.parent.document).css('right', '0');
            jQuery('#login',window.parent.document).click(function() {
               aetn.video.adobePass.login();
               return false;
            });
            // Don't turn-on login button if provider buttons are already showing
            var provider_list_on = jQuery('#cable_control #cable_control_drop .mvpd-p',videoDiv).length;
            aetn.log('provider_list_on: ' + provider_list_on);
            if(provider_list_on == 0 || provider_list_on == null) {
                //if(jQuery('#video-player',videoDiv).length > 0 && aetn.videoplayer.runtime() == 'Flash'){ // display sigin-in link only on video page
                if( swfobject.getFlashPlayerVersion().major > 0 ) {
                    jQuery('#login',window.parent.document).show();
                }else{
                    jQuery('#login',window.parent.document).hide();
                }
            }

            console.log( 'AETN:VIDEO:TVE:displayUserAsUnAuthenticated: ',
                {
                    that   : that
                }
            );

            that.cookie.create('aetv-tve-auth', '', -1);
            that.cookie.create('TVEAuthenticated', '', -1);
            that.cookie.create('aetv-tve-omniture_login_complete', '', -1);
            that.cookie.create('aetv-tve-mvpd', '', -1);
            that.cookie.create('tveProvider', '', -1);
            that.cookie.create('aetv-tve-mvpdlogo-bl', '', -1);
            that.cookie.create('aetv-tve-mvpdlink', '', -1);
            aetn.log('DEBUG: Deleting aetv-tve-auth, aetv-tve-omniture_login_complete, aetv-tve-mvpd, tveProvider, aetv-tve-mvpdlogo-bl, aetv-tve-mvpdlink');

        };

    }(tve);



    tve.getRequestor = function ( requestor_id ) {

        var requestors = {

            AETV : {
                brand_name        : 'A&E',
                mobile_app_site   : 'AETV',
                mobile_app_handle : 'aetvplus',
                ios_app_link      : 'https://itunes.apple.com/us/app/a-e/id571711580?mt=8',
                android_app_link  : 'https://play.google.com/store/apps/details?id=com.aetn.aetv.watch',
                kindle_app_link   : 'http://www.amazon.com/dp/B00E5NGYVM',
                accent_color      : '#007cbe',
                picker_class      : 'mvpd-picker-aetv',
                flash_help        : 'https://aetn.zendesk.com/hc/en-us/articles/202317089-Do-I-Need-To-Download-Or-Install-Any-Special-Software-To-Watch-The-Videos-',
                newsletter_form   : 'player.aetndigital.com/pservice/player/aetn.video.player/js/aetn/video/tve/newsletter-forms/ae.html'
            },

            HISTORY : {
                brand_name        : 'History',
                mobile_app_site   : 'HIS',
                mobile_app_handle : 'historyplus',
                ios_app_link      : 'https://itunes.apple.com/us/app/history/id576009463?mt=8',
                android_app_link  : 'https://play.google.com/store/apps/details?id=com.aetn.history.watch',
                kindle_app_link   : 'http://www.amazon.com/dp/B00E5NH46Q',
                accent_color      : '#fa2c00',
                picker_class      : 'mvpd-picker-history',
                flash_help        : 'https://history.zendesk.com/hc/en-us/articles/203020045-Do-I-need-to-download-or-install-any-special-software-to-watch-the-videos-',
                newsletter_form   : 'player.aetndigital.com/pservice/player/aetn.video.player/js/aetn/video/tve/newsletter-forms/history.html'
            },

            LIFETIME : {
                brand_name        : 'Lifetime',
                mobile_app_site   : 'MYL',
                mobile_app_handle : 'lifetimeplus',
                ios_app_link      : 'https://itunes.apple.com/us/app/lifetime/id579966222?mt=8',
                android_app_link  : 'https://play.google.com/store/apps/details?id=com.aetn.lifetime.watch',
                kindle_app_link   : 'http://www.amazon.com/dp/B00E5NH6YG',
                accent_color      : '',
                picker_class      : 'mvpd-picker-lifetime',
                flash_help        : 'https://mylifetime.zendesk.com/hc/en-us/articles/202274269-Do-I-need-to-download-or-install-any-special-software-to-watch-the-videos-',
                newsletter_form   : 'player.aetndigital.com/pservice/player/aetn.video.player/js/aetn/video/tve/newsletter-forms/lifetime.html',
                newsletter_form_h : '570px'
            },

            FYI : {
                brand_name        : 'FYI',
                mobile_app_site   : 'FYI',
                mobile_app_handle : 'fyiplus',
                ios_app_link      : 'https://itunes.apple.com/us/app/fyi-tv/id887597381?mt=8',
                android_app_link  : '',
                kindle_app_link   : '',
                accent_color      : '',
                picker_class      : 'mvpd-picker-fyi',
                flash_help        : 'http://helpx.adobe.com/flash-player.html',
                newsletter_form   : 'player.aetndigital.com/pservice/player/aetn.video.player/js/aetn/video/tve/newsletter-forms/fyi.html'
            }
        };

        return requestors[requestor_id] ? requestors[requestor_id] : {brand_name: 'unspecified'};

    };


    tve.showDeeplinkPrompt = function ( mpxId, requestor_id, inPlayer, playerContainer, caller ) {

        console.log ( 'showDeeplinkPrompt CALLED with following parameters: ',
            {
                mpxId           : mpxId,
                requestor_id    : requestor_id,
                inPlayer        : inPlayer,
                playerContainer : playerContainer,
                caller          : caller
            }
        );

        var requestorId = (requestor_id && requestor_id !== null) ? requestor_id : this.requestorId;

        var requestorInfo = this.getRequestor( requestorId );
        var deeplink = requestorInfo.mobile_app_handle + '://feature/select/tpid/' + mpxId +
                       '?cmpid=CNP_' + requestorInfo.mobile_app_site + '_Deeplink&utm_source=CNP&utm_medium=' +
                       requestorInfo.mobile_app_site + '&utm_campaign=Deeplink';

        var isiOS      = /ipad|iphone|ipod/i.test(window.navigator.userAgent.toLowerCase());
        var isAndroid  = /android/i.test(window.navigator.userAgent.toLowerCase());
        var isKindle   = /kindle/i.test(window.navigator.userAgent.toLowerCase());
        var storeLink  = '';

        //var storeDelay = document.location.search.substring(1) != '' ? document.location.search.substring(1) : 300;
        var storeDelay = 300;

        var $closeButton = jQuery('#mvpd-picker-deeplink-prompt-close');
        var $watchButton = jQuery('#mvpd-picker-deeplink-prompt-button');

        if (isiOS) {
            storeLink = requestorInfo.ios_app_link;
        }
        else if (isKindle) {
            storeLink = requestorInfo.kindle_app_link;
            storeDelay = 20;
        }
        else if (isAndroid) {
            storeLink = requestorInfo.android_app_link;
            storeDelay = 20;
        }

        var customData = {
            mpxid     : mpxId,
            deeplink  : deeplink,
            storelink : storeLink,
            delay     : storeDelay
        };

        $watchButton.data( customData );
        $closeButton.data( customData );

        if ( playerContainer ) {
            $closeButton.data('playercontainer', playerContainer);
        }

        if ( typeof inPlayer != 'undefined' ) {

            inPlayer.pauseVideo( true );
            $closeButton.data('player', inPlayer);

        } else {

            $closeButton.data('player', null);
        }

        jQuery('#mvpd-picker-overlay').show();
        jQuery('#mvpd-picker-deeplink-prompt').show();
        jQuery('#mvpd-picker').hide();

    };


    tve.showNoFlashPrompt = function () {
        var mvpdPickerOverlay   = jQuery("#mvpd-picker-overlay"),
            mvpdPicker          = jQuery("#mvpd-picker"),
            flashPrompt         = jQuery("#mvpd-picker-flash-support-prompt-wrapper"),
            flashDisabledPrompt = jQuery("#mvpd-picker-flash-disabled-prompt-wrapper");

        mvpdPicker.hide();
        flashDisabledPrompt.hide();
        mvpdPickerOverlay.show();
        flashPrompt.show();
    };


    tve.showFlashDisabledPrompt = function () {
        var mvpdPickerOverlay   = jQuery("#mvpd-picker-overlay"),
            mvpdPicker          = jQuery("#mvpd-picker"),
            flashPrompt         = jQuery("#mvpd-picker-flash-support-prompt-wrapper"),
            flashDisabledPrompt = jQuery("#mvpd-picker-flash-disabled-prompt-wrapper");

        mvpdPicker.hide();
        flashPrompt.hide();
        mvpdPickerOverlay.show();
        flashDisabledPrompt.show();
    };


    tve.showAuthError = function( inRequestedResourceID, inRequestErrorCode, inRequestDetails ) {
        var mvpdPickerOverlay   = jQuery("#mvpd-picker-overlay"),
            mvpdPicker          = jQuery("#mvpd-picker"),
            flashPrompt         = jQuery("#mvpd-picker-flash-support-prompt-wrapper"),
            flashDisabledPrompt = jQuery("#mvpd-picker-flash-disabled-prompt-wrapper"),
            authErrorMessage    = jQuery("#mvpd-picker-auth-error-message"),
            authError           = jQuery("#mvpd-picker-auth-error-wrapper");

        authErrorMessage.text( inRequestDetails );
        mvpdPicker.hide();
        flashPrompt.hide();
        flashDisabledPrompt.hide();
        mvpdPickerOverlay.show();
        authError.show();
    };


    tve.omniture = function(tveObject) {

        var that = tveObject,
        module = {};

        module.sendOmnitureEvent = function(eventName, eventDescription) {

            var isAuth = (that.cookie.read('TVEAuthenticated') == '1' ? '1' : '0');
            var mvpdName = that.cookie.read('tveProvider');

            s.linkTrackVars   = 'events,prop55,prop56,eVar55,eVar56';
            s.linkTrackEvents = eventName;
            s.events          = eventName;
            s.eVar55          = isAuth;
            s.prop55          = isAuth;
            s.eVar56          = mvpdName;
            s.prop56          = mvpdName;

            s.tl( true, 'o', eventDescription );

            s.linkTrackVars   = 'None';
            s.linkTrackEvents = 'None';
        };

        module.registerOptin = function() {

            console.log( "aetn.adobePass.page.omniture.registerOptin() - this == ", this );
            s.linkTrackVars             = "eVar13,events";
            s.linkTrackEvents           = "event43";
            s.eVar13                    = "Post Auth Email Opt In";
            s.events                    = "event43";
            s.useForcedLinkTracking     = true;
            s.forcedLinkTrackingTimeout = 1000;

            window.s.tl( this, 'o', 'PostAuthentication', null, 'navigate' );
            s.eVar13 = "";
            s.events = "";
        };

      return module;

    }(tve);


    tve.updateLogos = function () {

        if (aetn.mvpds.ShawGo) {
            aetn.mvpds.ShawGo.logoUrl = 'http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/tier1/shaw.png';
        }

        if (aetn.mvpds.DTV) {
            aetn.mvpds.DTV.logoUrl = 'http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/tier1/directtv.png';
        }

        if (aetn.mvpds.Brighthouse) {
            aetn.mvpds.Brighthouse.logoUrl = 'http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/tier1/bright-house.png';
        }

        if (aetn.mvpds.Comcast_SSO) {
            aetn.mvpds.Comcast_SSO.logoUrl = 'http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/tier1/xfinity.png';
        }

        if (aetn.mvpds.Cablevision) {
            aetn.mvpds.Cablevision.logoUrl = 'http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/tier1/optimum.png';
        }

        if (aetn.mvpds.TWC) {
            aetn.mvpds.TWC.logoUrl = 'http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/tier1/twc.png';
        }

        if (aetn.mvpds.Verizon) {
            aetn.mvpds.Verizon.logoUrl = 'http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/tier1/verizon-fios.png';
        }

        if (aetn.mvpds['telus_auth-gateway_net']) {
            aetn.mvpds['telus_auth-gateway_net'].logoUrl = 'http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/tier1/telus.png';
        }

        if (aetn.mvpds.Dish) {
            aetn.mvpds.Dish.logoUrl = 'http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/tier1/dish.png';
        }

    };


    tve.closed_overlay_markup = function (requestor_id) {

        var requestorId = (requestor_id && requestor_id !== null) ? requestor_id : this.requestorId;
        var brand_name = this.getRequestor(requestorId).brand_name;

        return '<div id="mvpd-flow-auth-cancelled-overlay">' +
               '    <div id="mvpd-flow-auth-cancelled-overlay-container">' +
               '        <div id="mvpd-flow-auth-cancelled-overlay-message">Sign in to watch all available ' + brand_name + ' videos. It\'s FREE!</div>' +
               '        <div id="mvpd-flow-auth-cancelled-overlay-button" onclick="parent.aetn.video.adobePass.login( document.location.href )">Sign in</div>' +
               '    </div>' +
               '</div>';

    };


    tve.deeplink_overlay_markup = function (requestor_id, deepLink, storeLink, mpxId ) {

        var requestorId     = (requestor_id && requestor_id !== null) ? requestor_id : this.requestorId,
            requestorInfo   = this.getRequestor(requestorId),
            brand_name      = requestorInfo.brand_name,
            picker_class    = requestorInfo.picker_class;

        return '<div id="mvpd-picker-deeplink-overlay">' +
        '    <div id="mvpd-picker-deeplink-overlay-close"></div>' +
        '    <div id="mvpd-picker-deeplink-overlay-message">You can access this video by signing in with your TV provider on the Free  ' + brand_name + '  App</div>' +
        '    <a id="mvpd-picker-deeplink-overlay-button" href="#">Watch video</a>' +
        '</div>';

    };


    tve.picker_markup  = function ( requestor_id ) {

        var brand_name   = this.getRequestor(requestor_id).brand_name,
            picker_class = this.getRequestor(requestor_id).picker_class,
            flash_help   = this.getRequestor(requestor_id).flash_help,
            newsletter_form = this.getRequestor(requestor_id).newsletter_form,
            playerHostPrefix = ( this.env == 'prod' ) ? '' : this.env;

        return '<div id="mvpd-picker-overlay" class="' + picker_class + '">' +
        '' +
        '' +
        '' +
        '' +
        '  <div id="mvpd-picker-deeplink-prompt" style="display:none">' +
        '      <div id="mvpd-picker-deeplink-prompt-close"></div>' +
        '      <div id="mvpd-picker-deeplink-prompt-message">You can access this video by signing in with your TV provider on the Free  ' + brand_name + '  App</div>' +
        '      <a id="mvpd-picker-deeplink-prompt-button" href="#">Watch video</a>' +
        '  </div>' +
        '' +
        '' +
        '' +
        '' +
        '  <div id="mvpd-picker-flash-support-prompt-wrapper" style="display:none">' +
        '    <div id="mvpd-picker-flash-support-prompt">' +
        '        <div id="mvpd-picker-flash-support-prompt-header">This video requires a recent version of Adobe Flash Player</div>' +
        '        <div id="mvpd-picker-flash-support-prompt-message">Please install or update your Adobe Flash Player. Once installed you will be able to sign in with your TV provider and gain access to all available ' + brand_name + ' videos.</div>' +
        '        <a id="mvpd-picker-flash-support-prompt-button" href="http://get.adobe.com/flashplayer/"><span>Get Adobe Flash Player</span></a>' +
        '        <a id="mvpd-picker-flash-support-prompt-help" href="' + flash_help + '">Help</a>' +
        '    </div>' +
        '    <div id="mvpd-picker-flash-support-prompt-close"></div>' +
        '  </div>' +
        '' +
        '' +
        '' +
        '' +
        '  <div id="mvpd-picker-flash-disabled-prompt-wrapper" style="display:none">' +
        '    <div id="mvpd-picker-flash-disabled-prompt">' +
        '        <div id="mvpd-picker-flash-disabled-prompt-header">Sorry, something went wrong with Adobe Flash Player.</div>' +
        '        <div id="mvpd-picker-flash-disabled-prompt-message">Click the link below and follow the steps to determine if Adobe Flash Player is enabled on your browser.​ Once enabled you will be able to sign in with your TV provider and gain access to all available ' + brand_name + ' videos.</div>' +
        '        <a id="mvpd-picker-flash-disabled-prompt-button" href="http://helpx.adobe.com/flash-player.html"><span>Flash Help</span></a>' +
        '    </div>' +
        '    <div id="mvpd-picker-flash-disabled-prompt-close"></div>' +
        '  </div>' +
        '' +
        '' +
        '' +
        '' +
        '  <div id="mvpd-picker-newsletter-form-wrapper" style="display:none">' +
        '    <div id="mvpd-picker-newsletter-form">' +
        '        <iframe id="mvpd-picker-newsletter-form-iframe" name="signup" src="http://' + playerHostPrefix + newsletter_form +'" frameborder="0" scrolling="no" seamless></iframe>' +
        '    </div>' +
        '    <div id="mvpd-picker-newsletter-form-close"></div>' +
        '  </div>' +
        '' +
        '' +
        '' +
        '' +
        '  <div id="mvpd-picker-auth-error-wrapper" style="display:none">' +
        '    <div id="mvpd-picker-auth-error">' +
        '        <div id="mvpd-picker-auth-error-header">Sorry, an error has occurred.<!-- There was a problem granting you access to this video. --></div>' +
        '        <div id="mvpd-picker-auth-error-message">Something went wrong</div>' +
        '        <div id="mvpd-picker-auth-error-button"></div>' +
        '    </div>' +
        '    <div id="mvpd-picker-auth-error-close"></div>' +
        '  </div>' +
        '' +
        '' +
        '' +
        '' +
        '  <div id="mvpd-picker">' +
        '    <div id="mvpd-picker-head">' +
        '      <div id="mvpd-picker-head-caption">Sign in to watch all available ' + brand_name + ' videos. It\'s FREE!</div>' +
        '      <div id="mvpd-picker-head-exit"></div>' +
        '    </div>' +
        '    <div id="mvpd-picker-main">' +
        '' +
        '' +
        '' +
        '' +
        '' +
        '      <div id="mvpd-picker-tier1" class="mvpd-picker-screen">' +
        '        <div id="mvpd-picker-tier1-top">' +
        '          Select your TV provider:' +
        '        </div>' +
        '        <div id="mvpd-picker-tier1-list"></div>' +
        '        <div id="mvpd-picker-tier1-bottom">' +
        '          <button id="mvpd-picker-tier1-bottom-more">More TV Providers</button>' +
        '        </div>' +
        '      </div>' +
        '' +
        '' +
        '' +
        '' +
        '' +
        '      <div id="mvpd-picker-tier2" class="mvpd-picker-screen">' +
        '        <div id="mvpd-picker-tier2-top">' +
        '          <div id="mvpd-picker-tier2-top-search">' +
        '            <input id="mvpd-picker-tier2-top-search-field" type="text" value="Search for your TV provider">' +
        '          </div>' +
        '          <div id="mvpd-picker-tier2-top-matches"></div>' +
        '        </div>' +
        '        <div id="mvpd-picker-tier2-list"></div>' +
        '        <div id="mvpd-picker-tier2-bottom">' +
        '          <button id="mvpd-picker-tier2-bottom-back">Back</button>' +
        '        </div>' +
        '      </div>' +
        '' +
        '' +
        '' +
        '      <div id="mvpd-picker-faq" class="mvpd-picker-screen">' +
        '        <div id="mvpd-picker-faq-top">' +
        '          Frequently Asked Questions' +
        '        </div>' +
        '        <div id="mvpd-picker-faq-main">' +
        '          <h4>My TV provider is not listed. Why not?</h4>' +
        '          <div class="pane">' +
        '            <p>' +
        '            We are currently working on adding more TV providers. Please check back frequently to see if your TV provider has been added.' +
        '            </p>' +
        '          </div>' +
        '          <h4>Why do I need to log in to watch some video content?</h4>' +
        '          <div class="pane">' +
        '            <p>' +
        '            Viewers who verify their subscription to a TV provider get access to a deeper catalog of video content, including more full episodes. If you cannot log in because your TV provider is not currently supported, you will still have a lot of video to watch and check back soon because we\'ll be adding more TV providers in the coming months.' +
        '            </p>' +
        '          </div>' +
        '          <h4>I am able to watch ' + brand_name + ' on TV. Am I already registered for this service?</h4>' +
        '          <div class="pane">' +
        '            <p>This service is only available through participating TV providers. To access it, you will need a subscription from a participating TV provider and a high-speed internet connection. Please contact your TV provider to find out if they are participating in this offering.' +
        '            </p>' +
        '          </div>' +
        '          <h4>How much does it cost to access all of the video content? </h4>' +
        '          <div class="pane">' +
        '            <p>' +
        '            Accessing video content is free, however, you will need to verify your TV provider subscription by logging in in order to access all of our video content. It doesn\'t cost anything extra than your current TV provider subscription. And, don\'t worry, if your TV provider is not listed, we still have plenty of video for you to watch.' +
        '            </p>' +
        '          </div>' +
        '          <h4>Can I watch videos if my TV provider isn\'t currently supported? </h4>' +
        '          <div class="pane">' +
        '            <p>' +
        '            Yes! There are lots of full episodes and web exclusives to watch - even if your TV provider isn\'t currently supported.' +
        '            </p>' +
        '          </div>' +
        '          <h4>What kind of programming is available if I log in?</h4>' +
        '          <div class="pane">' +
        '            <p>' +
        '            You will get access to more full episodes than ever before.' +
        '            </p>' +
        '          </div>' +
        '          <h4>How often is new video added to the website? </h4>' +
        '          <div class="pane">' +
        '            <p>' +
        '            There will be new episodes and web exclusives added every day.' +
        '            </p>' +
        '          </div>' +
        '          <h4>How quickly does a new episode get added after it airs on TV?</h4>' +
        '          <div class="pane">' +
        '            <p>' +
        '            Generally speaking, new episodes will be available the morning after they air.' +
        '            </p>' +
        '          </div>' +
        '          <h4>Is there a limit to how much video I can watch on your website? </h4>' +
        '          <div class="pane">' +
        '            <p>' +
        '            There is no limit.' +
        '            </p>' +
        '          </div>' +
        '          <h4>How is this service different from ' + brand_name + ' and ' + brand_name + ' On Demand on my TV?</h4>' +
        '          <div class="pane">' +
        '            <p>While our On Demand offerings complement your viewing experience on your TV, this service is a new way to experience our programming right on your computer through your high-speed internet connection.' +
        '            </p>' +
        '          </div>' +
        '          <h4>What countries are able to view ' + brand_name + ' full episodes?</h4>' +
        '          <div class="pane">' +
        '            <p>' +
        '            Full episodes can be viewed only in the United States.' +
        '            </p>' +
        '          </div>' +
        '          <h4>Why isn\'t my favorite show available? </h4>' +
        '          <div class="pane">' +
        '            <p>' +
        '            We release as many full episodes as possible. Check back often because we add new video every day.' +
        '            </p>' +
        '          </div>' +
        '          <h4>Where can I watch full episodes on your website? Do I have to be at home? </h4>' +
        '          <div class="pane">' +
        '            <p>' +
        '            You can watch full episodes from any computer in the United States with a high-speed internet connection. You can also download our ' + brand_name + ' mobile watch app.' +
        '            </p>' +
        '          </div>' +
        '          <h4>The episode I am looking for is no longer available. Why?</h4>' +
        '          <div class="pane">' +
        '            <p>' +
        '            Our video content is constantly being updated. Check back as old episodes are often made available again.' +
        '            </p>' +
        '          </div>' +
        '          <!-- <h4>How can I contact ' + brand_name + ' if I need video support?</h4>' +
        '          <div class="pane">' +
        '            <p>' +
        '            Please <a href="#">click here</a> to contact us with specifics.' +
        '            </p>' +
        '          </div> -->' +
        '        </div>' +
        '        <div id="mvpd-picker-faq-bottom">' +
        '          <button id="mvpd-picker-faq-bottom-back" data-back-where="tier1">Back</button>' +
        '        </div>' +
        '      </div>' +
        '' +
        '' +
        '    </div>' +
        '    <div id="mvpd-picker-foot">' +
        '      <div id="mvpd-picker-foot-caption">FAQ</div>' +
        '    </div>' +
        '  </div>' +
        '</div>';

    };


    tve.picker_css  = function ( requestor_id ) {

        var requestorId = (requestor_id && requestor_id !== null) ? requestor_id : this.requestorId;
        var requestorInfo = this.getRequestor(requestorId);
        var accent_color = requestorInfo.accent_color ? requestorInfo.accent_color : "#5b544f";
        var newsletter_form_h = requestorInfo.newsletter_form_h ? requestorInfo.newsletter_form_h : "603px";

        return "\
        #mvpd-picker-overlay {\
          display: none;\
          position: fixed;\
          left: 0px;\
          top: 0px;\
          width:100%;\
          height:100%;\
          z-index: 100000;\
          background-image: url(http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/assets/main-overlay-bg.png);\
        }\
        \
        \
        #mvpd-picker {\
          max-width: 788px;\
          width: calc(100% - 100px);\
          height: 520px;\
          background-color: #eeeeee;\
          margin: 100px auto;\
          -webkit-box-shadow: 0px 0px 31px 13px rgba(0,0,0,0.78);\
          -moz-box-shadow: 0px 0px 31px 13px rgba(0,0,0,0.78);\
          box-shadow: 0px 0px 31px 13px rgba(0,0,0,0.78);\
        }\
        \
        #mvpd-picker-head {\
          width: 100%;\
          height: 54px;\
          background-color: " + accent_color + ";\
          position: relative;\
        }\
        \
        #mvpd-picker-head-caption {\
          color: #eee;\
          font-size: 25px;\
          font-weight: 600;\
          font-family: 'Open Sans Semibold', 'OpenSans-Semibold', 'Open Sans';\
          font-style: normal;\
          left: 34px;\
          position: absolute;\
          top: 9px;\
        }\
        \
        \
        #mvpd-picker-head-exit {\
          background-image: url( http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/assets/picker-close.png );\
          height: 33px;\
          width: 33px;\
          position: absolute;\
          right: -18px;\
          top: -18px;\
          cursor: pointer;\
        \
        }\
        \
        #mvpd-picker-main {\
          height: auto;\
          width: calc(100% - 32px);\
          overflow-y: hidden;\
          overflow-x: visible;\
          margin-left: 32px;\
        }\
        \
        #mvpd-picker-foot {\
          background-color: "+ accent_color + ";\
          color: #a29e9b;\
          font-family: 'Open Sans Semibold', 'OpenSans-Semibold', 'Open Sans';\
          font-weight: 400;\
          font-size: 16px;\
          font-style: normal;\
          height: 31px;\
          position: relative;\
        \
        \
        }\
        \
        #mvpd-picker-foot-caption {\
          position: absolute;\
          display: inline-block;\
          left: 31px;\
          top: 8px;\
          cursor: pointer;\
          height: 15px;\
          line-height: 15px;\
          color: #f6f1e7;\
        }\
        \
        #mvpd-picker-foot-caption:hover {\
          color: #fff;\
        }\
        \
        \
        \
        \
        \
        \
        \
        \
        \
        \
        \
        \
        \
        #mvpd-picker-faq {\
          height: 100%;\
          display: none;\
        \
        }\
        \
        #mvpd-picker-faq-top {\
          width: 100%;\
          height: 70px;\
          color: #5b544f;\
          font-family: 'Open Sans Semibold', 'OpenSans-Semibold', 'Open Sans';\
          font-weight: bold;\
          font-size: 20px;\
          display: inline-block;\
          line-height: 54px;\
        }\
        \
        #mvpd-picker-faq-main {\
          width: 100%;\
          height: 295px;\
          overflow-y: scroll;\
          overflow-x: hidden;\
          font-family: 'Open Sans';\
        }\
        \
        #mvpd-picker-faq-main .pane p {\
          font-weight: 200;\
          font-family: 'Open Sans Light', 'OpenSans-Light', 'Open Sans';\
          font-size: 18px;\
          margin-top: 8px !important;\
          margin-bottom: 16px !important;\
          color: #847f7c;\
          width: 95%;\
          line-height: 21px;\
        }\
        \
        #mvpd-picker-faq-main h4 {\
          font-weight: 600;\
          font-family: 'Open Sans Semibold', 'OpenSans-Semibold', 'Open Sans';\
          font-size: 20px;\
          margin: 0;\
          color: #5b544f;\
          width: 95%\
        }\
        \
        #mvpd-picker-faq-bottom {\
          width: calc(100% - 20px);\
          height: 70px;\
          position: relative;\
        }\
        \
        #mvpd-picker-faq-bottom-back {\
          position: absolute;\
          right: 0px;\
          top: 20px;\
          width: 70px;\
          height: 32px;\
          border: 0 none transparent;\
          background-color: #fff;\
          color: #5b544f;\
          font-size: 18px;\
          font-weight: bold;\
          font-family: 'Open Sans';\
          cursor: pointer;\
          border-radius: 10px;\
          -webkit-box-shadow: 0px 4px 1px 0px rgba(80, 80, 80, 0.45);\
          -moz-box-shadow:    0px 4px 1px 0px rgba(80, 80, 80, 0.45);\
          box-shadow:         0px 4px 1px 0px rgba(80, 80, 80, 0.45);\
        }\
        \
        \
        #mvpd-picker-faq-bottom-back:hover {\
        \
          -webkit-box-shadow: 0px 2px 1px 0px rgba(50, 50, 50, 0.75);\
          -moz-box-shadow:    0px 2px 1px 0px rgba(50, 50, 50, 0.75);\
          box-shadow:         0px 2px 1px 0px rgba(50, 50, 50, 0.75);\
          margin-top: 2px;\
        }\
        \
        \
        #mvpd-picker-tier2 {\
          width: calc(100% - 20px);\
          display: none;\
        \
        \
        }\
        \
        \
        #mvpd-picker-tier2-top {\
          width: 100%;\
          height: 70px;\
          position: relative;\
        \
        }\
        \
        \
        #mvpd-picker-tier2-top-search {\
          width: 100%;\
          height: 37px;\
          margin-top: 12px;\
          background-color: #f7f7f7;\
          background-image: url( http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/assets/search-spyglass.png );\
          background-repeat: no-repeat;\
          background-position: 0 0;\
        }\
        \
        \
        #mvpd-picker-tier2-top-search-field {\
          display: block;\
          height: 37px;\
          border: 0px none;\
          width: calc(100% - 37px);\
          margin-left: 37px;\
          color: #b4b4b4;\
          font-family: 'Open Sans';\
          font-size: 20px;\
          background-color: #f7f7f7;\
          padding: 0;\
          border-radius: 0;\
          box-shadow: 0 0 0;\
        }\
        \
        #mvpd-picker-tier2-top-matches {\
          position: absolute;\
          height: 14px;\
          top: 45px;\
          left: 26px;\
          color: #a00;\
          font-size: 14px;\
          font-family: 'Open Sans', Sans, arial;\
          font-weight: 200;\
        \
        }\
        \
        \
        \
        #mvpd-picker-tier2-list {\
          width: 100%;\
          height: 295px;\
          overflow-y: auto;\
          overflow-x: hidden;\
        }\
        \
        \
        #mvpd-picker-tier2-bottom {\
          width: 100%;\
          height: 70px;\
          position: relative;\
        }\
        \
        #mvpd-picker-tier2-bottom-back {\
          position: absolute;\
          right: 0px;\
          top: 11px;\
          width: 70px;\
          height: 32px;\
          border: 0px none;\
          background-color: #fff;\
          color: #5b544f;\
          font-size: 18px;\
          font-weight: bold;\
          font-family: 'Open Sans';\
          cursor: pointer;\
          border-radius: 10px;\
          -webkit-box-shadow: 0px 4px 1px 0px rgba(50, 50, 50, 0.45);\
          -moz-box-shadow:    0px 4px 1px 0px rgba(50, 50, 50, 0.45);\
          box-shadow:         0px 4px 1px 0px rgba(50, 50, 50, 0.45);\
        }\
        \
        \
        #mvpd-picker-tier2-bottom-back:hover {\
        \
          -webkit-box-shadow: 0px 2px 1px 0px rgba(50, 50, 50, 0.75);\
          -moz-box-shadow:    0px 2px 1px 0px rgba(50, 50, 50, 0.75);\
          box-shadow:         0px 2px 1px 0px rgba(50, 50, 50, 0.75);\
          margin-top: 2px;\
        }\
        \
        \
        \
        #mvpd-picker-tier2-list .mvpd-picker-tier2-entry {\
          width: calc(100% - 50px);\
          height: 34px;\
          font-family: 'Open Sans Semibold', 'OpenSans-Semibold', 'Open Sans';\
          font-weight: 400;\
          font-size: 20px;\
          color: #666666;\
          font-style: normal;\
          padding-left: 27px;\
          cursor: pointer;\
        \
        }\
        \
        \
        \
        #mvpd-picker-tier2-list .mvpd-picker-tier2-entry:hover {\
          background-color: " + accent_color + ";\
          color: #fff;\
        }\
        \
        \
        \
        \
        \
        \
        \
        \
        \
        \
        \
        /* Tier 1 */ \
        \
        #mvpd-picker-tier1 {\
          width: 100%;\
          display: block;\
        \
        \
        }\
        \
        \
        #mvpd-picker-tier1-top {\
          width: 100%;\
          height: 49px;\
          color: #5b544f;\
          font-family: 'Open Sans Semibold', 'OpenSans-Semibold', 'Open Sans';\
          font-weight: 600;\
          font-size: 20px;\
          display: inline-block;\
          line-height: 44px;\
        \
        }\
        \
        \
        \
        \
        \
        \
        #mvpd-picker-tier1-list {\
          width: 100%;\
          height: 350px;\
          overflow-y: auto;\
          overflow-x: visible;\
          clear: both;\
          margin-left: -10px;\
        }\
        \
        \
        #mvpd-picker-tier1-bottom {\
          width: calc(100% - 32px);\
          height: 53px;\
          position: relative;\
        }\
        \
        #mvpd-picker-tier1-bottom-more {\
          position: absolute;\
          right: 0px;\
          top: 6px;\
          width: 206px;\
          height: 32px;\
          border: 0px none;\
          background-color: #fff;\
          color: #5b544f;\
          font-size: 18px;\
          font-weight: bold;\
          font-family: 'Open Sans';\
          cursor: pointer;\
          border-radius: 10px;\
          -webkit-box-shadow: 0px 4px 1px 0px rgba(50, 50, 50, 0.45);\
          -moz-box-shadow:    0px 4px 1px 0px rgba(50, 50, 50, 0.45);\
          box-shadow:         0px 4px 1px 0px rgba(50, 50, 50, 0.45);\
        }\
        \
        \
        \
        #mvpd-picker-tier1-bottom-more:hover {\
        \
          -webkit-box-shadow: 0px 2px 1px 0px rgba(50, 50, 50, 0.75);\
          -moz-box-shadow:    0px 2px 1px 0px rgba(50, 50, 50, 0.75);\
          box-shadow:         0px 2px 1px 0px rgba(50, 50, 50, 0.75);\
          margin-top: 2px;\
        }\
        \
        \
        \
        #mvpd-picker-tier1-list .mvpd-picker-tier1-entry {\
          width: 233px;\
          height: 89px;\
          margin-left: 10px;\
          margin-bottom: 25px;\
          /*float: left;*/\
          background-repeat: no-repeat;\
          background-position:  50% 50%;\
          /*background-size: 80%;*/\
          border-radius: 10px;\
          background-color: #fff;\
          -webkit-box-shadow: 0px 7px 1px 0px rgba(119, 119, 119, 0.4);\
          -moz-box-shadow:    0px 7px 1px 0px rgba(119, 119, 119, 0.4);\
          box-shadow:         0px 7px 1px 0px rgba(119, 119, 119, 0.4);\
          cursor: pointer;\
          display: inline-block;\
        }\
        \
        \
        \
        #mvpd-picker-tier1-list .mvpd-picker-tier1-entry:hover {\
          -webkit-box-shadow: 0px 5px 1px 0px rgba(119, 119, 119, 0.7);\
          -moz-box-shadow:    0px 5px 1px 0px rgba(119, 119, 119, 0.7);\
          box-shadow:         0px 5px 1px 0px rgba(119, 119, 119, 0.7);\
          margin-bottom:  23px;\
          margin-top: 2px;\
        }\
        \
        \
        \
        \
        \
        #mvpdframe {\
          border: 2px solid #000;\
          box-shadow: 0 6px 11px 2px rgba(0, 0, 0, 1);\
          border-radius: 0 12px 12px;\
          background-color: #3a3a3a;\
          background-image: url( http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/assets/mvpd-iframe-loading.gif );\
          background-position: 50% 50%;\
          background-repeat: no-repeat;\
        }\
        \
        #mvpdframeClose {\
            color: #656565;\
            background-color: #bfbfbf;\
            border: 2px solid #000;\
            font-family: Abel, Arial;\
            height: 19px;\
            margin-bottom: -2px;\
            text-align: center;\
            width: 83px;\
            cursor: pointer;\
            border-radius: 9px 9px 0 0;\
            text-transform: lowercase;\
            padding-bottom: 3px;\
            padding-top: 2px;\
            font-size: 17px;\
            line-height: 21px;\
            box-shadow: 0 2px 10px 1px rgba(0, 0, 0, 1);\
        }\
        \
        #mvpdframeClose:before {\
          margin-top: 2px;\
          content: url( http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/assets/mvpd-iframe-close.png ) \" \";\
        }\
        \
        \
        \
        #mvpd-picker-deeplink-prompt {\
            display:block;\
            margin: 182px auto;\
            position: relative;\
            height: 351px;\
            background-color: #383838;\
            width: 437px;\
            text-align: center;\
            box-shadow: 0 0 28px 12px rgba(0, 0, 0, 1);\
            z-index: 100001;\
        }\
        \
        #mvpd-picker-deeplink-prompt-close {\
            width: 29px;\
            height: 29px;\
            /* background-color: orange; */\
            position: absolute;\
            top: -12px;\
            right: -12px;\
            cursor: pointer;\
            background-image: url( http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/assets/deeplink-prompt-close.png );\
        }\
        \
        \
        #mvpd-picker-deeplink-prompt-message {\
            width: 352px;\
            margin: auto;\
            background-color: none;\
            color: #fff;\
            font-family: Open Sans;\
            font-size: 22px;\
            font-weight: 400;\
            height: 95px;\
            line-height: 28px;\
            position: relative;\
            top: 87px;\
            color: #fff;\
        }\
        \
        #mvpd-picker-deeplink-prompt-button {\
            color: #fff;\
            position: relative;\
            top: 107px;\
            background-color: #e04147;\
            height: 58px;\
            line-height: 56px;\
            text-transform: uppercase;\
            width: 218px;\
            margin: auto;\
            font-family: Arial;\
            font-size: 21px;\
            font-weight: 600;\
            cursor: pointer;\
            border-radius: 6px;\
            display: block;\
            text-decoration: none;\
            box-shadow: 0 0 20px 6px rgba(0, 0, 0, 0.3);\
        }\
        \
        \
        \
        #mvpd-flow-auth-cancelled-overlay {\
          background-color: #000;\
          width: 100%;\
          height: 100%;\
          position: relative;\
        }\
        \
        #mvpd-flow-auth-cancelled-overlay-container {\
          margin: auto;\
          position: absolute;\
          top: 0;\
          left: 0;\
          bottom: 0;\
          right: 0;\
          background-color: #000;\
          height: 75%;\
          background-image: url( http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/assets/player-overlay-key.png );\
          background-repeat: no-repeat;\
          background-position: 50% 0;\
        }\
        \
        #mvpd-flow-auth-cancelled-overlay-message {\
          margin: auto;\
          margin-top: 74px;\
          width: 48%;\
          text-align: center;\
          font-size: 2vw;\
          font-weight: normal;\
          font-family: 'Roboto Condensed', sans-serif;\
          color: #fff;\
          height: auto;\
        }\
        \
        #mvpd-flow-auth-cancelled-overlay-button {\
          margin: auto;\
          margin-top: 21px;\
          width: 190px;\
          height: 45px;\
          text-align: center;\
          font-size: 22px;\
          font-weight: normal;\
          font-family: 'Roboto Condensed', sans-serif;\
          color: #fff;\
          background-color: #007cbf;\
          cursor: pointer;\
          text-transform: uppercase;\
          border-radius: 4px;\
          line-height: 42px;\
        }\
        \
        #mvpddiv {\
            z-index: 100002 !important;\
        }\
        \
        #mvpd-picker-deeplink-overlay {\
            width: 100%;\
            height: 100%;\
            background-color: #383838;\
            text-align: center;\
        }\
        \
        #mvpd-picker-deeplink-overlay-close {\
            \
        }\
        \
        #mvpd-picker-deeplink-overlay-message {\
            width: 75%;\
            margin: auto;\
            top: 18%;\
            position: relative;\
            height: auto;\
            font-size: 2.4vw;\
            font-family: Open Sans;\
        }\
        \
        #mvpd-picker-deeplink-overlay-button {\
            display: block;\
            position: relative;\
            margin: auto;\
            height: 16%;\
            width: 61%;\
            background-color: #e04147;\
            top: 26%;\
            font-size: 3vw;\
            line-height: 4.5vw;\
            font-family: Open Sans;\
            color: #fff;\
            text-transform: uppercase;\
            font-family: Arial;\
            font-weight: 600;\
            cursor: pointer;\
            border-radius: 6px;\
            text-decoration: none;\
            box-shadow: 0 0 20px 6px rgba(0, 0, 0, 0.3);\
        }\
        \
        \
        #mvpd-picker-flash-support-prompt-wrapper {\
          width: 787px;\
          height: 300px;\
          padding: 0;\
          position: absolute;\
          margin: auto;\
          left: 0;\
          right: 0;\
          top: 0;\
          bottom: 0;\
        }\
        \
        #mvpd-picker-flash-support-prompt {\
          width: 787px;\
          height: auto;\
          background-color: #ffeecc;\
          font-family: \"Roboto Condensed\", RobotoCondensed;\
          padding: 0;\
          position: relative;\
          overflow: auto;\
          -webkit-box-shadow: 0px 0px 17px 0px rgba(0, 0, 0, 1);\
          -moz-box-shadow:    0px 0px 17px 0px rgba(0, 0, 0, 1);\
          box-shadow:         0px 0px 17px 0px rgba(0, 0, 0, 1);\
        }\
        \
        #mvpd-picker-flash-support-prompt-close {\
          background-image: url( http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/assets/close-flash-prompt.png );\
          width: 62px;\
          height: 63px;\
          position: absolute;\
          right: -30px;\
          top: -30px;\
          cursor: pointer;\
        }\
        \
        #mvpd-picker-flash-support-prompt-header {\
          position: relative;\
          color: #0067b1;\
          font-size: 27px;\
          font-weight: bold;\
          margin-top: 32px;\
          margin-left: 32px;\
          width: 723px;\
          height: 30px;\
        \
        \
        }\
        \
        #mvpd-picker-flash-support-prompt-message {\
          position: relative;\
          font-family: 'Roboto', sans-serif;\
          color: #323232;\
          font-size: 21px;\
          font-weight: normal;\
          top: 15px;\
          margin-left: 32px;\
          width: 723px;\
        }\
        \
        \
        #mvpd-picker-flash-support-prompt-button {\
          display: inline-block;\
          height: 39px;\
          width: 158px;\
          margin-top: 32px;\
          margin-left: 32px;\
          margin-bottom: 20px;\
          background-image: url(http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/assets/get-flash-2.png);\
          background-repeat: no-repeat;\
          float: left;\
        \
        }\
        \
        #mvpd-picker-flash-support-prompt-button span {\
          display: none;\
        }\
        \
        #mvpd-picker-flash-support-prompt-help {\
          display: inline-block;\
          height: 39px;\
          width: 158px;\
          margin-top: 32px;\
          margin-left: 32px;\
          margin-bottom: 20px;\
          background-color: #0067b1;\
          color: #fff;\
          text-align: center;\
          text-decoration: none;\
          font-family: Roboto, sans-serif;\
          font-size: 17px;\
          line-height: 36px;\
        }\
        \
        \
        #mvpd-picker-flash-disabled-prompt-wrapper {\
          width: 787px;\
          height: 300px;\
          padding: 0;\
          position: absolute;\
          margin: auto;\
          left: 0;\
          right: 0;\
          top: 0;\
          bottom: 0;\
        }\
        \
        #mvpd-picker-flash-disabled-prompt {\
          width: 787px;\
          height: auto;\
          background-color: #ffeecc;\
          font-family: \"Roboto Condensed\", RobotoCondensed;\
          padding: 0;\
          position: relative;\
          overflow: auto;\
          -webkit-box-shadow: 0px 0px 17px 0px rgba(0, 0, 0, 1);\
          -moz-box-shadow:    0px 0px 17px 0px rgba(0, 0, 0, 1);\
          box-shadow:         0px 0px 17px 0px rgba(0, 0, 0, 1);\
        }\
        \
        #mvpd-picker-flash-disabled-prompt-close {\
          background-image: url( http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/assets/close-flash-prompt.png );\
          width: 62px;\
          height: 63px;\
          position: absolute;\
          right: -30px;\
          top: -30px;\
          cursor: pointer;\
        }\
        \
        #mvpd-picker-flash-disabled-prompt-header {\
          position: relative;\
          color: #0067b1;\
          font-size: 27px;\
          font-weight: bold;\
          margin-top: 32px;\
          margin-left: 32px;\
          width: 723px;\
          height: 30px;\
        \
        \
        }\
        \
        #mvpd-picker-flash-disabled-prompt-message {\
          position: relative;\
          font-family: 'Roboto', sans-serif;\
          color: #323232;\
          font-size: 21px;\
          font-weight: normal;\
          top: 15px;\
          margin-left: 32px;\
          width: 723px;\
        }\
        \
        \
        #mvpd-picker-flash-disabled-prompt-button {\
          display: block;\
          height: 39px;\
          width: 158px;\
          margin-top: 32px;\
          margin-left: 32px;\
          margin-bottom: 20px;\
          background-color: #0067b1;\
          color: #fff;\
          text-align: center;\
          text-decoration: none;\
          font-family: Roboto,sans-serif;\
          font-size: 17px;\
          line-height: 36px;\
        \
        }\
        \
        \
        \
        #mvpd-picker-auth-error-wrapper {\
          width: 787px;\
          height: 300px;\
          padding: 0;\
          position: absolute;\
          margin: auto;\
          left: 0;\
          right: 0;\
          top: 0;\
          bottom: 0;\
        }\
        \
        #mvpd-picker-auth-error {\
          width: 787px;\
          height: auto;\
          background-color: #ffeecc;\
          font-family: \"Roboto Condensed\", RobotoCondensed;\
          padding: 0;\
          position: relative;\
          overflow: auto;\
          -webkit-box-shadow: 0px 0px 17px 0px rgba(0, 0, 0, 1);\
          -moz-box-shadow:    0px 0px 17px 0px rgba(0, 0, 0, 1);\
          box-shadow:         0px 0px 17px 0px rgba(0, 0, 0, 1);\
        }\
        \
        #mvpd-picker-auth-error-close {\
          background-image: url( http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/assets/close-flash-prompt.png );\
          width: 62px;\
          height: 63px;\
          position: absolute;\
          right: -30px;\
          top: -30px;\
          cursor: pointer;\
        }\
        \
        #mvpd-picker-auth-error-header {\
          position: relative;\
          color: #AA0000;\
          font-size: 27px;\
          font-weight: bold;\
          margin-top: 32px;\
          margin-left: 32px;\
          width: 723px;\
          height: 30px;\
        }\
        \
        #mvpd-picker-auth-error-message {\
          position: relative;\
          font-family: 'Roboto', sans-serif;\
          color: #323232;\
          font-size: 21px;\
          font-weight: normal;\
          top: 15px;\
          margin-left: 32px;\
          width: 723px;\
        }\
        \
        #mvpd-picker-auth-error-button {\
          display: block;\
          height: 39px;\
          width: 158px;\
          margin-top: 32px;\
          margin-left: 32px;\
          margin-bottom: 20px;\
          background-color: transparent;\
          border: 0 none;\
        }\
        \
        \
        \
        #mvpd-picker-newsletter-form-wrapper {\
          width: 780px;\
          height: " + newsletter_form_h + ";\
          padding: 0;\
          position: absolute;\
          margin: auto;\
          left: 0;\
          right: 0;\
          top: 0;\
          bottom: 0;\
          -webkit-box-shadow: 0px 0px 17px 0px rgba(0, 0, 0, 1);\
          -moz-box-shadow:    0px 0px 17px 0px rgba(0, 0, 0, 1);\
          box-shadow:         0px 0px 17px 0px rgba(0, 0, 0, 1);\
        }\
        \
        #mvpd-picker-newsletter-form {\
          width: 100%;\
          height: 100%;\
          padding: 0;\
          position: relative;\
          overflow: hidden;\
          background-color: #f5f1e2;\
        }\
        \
        #mvpd-picker-newsletter-form-close {\
          background-image: url( http://player.aetndigital.com/videoassetsurl/videoassets/tve-auth/assets/close-flash-prompt.png );\
          width: 62px;\
          height: 63px;\
          position: absolute;\
          right: -30px;\
          top: -30px;\
          cursor: pointer;\
        }\
        \
        #mvpd-picker-newsletter-form-iframe {\
          width: 100%;\
          height: 100%;\
        }\
        \
        \
        \
        \
        \
        \
        \
        \
        \
        \
        @media (max-width: 891px) and (min-width: 638px) {\
            #mvpd-picker {\
              width: 546px;\
            }\
            \
            #mvpd-picker-head-caption {\
                font-size: 17px;\
                font-weight: 600;\
                line-height: 21px;\
                top: 7px;\
            }\
            \
            #mvpd-flow-auth-cancelled-overlay-message {\
                width: 78%;\
                font-size: 2.2vw;\
            }\
            \
            #mvpd-picker-newsletter-form-wrapper {\
                width: 546px;\
                height: 766px;\
            }\
            \
        }\
        \
        @media (max-width: 637px) {\
            #mvpd-picker {\
              width: 301px;\
            }\
            \
            #mvpd-picker-head-caption {\
                font-size: 17px;\
                font-weight: 600;\
                line-height: 21px;\
                top: 7px;\
            }\
            \
            \
            #mvpd-picker-tier2-list .mvpd-picker-tier2-entry {\
                display: block;\
                font-size: 17px;\
                height: auto;\
                line-height: 20px;\
                margin-bottom: 12px;\
                min-height: 34px;\
            }\
            \
            #mvpd-picker-tier2-top-search-field {\
                font-size: 15px;\
            }\
            \
            #mvpd-picker-faq-top {\
                line-height: 29px;\
            }\
            \
            #mvpd-flow-auth-cancelled-overlay-message {\
                width: 78%;\
                font-size: 2.5vw;\
            }\
            \
            #mvpd-picker-newsletter-form-wrapper {\
                width: 310px;\
                height: 1034px;\
            }\
            \
        }\
        \
        @media (max-width: 568px) {\
            \
            #mvpd-picker-deeplink-prompt {\
                width: 85%;\
                height: 44vw;\
                margin: 3% auto;\
            }\
            \
            #mvpd-picker-deeplink-prompt-message {\
                width: 85%;\
                top: 3%;\
                font-size: 5vw;\
                line-height: 7vw;\
                height: auto;\
                margin-bottom: 11%;\
            }\
            \
            #mvpd-picker-deeplink-prompt-button {\
                top: 0;\
                height: 40px;\
                line-height: 40px;\
                width: 174px;\
                font-size: 16px;\
            }\
        }\
        \
        @media (max-width: 320px) {\
            \
            #mvpd-picker-deeplink-prompt {\
                margin: 50% auto;\
            }\
        }";

    };


    return tve;

}();


// jQuery(document).ready(function($) {

// 	aetn.adobePass.page.init('AETV');
// });
;

// Global Killswitch
if (Drupal.jsEnabled) {
$(document).ready(function() {
    $("body").append($("#memcache-devel"));
  });
}
;
// jquery.pjax.js
// copyright chris wanstrath
// https://github.com/defunkt/jquery-pjax

(function($){

// When called on a container with a selector, fetches the href with
// ajax into the container or with the data-pjax attribute on the link
// itself.
//
// Tries to make sure the back button and ctrl+click work the way
// you'd expect.
//
// Exported as $.fn.pjax
//
// Accepts a jQuery ajax options object that may include these
// pjax specific options:
//
//
// container - Where to stick the response body. Usually a String selector.
//             $(container).html(xhr.responseBody)
//             (default: current jquery context)
//      push - Whether to pushState the URL. Defaults to true (of course).
//   replace - Want to use replaceState instead? That's cool.
//
// For convenience the second parameter can be either the container or
// the options object.
//
// Returns the jQuery object
function fnPjax(selector, container, options) {
  var context = this
  return this.on('click.pjax', selector, function(event) {
    var opts = $.extend({}, optionsFor(container, options))
    if (!opts.container)
      opts.container = $(this).attr('data-pjax') || context
    handleClick(event, opts)
  })
}

// Public: pjax on click handler
//
// Exported as $.pjax.click.
//
// event   - "click" jQuery.Event
// options - pjax options
//
// Examples
//
//   $(document).on('click', 'a', $.pjax.click)
//   // is the same as
//   $(document).pjax('a')
//
//  $(document).on('click', 'a', function(event) {
//    var container = $(this).closest('[data-pjax-container]')
//    $.pjax.click(event, container)
//  })
//
// Returns nothing.
function handleClick(event, container, options) {
  options = optionsFor(container, options)

  var link = event.currentTarget

  if (link.tagName.toUpperCase() !== 'A')
    throw "$.fn.pjax or $.pjax.click requires an anchor element"

  // Middle click, cmd click, and ctrl click should open
  // links in a new tab as normal.
  if ( event.which > 1 || event.metaKey || event.ctrlKey || event.shiftKey || event.altKey )
    return

  // Ignore cross origin links
  if ( location.protocol !== link.protocol || location.hostname !== link.hostname )
    return

  // Ignore anchors on the same page
  if (link.hash && link.href.replace(link.hash, '') ===
       location.href.replace(location.hash, ''))
    return

  // Ignore empty anchor "foo.html#"
  if (link.href === location.href + '#')
    return

  var defaults = {
    url: link.href,
    container: $(link).attr('data-pjax'),
    target: link,
    fragment: null
  }

  var opts = $.extend({}, defaults, options)
  var clickEvent = $.Event('pjax:click')
  $(link).trigger(clickEvent, [opts])

  if (!clickEvent.isDefaultPrevented()) {
    pjax(opts)
    event.preventDefault()
  }
}

// Public: pjax on form submit handler
//
// Exported as $.pjax.submit
//
// event   - "click" jQuery.Event
// options - pjax options
//
// Examples
//
//  $(document).on('submit', 'form', function(event) {
//    var container = $(this).closest('[data-pjax-container]')
//    $.pjax.submit(event, container)
//  })
//
// Returns nothing.
function handleSubmit(event, container, options) {
  options = optionsFor(container, options)

  var form = event.currentTarget

  if (form.tagName.toUpperCase() !== 'FORM')
    throw "$.pjax.submit requires a form element"

  var defaults = {
    type: form.method.toUpperCase(),
    url: form.action,
    data: $(form).serializeArray(),
    container: $(form).attr('data-pjax'),
    target: form,
    fragment: null
  }

  pjax($.extend({}, defaults, options))

  event.preventDefault()
}

// Loads a URL with ajax, puts the response body inside a container,
// then pushState()'s the loaded URL.
//
// Works just like $.ajax in that it accepts a jQuery ajax
// settings object (with keys like url, type, data, etc).
//
// Accepts these extra keys:
//
// container - Where to stick the response body.
//             $(container).html(xhr.responseBody)
//      push - Whether to pushState the URL. Defaults to true (of course).
//   replace - Want to use replaceState instead? That's cool.
//
// Use it just like $.ajax:
//
//   var xhr = $.pjax({ url: this.href, container: '#main' })
//   console.log( xhr.readyState )
//
// Returns whatever $.ajax returns.
function pjax(options) {
  options = $.extend(true, {}, $.ajaxSettings, pjax.defaults, options)

  if ($.isFunction(options.url)) {
    options.url = options.url()
  }

  var target = options.target

  var hash = parseURL(options.url).hash

  var context = options.context = findContainerFor(options.container)

  // We want the browser to maintain two separate internal caches: one
  // for pjax'd partial page loads and one for normal page loads.
  // Without adding this secret parameter, some browsers will often
  // confuse the two.
  if (!options.data) options.data = {}
  options.data._pjax = context.selector

  function fire(type, args) {
    var event = $.Event(type, { relatedTarget: target })
    context.trigger(event, args)
    return !event.isDefaultPrevented()
  }

  var timeoutTimer

  options.beforeSend = function(xhr, settings) {
    // No timeout for non-GET requests
    // Its not safe to request the resource again with a fallback method.
    if (settings.type !== 'GET') {
      settings.timeout = 0
    }

    xhr.setRequestHeader('X-PJAX', 'true')
    xhr.setRequestHeader('X-PJAX-Container', context.selector)

    if (!fire('pjax:beforeSend', [xhr, settings]))
      return false

    if (settings.timeout > 0) {
      timeoutTimer = setTimeout(function() {
        if (fire('pjax:timeout', [xhr, options]))
          xhr.abort('timeout')
      }, settings.timeout)

      // Clear timeout setting so jquerys internal timeout isn't invoked
      settings.timeout = 0
    }

    options.requestUrl = parseURL(settings.url).href
  }

  options.complete = function(xhr, textStatus) {
    if (timeoutTimer)
      clearTimeout(timeoutTimer)

    fire('pjax:complete', [xhr, textStatus, options])

    fire('pjax:end', [xhr, options])
  }

  options.error = function(xhr, textStatus, errorThrown) {
    var container = extractContainer("", xhr, options)

    var allowed = fire('pjax:error', [xhr, textStatus, errorThrown, options])
    if (options.type == 'GET' && textStatus !== 'abort' && allowed) {
      locationReplace(container.url)
    }
  }

  options.success = function(data, status, xhr) {
    // If $.pjax.defaults.version is a function, invoke it first.
    // Otherwise it can be a static string.
    var currentVersion = (typeof $.pjax.defaults.version === 'function') ?
      $.pjax.defaults.version() :
      $.pjax.defaults.version

    var latestVersion = xhr.getResponseHeader('X-PJAX-Version')

    var container = extractContainer(data, xhr, options)

    // If there is a layout version mismatch, hard load the new url
    if (currentVersion && latestVersion && currentVersion !== latestVersion) {
      locationReplace(container.url)
      return
    }

    // If the new response is missing a body, hard load the page
    if (!container.contents) {
      locationReplace(container.url)
      return
    }

    pjax.state = {
      id: options.id || uniqueId(),
      url: container.url,
      title: container.title,
      container: context.selector,
      fragment: options.fragment,
      timeout: options.timeout
    }

    if (options.push || options.replace) {
      window.history.replaceState(pjax.state, container.title, container.url)
    }

    if (container.title) document.title = container.title
    context.html(container.contents)
    executeScriptTags(container.scripts)

    // Scroll to top by default
    if (typeof options.scrollTo === 'number')
      $(window).scrollTop(options.scrollTo)

    // If the URL has a hash in it, make sure the browser
    // knows to navigate to the hash.
    if ( hash !== '' ) {
      // Avoid using simple hash set here. Will add another history
      // entry. Replace the url with replaceState and scroll to target
      // by hand.
      //
      //   window.location.hash = hash
      var url = parseURL(container.url)
      url.hash = hash

      pjax.state.url = url.href
      window.history.replaceState(pjax.state, container.title, url.href)

      var target = $(url.hash)
      if (target.length) $(window).scrollTop(target.offset().top)
    }

    fire('pjax:success', [data, status, xhr, options])
  }


  // Initialize pjax.state for the initial page load. Assume we're
  // using the container and options of the link we're loading for the
  // back button to the initial page. This ensures good back button
  // behavior.
  if (!pjax.state) {
    pjax.state = {
      id: uniqueId(),
      url: window.location.href,
      title: document.title,
      container: context.selector,
      fragment: options.fragment,
      timeout: options.timeout
    }
    window.history.replaceState(pjax.state, document.title)
  }

  // Cancel the current request if we're already pjaxing
  var xhr = pjax.xhr
  if ( xhr && xhr.readyState < 4) {
    xhr.onreadystatechange = $.noop
    xhr.abort()
  }

  pjax.options = options
  var xhr = pjax.xhr = $.ajax(options)

  if (xhr.readyState > 0) {
    if (options.push && !options.replace) {
      // Cache current container element before replacing it
      cachePush(pjax.state.id, context.clone().contents())

      window.history.pushState(null, "", stripPjaxParam(options.requestUrl))
    }

    fire('pjax:start', [xhr, options])
    fire('pjax:send', [xhr, options])
  }

  return pjax.xhr
}

// Public: Reload current page with pjax.
//
// Returns whatever $.pjax returns.
function pjaxReload(container, options) {
  var defaults = {
    url: window.location.href,
    push: false,
    replace: true,
    scrollTo: false
  }

  return pjax($.extend(defaults, optionsFor(container, options)))
}

// Internal: Hard replace current state with url.
//
// Work for around WebKit
//   https://bugs.webkit.org/show_bug.cgi?id=93506
//
// Returns nothing.
function locationReplace(url) {
  window.history.replaceState(null, "", "#")
  window.location.replace(url)
}


var initialPop = true
var initialURL = window.location.href
var initialState = window.history.state

// Initialize $.pjax.state if possible
// Happens when reloading a page and coming forward from a different
// session history.
if (initialState && initialState.container) {
  pjax.state = initialState
}

// Non-webkit browsers don't fire an initial popstate event
if ('state' in window.history) {
  initialPop = false
}

// popstate handler takes care of the back and forward buttons
//
// You probably shouldn't use pjax on pages with other pushState
// stuff yet.
function onPjaxPopstate(event) {
  var state = event.state

  if (state && state.container) {
    // When coming forward from a seperate history session, will get an
    // initial pop with a state we are already at. Skip reloading the current
    // page.
    if (initialPop && initialURL == state.url) return

    var container = $(state.container)
    if (container.length) {
      var direction, contents = cacheMapping[state.id]

      if (pjax.state) {
        // Since state ids always increase, we can deduce the history
        // direction from the previous state.
        direction = pjax.state.id < state.id ? 'forward' : 'back'

        // Cache current container before replacement and inform the
        // cache which direction the history shifted.
        cachePop(direction, pjax.state.id, container.clone().contents())
      }

      var popstateEvent = $.Event('pjax:popstate', {
        state: state,
        direction: direction
      })
      container.trigger(popstateEvent)

      var options = {
        id: state.id,
        url: state.url,
        container: container,
        push: false,
        fragment: state.fragment,
        timeout: state.timeout,
        scrollTo: false
      }

      if (contents) {
        container.trigger('pjax:start', [null, options])

        if (state.title) document.title = state.title
        container.html(contents)
        pjax.state = state

        container.trigger('pjax:end', [null, options])
      } else {
        pjax(options)
      }

      // Force reflow/relayout before the browser tries to restore the
      // scroll position.
      container[0].offsetHeight
    } else {
      locationReplace(location.href)
    }
  }
  initialPop = false
}

// Fallback version of main pjax function for browsers that don't
// support pushState.
//
// Returns nothing since it retriggers a hard form submission.
function fallbackPjax(options) {
  var url = $.isFunction(options.url) ? options.url() : options.url,
      method = options.type ? options.type.toUpperCase() : 'GET'

  var form = $('<form>', {
    method: method === 'GET' ? 'GET' : 'POST',
    action: url,
    style: 'display:none'
  })

  if (method !== 'GET' && method !== 'POST') {
    form.append($('<input>', {
      type: 'hidden',
      name: '_method',
      value: method.toLowerCase()
    }))
  }

  var data = options.data
  if (typeof data === 'string') {
    $.each(data.split('&'), function(index, value) {
      var pair = value.split('=')
      form.append($('<input>', {type: 'hidden', name: pair[0], value: pair[1]}))
    })
  } else if (typeof data === 'object') {
    for (key in data)
      form.append($('<input>', {type: 'hidden', name: key, value: data[key]}))
  }

  $(document.body).append(form)
  form.submit()
}

// Internal: Generate unique id for state object.
//
// Use a timestamp instead of a counter since ids should still be
// unique across page loads.
//
// Returns Number.
function uniqueId() {
  return (new Date).getTime()
}

// Internal: Strips _pjax param from url
//
// url - String
//
// Returns String.
function stripPjaxParam(url) {
  return url
    .replace(/\?_pjax=[^&]+&?/, '?')
    .replace(/_pjax=[^&]+&?/, '')
    .replace(/[\?&]$/, '')
}

// Internal: Parse URL components and returns a Locationish object.
//
// url - String URL
//
// Returns HTMLAnchorElement that acts like Location.
function parseURL(url) {
  var a = document.createElement('a')
  a.href = url
  return a
}

// Internal: Build options Object for arguments.
//
// For convenience the first parameter can be either the container or
// the options object.
//
// Examples
//
//   optionsFor('#container')
//   // => {container: '#container'}
//
//   optionsFor('#container', {push: true})
//   // => {container: '#container', push: true}
//
//   optionsFor({container: '#container', push: true})
//   // => {container: '#container', push: true}
//
// Returns options Object.
function optionsFor(container, options) {
  // Both container and options
  if ( container && options )
    options.container = container

  // First argument is options Object
  else if ( $.isPlainObject(container) )
    options = container

  // Only container
  else
    options = {container: container}

  // Find and validate container
  if (options.container)
    options.container = findContainerFor(options.container)

  return options
}

// Internal: Find container element for a variety of inputs.
//
// Because we can't persist elements using the history API, we must be
// able to find a String selector that will consistently find the Element.
//
// container - A selector String, jQuery object, or DOM Element.
//
// Returns a jQuery object whose context is `document` and has a selector.
function findContainerFor(container) {
  container = $(container)

  if ( !container.length ) {
    throw "no pjax container for " + container.selector
  } else if ( container.selector !== '' && container.context === document ) {
    return container
  } else if ( container.attr('id') ) {
    return $('#' + container.attr('id'))
  } else {
    throw "cant get selector for pjax container!"
  }
}

// Internal: Filter and find all elements matching the selector.
//
// Where $.fn.find only matches descendants, findAll will test all the
// top level elements in the jQuery object as well.
//
// elems    - jQuery object of Elements
// selector - String selector to match
//
// Returns a jQuery object.
function findAll(elems, selector) {
  return elems.filter(selector).add(elems.find(selector));
}

function parseHTML(html) {
  return $.parseHTML(html, document, true)
}

// Internal: Extracts container and metadata from response.
//
// 1. Extracts X-PJAX-URL header if set
// 2. Extracts inline <title> tags
// 3. Builds response Element and extracts fragment if set
//
// data    - String response data
// xhr     - XHR response
// options - pjax options Object
//
// Returns an Object with url, title, and contents keys.
function extractContainer(data, xhr, options) {
  var obj = {}

  // Prefer X-PJAX-URL header if it was set, otherwise fallback to
  // using the original requested url.
  obj.url = stripPjaxParam(xhr.getResponseHeader('X-PJAX-URL') || options.requestUrl)

  // Attempt to parse response html into elements
  if (/<html/i.test(data)) {
    var $head = $(parseHTML(data.match(/<head[^>]*>([\s\S.]*)<\/head>/i)[0]))
    var $body = $(parseHTML(data.match(/<body[^>]*>([\s\S.]*)<\/body>/i)[0]))
  } else {
    var $head = $body = $(parseHTML(data))
  }

  // If response data is empty, return fast
  if ($body.length === 0)
    return obj

  // If there's a <title> tag in the header, use it as
  // the page's title.
  obj.title = findAll($head, 'title').last().text()

  if (options.fragment) {
    // If they specified a fragment, look for it in the response
    // and pull it out.
    if (options.fragment === 'body') {
      var $fragment = $body
    } else {
      var $fragment = findAll($body, options.fragment).first()
    }

    if ($fragment.length) {
      obj.contents = $fragment.contents()

      // If there's no title, look for data-title and title attributes
      // on the fragment
      if (!obj.title)
        obj.title = $fragment.attr('title') || $fragment.data('title')
    }

  } else if (!/<html/i.test(data)) {
    obj.contents = $body
  }

  // Clean up any <title> tags
  if (obj.contents) {
    // Remove any parent title elements
    obj.contents = obj.contents.not(function() { return $(this).is('title') })

    // Then scrub any titles from their descendents
    obj.contents.find('title').remove()

    // Gather all script[src] elements
    obj.scripts = findAll(obj.contents, 'script[src]').remove()
    obj.contents = obj.contents.not(obj.scripts)
  }

  // Trim any whitespace off the title
  if (obj.title) obj.title = $.trim(obj.title)

  return obj
}

// Load an execute scripts using standard script request.
//
// Avoids jQuery's traditional $.getScript which does a XHR request and
// globalEval.
//
// scripts - jQuery object of script Elements
//
// Returns nothing.
function executeScriptTags(scripts) {
  if (!scripts) return

  var existingScripts = $('script[src]')

  scripts.each(function() {
    var src = this.src
    var matchedScripts = existingScripts.filter(function() {
      return this.src === src
    })
    if (matchedScripts.length) return

    var script = document.createElement('script')
    script.type = $(this).attr('type')
    script.src = $(this).attr('src')
    document.head.appendChild(script)
  })
}

// Internal: History DOM caching class.
var cacheMapping      = {}
var cacheForwardStack = []
var cacheBackStack    = []

// Push previous state id and container contents into the history
// cache. Should be called in conjunction with `pushState` to save the
// previous container contents.
//
// id    - State ID Number
// value - DOM Element to cache
//
// Returns nothing.
function cachePush(id, value) {
  cacheMapping[id] = value
  cacheBackStack.push(id)

  // Remove all entires in forward history stack after pushing
  // a new page.
  while (cacheForwardStack.length)
    delete cacheMapping[cacheForwardStack.shift()]

  // Trim back history stack to max cache length.
  while (cacheBackStack.length > pjax.defaults.maxCacheLength)
    delete cacheMapping[cacheBackStack.shift()]
}

// Shifts cache from directional history cache. Should be
// called on `popstate` with the previous state id and container
// contents.
//
// direction - "forward" or "back" String
// id        - State ID Number
// value     - DOM Element to cache
//
// Returns nothing.
function cachePop(direction, id, value) {
  var pushStack, popStack
  cacheMapping[id] = value

  if (direction === 'forward') {
    pushStack = cacheBackStack
    popStack  = cacheForwardStack
  } else {
    pushStack = cacheForwardStack
    popStack  = cacheBackStack
  }

  pushStack.push(id)
  if (id = popStack.pop())
    delete cacheMapping[id]
}

// Public: Find version identifier for the initial page load.
//
// Returns String version or undefined.
function findVersion() {
  return $('meta').filter(function() {
    var name = $(this).attr('http-equiv')
    return name && name.toUpperCase() === 'X-PJAX-VERSION'
  }).attr('content')
}

// Install pjax functions on $.pjax to enable pushState behavior.
//
// Does nothing if already enabled.
//
// Examples
//
//     $.pjax.enable()
//
// Returns nothing.
function enable() {
  $.fn.pjax = fnPjax
  $.pjax = pjax
  $.pjax.enable = $.noop
  $.pjax.disable = disable
  $.pjax.click = handleClick
  $.pjax.submit = handleSubmit
  $.pjax.reload = pjaxReload
  $.pjax.defaults = {
    timeout: 650,
    push: true,
    replace: false,
    type: 'GET',
    dataType: 'html',
    scrollTo: 0,
    maxCacheLength: 20,
    version: findVersion
  }
  $(window).on('popstate.pjax', onPjaxPopstate)
}

// Disable pushState behavior.
//
// This is the case when a browser doesn't support pushState. It is
// sometimes useful to disable pushState for debugging on a modern
// browser.
//
// Examples
//
//     $.pjax.disable()
//
// Returns nothing.
function disable() {
  $.fn.pjax = function() { return this }
  $.pjax = fallbackPjax
  $.pjax.enable = enable
  $.pjax.disable = $.noop
  $.pjax.click = $.noop
  $.pjax.submit = $.noop
  $.pjax.reload = function() { window.location.reload() }

  $(window).off('popstate.pjax', onPjaxPopstate)
}


// Add the state property to jQuery's event object so we can use it in
// $(window).bind('popstate')
if ( $.inArray('state', $.event.props) < 0 )
  $.event.props.push('state')

 // Is pjax supported by this browser?
$.support.pjax =
  window.history && window.history.pushState && window.history.replaceState &&
  // pushState isn't reliable on iOS until 5.
  !navigator.userAgent.match(/((iPod|iPhone|iPad).+\bOS\s+[1-4]|WebApps\/.+CFNetwork)/)

$.support.pjax ? enable() : disable()

})(jQuery);
;
(function($) {

/**
 * Handles click events on selected links and loads the corresponding pages 
 * through pjax.
 */
Drupal.behaviors.pjax = {
  attach: function(context) {

    // The defaults attribute is only available in browsers that support pjax.
    if (!$.pjax.defaults) {
      return;
    }

    // Set defaults.
    $.pjax.defaults.scrollTo = Drupal.settings.pjax.scrollto !== "" ? parseInt(Drupal.settings.pjax.scrollto) : false;
    $.pjax.defaults.timeout = Drupal.settings.pjax.timeout;

    // Only proceed if a content selector is defined.
    if (Drupal.settings.pjax.contentSelector) {
      // Run the behavior just once on the content selector.
      $(Drupal.settings.pjax.contentSelector, context).once("pjax", function() {
        var $content = $(this);
        var $pjaxLinks;

        // Enable pjax for selected links.
        // pjax() uses live(), so links that are removed and re-added will still work.
        if (Drupal.settings.pjax.linksSelector) {
          $pjaxLinks = $(Drupal.settings.pjax.linksSelector);
          $pjaxLinks.pjax(Drupal.settings.pjax.contentSelector)
            // Add 'loading' class.
            .live('click', function(e) {
              $(this).addClass("pjax-link-loading");
            });
        }

        // Catch pjax start/end events.
        $content
          .bind('pjax:start', function() {
            // Detach JS behaviors when loading new content.
            Drupal.detachBehaviors($content, Drupal.settings);

            // Add 'loading' class.
            $content.addClass("pjax-loading");

            // Remove 'active' class from links.
            if ($pjaxLinks) {
              $pjaxLinks.removeClass("active");            
            }
          })
          .bind('pjax:end', function() {
            // Re-attach JS behaviors when content has loaded.
            Drupal.attachBehaviors($content, Drupal.settings);

            // Remove 'loading' class.
            $content.removeClass("pjax-loading");

            // Update classes on the clicked link.
            if ($pjaxLinks) {
              $pjaxLinks.filter(".pjax-link-loading")
                .removeClass("pjax-link-loading")
                .addClass("active");
            }
          });

      });
    }
  }
}

})(jQuery);
;
(function ($) {

    /**
     * @todo Undocumented Code!
     */
    $.extend({
        getUrlVars: function () {
          var vars = [], hash;
          var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
          for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
          }
          return vars;
        },
        getUrlVar: function (name) {
          return $.getUrlVars()[name];
        }
    });

    Drupal.gigya = Drupal.gigya || {};

    Drupal.gigya.hideLogin = function () {
      $('#user-login').hide();
    }

    /**
     * @todo Undocumented Code!
     */
    Drupal.gigya.logoutResponse = function (response) {
      if (response['status'] == 'OK') {
        document.getElementById('logoutMessage').innerHTML = "Successfully logged out, redirecting";
        window.location = Drupal.settings.gigya.logoutLocation;
      }
    };

    Drupal.gigya.addEmail = function (email) {
      if (typeof Drupal.gigya.toPost !== 'undefined') {
        Drupal.gigya.toPost.user.email = email;
        Drupal.gigya.login(Drupal.gigya.toPost);
      }
    }

    Drupal.gigya.login = function (post) {
      var base = post.context.id;
      var element_settings = {};
      element_settings.url = '/socialize-login';
      element_settings.event = 'gigyaLogin';
      var ajax = new Drupal.ajax(base, $('#' + post.context.id), element_settings);
      ajax.options.data = post;
      $(ajax.element).trigger('gigyaLogin');
    }

    /**
     * @todo Undocumented Code!
     */
    Drupal.gigya.loginCallback = function (response) {
      Drupal.gigya.toPost = response;
      if ((response.user.email.length === 0) && (response.user.isSiteUID !== true)) {
        var html = '<div class="form-item form-type-textfield form-item-email"><div class="description">Additional information is required in order to complete your registeration. Please fill-in the following info:</div><label for="email" style="float: none;">Email <span class="form-required" title="This field is required.">*</span></label><input type="text" id="email" name="email" value="" size="20" maxlength="60" class="form-text required"><button type="button" class="button" onClick="Drupal.gigya.addEmail(jQuery(this).prev().val())">' + Drupal.t('Submit') + '</button></div>';
        Drupal.CTools.Modal.show();
        $('#modal-title').html('Please fill-in missing details');
        $('#modal-content').html(html);
        return false;
      }
      if(response.provider !== 'site' ) {
        Drupal.gigya.login(response);
      }
    };

    /**
     * Callback for the getUserInfo function.
     *
     * Takes the getUserInfo object and renders the HTML to display an array
     * of the user object
     *
     * TODO: probably should be removed in production, since its just for dumping
     * user output.
     */
    Drupal.gigya.getUserInfoCallback = function (response) {
      if (response.status == 'OK') {
        var user = response['user'];
        // Variable which will hold property values.
        var str="<pre>";
        for (prop in user) {
          if (prop == 'birthYear' && user[prop] == 2009) {
            user[prop] = '';
          }
          if (prop == 'identities') {
            for (ident in user[prop]) {
              for (provide in user[prop][ident]) {
                str+=provide + " SUBvalue :"+ user[prop][ident][provide]+"\n";
              }
            }
          }
          // Concate prop and its value from object.
          str+=prop + " value :"+ user[prop]+"\n";
        }
        str+="</pre>";

        document.getElementById('userinfo').innerHTML = str;
      }
    };

    /**
     * @todo Undocumented Code!
     */
    Drupal.gigya.showAddConnectionsUI = function (connectUIParams) {
      gigya.services.socialize.showAddConnectionsUI(connectUIParams);
    };

    /**
     * @todo Undocumented Code!
     */
    Drupal.gigya.notifyLoginCallback = function (response) {
      if (response['status'] == 'OK') {
        setTimeout("$.get(Drupal.settings.basePath + 'socialize-ajax/notify-login')", 1000);
      }
    };


    /**
     * @todo Undocumented Code!
     */
    Drupal.gigya.initResponse = function (response) {
      if (null != response.user) {
        if (response.user.UID != Drupal.settings.gigya.notifyLoginParams.siteUID || !response.user.isLoggedIn) {
          gigya.services.socialize.notifyLogin(Drupal.settings.gigya.notifyLoginParams);
        }
      }
    }
    /**
     * this function checks if the user is looged in at gigya if so it renders the Gamification Plugin
     */
    Drupal.gigya.gmInit = function (response) {
      if (response.errorCode === 0) {
        if (typeof response.UID !== 'undefined') {
          Drupal.gigya.gmRender();
        }
      }
      return false;
    };

    /**
     * function that renders the Gamification Plugin
     */
    Drupal.gigya.gmRender = function () {
      $.each(Drupal.settings.gigyaGM, function(key, block) {
        $.each(block, function(name, params) {
          switch(name) {
          case 'challengeStatusParams':
            gigya.gm.showChallengeStatusUI(params);
            break;
          case 'userStatusParams':
            gigya.gm.showUserStatusUI(params);
            break;
          case 'achievementsParams':
            gigya.gm.showAchievementsUI(params);
            break;
          case 'leaderboardParams':
            gigya.gm.showLeaderboardUI(params);
            break;
          }
        });
      });
    }
    /**
     * this function checks if the user is looged in at gigya if so it renders the Gamification notification Plugin
     */
    Drupal.gigya.gmNotiInit = function (response) {
      if (response.errorCode === 0) {
        if (typeof response.UID !== 'undefined') {
          gigya.gm.showNotifications();
        }
      }
      return false;
    };


})(jQuery);

;
(function ($) {
    /**
     * @todo Undocumented Code!
     */
    Drupal.behaviors.gigyaNotifyFriends = {
      attach: function(context, settings) {
        if (typeof gigya !== 'undefined') {
          $('.friends-ui:not(.gigyaNotifyFriends-processed)', context).addClass('gigyaNotifyFriends-processed').each(
            function () {
              gigya.services.socialize.getUserInfo({callback:Drupal.gigya.notifyFriendsCallback});
              gigya.services.socialize.addEventHandlers({ onConnect:Drupal.gigya.notifyFriendsCallback, onDisconnect:Drupal.gigya.notifyFriendsCallback});
            }
          );
        }
      }
    };

    /**
     * @todo Undocumented Code!
     */
    Drupal.behaviors.gigyaInit = {
      attach: function(context, settings) {
        if (typeof gigya !== 'undefined') {
          if (typeof Drupal.settings.gigya.notifyLoginParams !== 'undefined') {
            Drupal.settings.gigya.notifyLoginParams.callback = Drupal.gigya.notifyLoginCallback;
            gigya.services.socialize.getUserInfo({callback: Drupal.gigya.initResponse});
          }

          // Attach event handlers.
          gigya.socialize.addEventHandlers({onLogin:Drupal.gigya.loginCallback});

          // Display LoginUI if necessary.
          if (typeof Drupal.settings.gigya.loginUIParams !== 'undefined') {
            $.each(Drupal.settings.gigya.loginUIParams, function (index, value) {
              value.context = {id: value.containerID};
              gigya.services.socialize.showLoginUI(value);
            });
          }

          // Display ConnectUI if necessary.
          if (typeof Drupal.settings.gigya.connectUIParams !== 'undefined') {
            gigya.services.socialize.showAddConnectionsUI(Drupal.settings.gigya.connectUIParams);
          }

          // Call ShareUI if it exists.
          if (typeof Drupal.settings.gigya.shareUIParams !== 'undefined') {
            //build a media object
            var mediaObj = {type: 'image', href: Drupal.settings.gigya.shareUIParams.linkBack};
            if ((Drupal.settings.gigya.shareUIParams.imageBhev === 'url') && (Drupal.settings.gigya.shareUIParams.imageUrl !== '')) {
              mediaObj.src = Drupal.settings.gigya.shareUIParams.imageUrl;
            }
            else if (Drupal.settings.gigya.shareUIParams.imageBhev === 'default') {
              if ($('meta[property=og:image]').length > 0) {
                mediaObj.src = $('meta[property=og:image]').attr('content');
              }
              else {
                mediaObj.src = $('#block-system-main img').eq(0).attr('src') || $('img').eq(0).attr('src');
              }
            }
            else {
              mediaObj.src = $('#block-system-main img').eq(0).attr('src') || $('img').eq(0).attr('src');
            }
            // Step 1: Construct a UserAction object and fill it with data.
            var ua = new gigya.services.socialize.UserAction();
            if (typeof Drupal.settings.gigya.shareUIParams.linkBack !== 'undefined') {
              ua.setLinkBack(Drupal.settings.gigya.shareUIParams.linkBack);
            }
            if (typeof Drupal.settings.gigya.shareUIParams.title !== 'undefined') {
              ua.setTitle(Drupal.settings.gigya.shareUIParams.title);
            }
            if (typeof Drupal.settings.gigya.shareUIParams.description !== 'undefined') {
              ua.setDescription(Drupal.settings.gigya.shareUIParams.description);
            }
            ua.addMediaItem(mediaObj);
            var params = {};
            if (typeof Drupal.settings.gigya.shareUIParams.extraParams !== 'undefined') {
              params = Drupal.settings.gigya.shareUIParams.extraParams;
            }
            params.userAction = ua;
            gigya.services.socialize.showShareUI(params);
          }
        }
      }
    };

    /**
     * @todo Undocumented Code!
     */
    Drupal.behaviors.gigyaLogut = {
      attach: function (context, settings) {
        if (typeof gigya !== 'undefined') {
          if ($.cookie('Drupal.visitor.gigya') == 'gigyaLogOut') {
            gigya.services.socialize.logout();
            $.cookie('Drupal.visitor.gigya', null);
          }
        }
      }
    };
    /**
     * initiate Gamification
     */

    Drupal.behaviors.gamification = {
      attach: function (context, settings) {
        if (typeof gigya !== 'undefined') {
          if (typeof Drupal.settings.gigyaGM !== 'undefined'){
            gigya.services.socialize.getUserInfo({callback: Drupal.gigya.gmInit});
          }
        }
      }
    };
    Drupal.behaviors.GMnotifications = {
      attach: function (context, settings) {
        if (typeof gigya !== 'undefined') {
          if (typeof Drupal.settings.gigyaGMnotification !== 'undefined'){
            gigya.services.socialize.getUserInfo({callback: Drupal.gigya.gmNotiInit});
          }
        }
      }
    };
    /**
     * initiate Activity Feed
     */
    Drupal.behaviors.gigyaActivityFeed = {
      attach: function (context, settings) {
        if (typeof gigya !== 'undefined') {
          if (typeof Drupal.settings.gigyaActivityFeed !== 'undefined'){
            gigya.socialize.showFeedUI(Drupal.settings.gigyaActivityFeed);
          }
        }
      }
    };
    Drupal.behaviors.addGigyaUidToForm = {
      attach: function (context, settings) {
        if (typeof gigya !== 'undefined') {
          if ($('#gigya-link-accounts-form .gigyaUid').length > 0 ) {
            $('#gigya-link-accounts-form .gigyaUid').val(Drupal.gigya.toPost.user.UID);
          }
          if ($('#modal-content form .gigyaUid').length > 0) {
            $('#modal-content form .gigyaUid').val(Drupal.gigya.toPost.user.UID);
          }
          $('#modal-content form .close-modal').once().click( function (e) {
            e.preventDefault();
            Drupal.CTools.Modal.dismiss();
          });
        }
      }
    };
    Drupal.behaviors.gigyaFillRegForm = {
      attach: function (context, settings) {
        if (typeof gigya !== 'undefined') {
          if ($('.modal-content #user-register-form').length > 0 ) {
            $('.modal-content #user-register-form input:[name=mail]').val(Drupal.gigya.toPost.user.email);
            $('.modal-content #user-register-form input:[name=name]').val(Drupal.gigya.toPost.user.email);
          }
        }
      }
    };



})(jQuery);

;
(function ($) {

/**
 * A progressbar object. Initialized with the given id. Must be inserted into
 * the DOM afterwards through progressBar.element.
 *
 * method is the function which will perform the HTTP request to get the
 * progress bar state. Either "GET" or "POST".
 *
 * e.g. pb = new progressBar('myProgressBar');
 *      some_element.appendChild(pb.element);
 */
Drupal.progressBar = function (id, updateCallback, method, errorCallback) {
  var pb = this;
  this.id = id;
  this.method = method || 'GET';
  this.updateCallback = updateCallback;
  this.errorCallback = errorCallback;

  // The WAI-ARIA setting aria-live="polite" will announce changes after users
  // have completed their current activity and not interrupt the screen reader.
  this.element = $('<div class="progress" aria-live="polite"></div>').attr('id', id);
  this.element.html('<div class="bar"><div class="filled"></div></div>' +
                    '<div class="percentage"></div>' +
                    '<div class="message">&nbsp;</div>');
};

/**
 * Set the percentage and status message for the progressbar.
 */
Drupal.progressBar.prototype.setProgress = function (percentage, message) {
  if (percentage >= 0 && percentage <= 100) {
    $('div.filled', this.element).css('width', percentage + '%');
    $('div.percentage', this.element).html(percentage + '%');
  }
  $('div.message', this.element).html(message);
  if (this.updateCallback) {
    this.updateCallback(percentage, message, this);
  }
};

/**
 * Start monitoring progress via Ajax.
 */
Drupal.progressBar.prototype.startMonitoring = function (uri, delay) {
  this.delay = delay;
  this.uri = uri;
  this.sendPing();
};

/**
 * Stop monitoring progress via Ajax.
 */
Drupal.progressBar.prototype.stopMonitoring = function () {
  clearTimeout(this.timer);
  // This allows monitoring to be stopped from within the callback.
  this.uri = null;
};

/**
 * Request progress data from server.
 */
Drupal.progressBar.prototype.sendPing = function () {
  if (this.timer) {
    clearTimeout(this.timer);
  }
  if (this.uri) {
    var pb = this;
    // When doing a post request, you need non-null data. Otherwise a
    // HTTP 411 or HTTP 406 (with Apache mod_security) error may result.
    $.ajax({
      type: this.method,
      url: this.uri,
      data: '',
      dataType: 'json',
      success: function (progress) {
        // Display errors.
        if (progress.status == 0) {
          pb.displayError(progress.data);
          return;
        }
        // Update display.
        pb.setProgress(progress.percentage, progress.message);
        // Schedule next timer.
        pb.timer = setTimeout(function () { pb.sendPing(); }, pb.delay);
      },
      error: function (xmlhttp) {
        pb.displayError(Drupal.ajaxError(xmlhttp, pb.uri));
      }
    });
  }
};

/**
 * Display errors on the page.
 */
Drupal.progressBar.prototype.displayError = function (string) {
  var error = $('<div class="messages error"></div>').html(string);
  $(this.element).before(error).hide();

  if (this.errorCallback) {
    this.errorCallback(this);
  }
};

})(jQuery);
;
