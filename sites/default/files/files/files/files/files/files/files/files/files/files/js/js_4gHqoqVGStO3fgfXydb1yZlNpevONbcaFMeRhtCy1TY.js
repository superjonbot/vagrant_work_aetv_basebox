(function ($) {

/**
 * Toggle the visibility of a fieldset using smooth animations.
 */
Drupal.toggleFieldset = function (fieldset) {
  var $fieldset = $(fieldset);
  if ($fieldset.is('.collapsed')) {
    var $content = $('> .fieldset-wrapper', fieldset).hide();
    $fieldset
      .removeClass('collapsed')
      .trigger({ type: 'collapsed', value: false })
      .find('> legend span.fieldset-legend-prefix').html(Drupal.t('Hide'));
    $content.slideDown({
      duration: 'fast',
      easing: 'linear',
      complete: function () {
        Drupal.collapseScrollIntoView(fieldset);
        fieldset.animating = false;
      },
      step: function () {
        // Scroll the fieldset into view.
        Drupal.collapseScrollIntoView(fieldset);
      }
    });
  }
  else {
    $fieldset.trigger({ type: 'collapsed', value: true });
    $('> .fieldset-wrapper', fieldset).slideUp('fast', function () {
      $fieldset
        .addClass('collapsed')
        .find('> legend span.fieldset-legend-prefix').html(Drupal.t('Show'));
      fieldset.animating = false;
    });
  }
};

/**
 * Scroll a given fieldset into view as much as possible.
 */
Drupal.collapseScrollIntoView = function (node) {
  var h = document.documentElement.clientHeight || document.body.clientHeight || 0;
  var offset = document.documentElement.scrollTop || document.body.scrollTop || 0;
  var posY = $(node).offset().top;
  var fudge = 55;
  if (posY + node.offsetHeight + fudge > h + offset) {
    if (node.offsetHeight > h) {
      window.scrollTo(0, posY);
    }
    else {
      window.scrollTo(0, posY + node.offsetHeight - h + fudge);
    }
  }
};

Drupal.behaviors.collapse = {
  attach: function (context, settings) {
    $('fieldset.collapsible', context).once('collapse', function () {
      var $fieldset = $(this);
      // Expand fieldset if there are errors inside, or if it contains an
      // element that is targeted by the URI fragment identifier.
      var anchor = location.hash && location.hash != '#' ? ', ' + location.hash : '';
      if ($fieldset.find('.error' + anchor).length) {
        $fieldset.removeClass('collapsed');
      }

      var summary = $('<span class="summary"></span>');
      $fieldset.
        bind('summaryUpdated', function () {
          var text = $.trim($fieldset.drupalGetSummary());
          summary.html(text ? ' (' + text + ')' : '');
        })
        .trigger('summaryUpdated');

      // Turn the legend into a clickable link, but retain span.fieldset-legend
      // for CSS positioning.
      var $legend = $('> legend .fieldset-legend', this);

      $('<span class="fieldset-legend-prefix element-invisible"></span>')
        .append($fieldset.hasClass('collapsed') ? Drupal.t('Show') : Drupal.t('Hide'))
        .prependTo($legend)
        .after(' ');

      // .wrapInner() does not retain bound events.
      var $link = $('<a class="fieldset-title" href="#"></a>')
        .prepend($legend.contents())
        .appendTo($legend)
        .click(function () {
          var fieldset = $fieldset.get(0);
          // Don't animate multiple times.
          if (!fieldset.animating) {
            fieldset.animating = true;
            Drupal.toggleFieldset(fieldset);
          }
          return false;
        });

      $legend.append(summary);
    });
  }
};

})(jQuery);
;
(function ($) {

Drupal.behaviors.dfpAdminColors = {
  attach: function (context) {

    var reg = /^(#)?([0-9a-fA-F]{3})([0-9a-fA-F]{3})?$/;
    $('#color-settings .color-setting', context).bind('change keyup', function (context) {
      hexcolor = reg.test($(this).val()) ? '#' + $(this).val() : 'transparent';
      $(this).closest('tr').find('.color-sample').css('background-color', hexcolor);
    });
  }
};

/**
 * Custom summary for the module vertical tab.
 */
Drupal.behaviors.dfpVerticalTabs = {
  attach: function (context) {

    var checkmark = '&#10003;';
    var exmark = '&#10008;';

    $('fieldset#edit-global-tag-settings', context).drupalSetSummary(function (context) {
      var networkId = Drupal.checkPlain($('#edit-dfp-network-id', context).val());
      var adUnit = Drupal.checkPlain($('#edit-dfp-default-adunit', context).val());
      var async = Drupal.checkPlain($('#edit-dfp-async-rendering', context).is(':checked'));
      var single = Drupal.checkPlain($('#edit-dfp-single-request', context).is(':checked'));

      summary = 'Network Id: ' + networkId + '<br/>';
      summary += (async == "true" ? checkmark : exmark) + ' Load ads asyncronously' + '<br/>';
      summary += (single == "true" ? checkmark : exmark) + ' Use a single request';

      return summary;
    });

    $('fieldset#edit-global-display-options', context).drupalSetSummary(function (context) {
      var slug = Drupal.checkPlain($('#edit-dfp-default-slug', context).val());
      var noscript = Drupal.checkPlain($('#edit-dfp-use-noscript', context).is(':checked'));
      var collapse = Drupal.checkPlain($('input[name="dfp_collapse_empty_divs"]:checked', context).val());

      summary = 'Global Slug: ' + slug + '<br/>';
      switch (collapse) {
        case '0':
          summary += exmark + ' Never collapse empty divs';
          break;
        case '1':
          summary += checkmark + ' Collapse divs only if empty';
          break;
        case '2':
          summary += checkmark + ' Expand divs only if non-empty';
          break;
      }

      return summary;
    });

    $('fieldset#edit-backfill-settings', context).drupalSetSummary(function (context) {
      var adType = Drupal.checkPlain($('#edit-settings-adsense-ad-types option:selected', context).text());
      var adTypeVal = Drupal.checkPlain($('#edit-settings-adsense-ad-types', context).val());

      summary = adTypeVal !== '' ? Drupal.t('Ad Type: ') + adType : '';
      return summary;
    });

    $('fieldset#edit-tag-settings', context).drupalSetSummary(function (context) {
      var slot = Drupal.checkPlain($('#edit-slot', context).val());
      var size = Drupal.checkPlain($('#edit-size', context).val());

      summary = slot !== '' ?  slot  + ' [' + size + ']' : '';
      return summary;
    });

    $('fieldset#edit-tag-display-options', context).drupalSetSummary(function (context) {
      summary = Drupal.t('Configure how the ad will be displayed');
      return summary;
    });

    $('fieldset#edit-breakpoint-settings', context).drupalSetSummary(function (context) {
      var summary = 'Configure DFP mappings.';
      return summary;
    });

    $('fieldset#edit-targeting-settings', context).drupalSetSummary(function (context) {
      var summary = '';
      $('.field-target-target', context).each(function (context) {
        target = Drupal.checkPlain($(this).val());
        if (target !== '') {
          value = Drupal.checkPlain($(this).closest('tr').find('.field-target-value').val());
          summary += target + ' = ' + value + '<br/>';
        }
      });
      return summary;
    });
  }
};

})(jQuery);
;
(function ($) {

/**
 * Attaches sticky table headers.
 */
Drupal.behaviors.tableHeader = {
  attach: function (context, settings) {
    if (!$.support.positionFixed) {
      return;
    }

    $('table.sticky-enabled', context).once('tableheader', function () {
      $(this).data("drupal-tableheader", new Drupal.tableHeader(this));
    });
  }
};

/**
 * Constructor for the tableHeader object. Provides sticky table headers.
 *
 * @param table
 *   DOM object for the table to add a sticky header to.
 */
Drupal.tableHeader = function (table) {
  var self = this;

  this.originalTable = $(table);
  this.originalHeader = $(table).children('thead');
  this.originalHeaderCells = this.originalHeader.find('> tr > th');
  this.displayWeight = null;

  // React to columns change to avoid making checks in the scroll callback.
  this.originalTable.bind('columnschange', function (e, display) {
    // This will force header size to be calculated on scroll.
    self.widthCalculated = (self.displayWeight !== null && self.displayWeight === display);
    self.displayWeight = display;
  });

  // Clone the table header so it inherits original jQuery properties. Hide
  // the table to avoid a flash of the header clone upon page load.
  this.stickyTable = $('<table class="sticky-header"/>')
    .insertBefore(this.originalTable)
    .css({ position: 'fixed', top: '0px' });
  this.stickyHeader = this.originalHeader.clone(true)
    .hide()
    .appendTo(this.stickyTable);
  this.stickyHeaderCells = this.stickyHeader.find('> tr > th');

  this.originalTable.addClass('sticky-table');
  $(window)
    .bind('scroll.drupal-tableheader', $.proxy(this, 'eventhandlerRecalculateStickyHeader'))
    .bind('resize.drupal-tableheader', { calculateWidth: true }, $.proxy(this, 'eventhandlerRecalculateStickyHeader'))
    // Make sure the anchor being scrolled into view is not hidden beneath the
    // sticky table header. Adjust the scrollTop if it does.
    .bind('drupalDisplaceAnchor.drupal-tableheader', function () {
      window.scrollBy(0, -self.stickyTable.outerHeight());
    })
    // Make sure the element being focused is not hidden beneath the sticky
    // table header. Adjust the scrollTop if it does.
    .bind('drupalDisplaceFocus.drupal-tableheader', function (event) {
      if (self.stickyVisible && event.clientY < (self.stickyOffsetTop + self.stickyTable.outerHeight()) && event.$target.closest('sticky-header').length === 0) {
        window.scrollBy(0, -self.stickyTable.outerHeight());
      }
    })
    .triggerHandler('resize.drupal-tableheader');

  // We hid the header to avoid it showing up erroneously on page load;
  // we need to unhide it now so that it will show up when expected.
  this.stickyHeader.show();
};

/**
 * Event handler: recalculates position of the sticky table header.
 *
 * @param event
 *   Event being triggered.
 */
Drupal.tableHeader.prototype.eventhandlerRecalculateStickyHeader = function (event) {
  var self = this;
  var calculateWidth = event.data && event.data.calculateWidth;

  // Reset top position of sticky table headers to the current top offset.
  this.stickyOffsetTop = Drupal.settings.tableHeaderOffset ? eval(Drupal.settings.tableHeaderOffset + '()') : 0;
  this.stickyTable.css('top', this.stickyOffsetTop + 'px');

  // Save positioning data.
  var viewHeight = document.documentElement.scrollHeight || document.body.scrollHeight;
  if (calculateWidth || this.viewHeight !== viewHeight) {
    this.viewHeight = viewHeight;
    this.vPosition = this.originalTable.offset().top - 4 - this.stickyOffsetTop;
    this.hPosition = this.originalTable.offset().left;
    this.vLength = this.originalTable[0].clientHeight - 100;
    calculateWidth = true;
  }

  // Track horizontal positioning relative to the viewport and set visibility.
  var hScroll = document.documentElement.scrollLeft || document.body.scrollLeft;
  var vOffset = (document.documentElement.scrollTop || document.body.scrollTop) - this.vPosition;
  this.stickyVisible = vOffset > 0 && vOffset < this.vLength;
  this.stickyTable.css({ left: (-hScroll + this.hPosition) + 'px', visibility: this.stickyVisible ? 'visible' : 'hidden' });

  // Only perform expensive calculations if the sticky header is actually
  // visible or when forced.
  if (this.stickyVisible && (calculateWidth || !this.widthCalculated)) {
    this.widthCalculated = true;
    var $that = null;
    var $stickyCell = null;
    var display = null;
    var cellWidth = null;
    // Resize header and its cell widths.
    // Only apply width to visible table cells. This prevents the header from
    // displaying incorrectly when the sticky header is no longer visible.
    for (var i = 0, il = this.originalHeaderCells.length; i < il; i += 1) {
      $that = $(this.originalHeaderCells[i]);
      $stickyCell = this.stickyHeaderCells.eq($that.index());
      display = $that.css('display');
      if (display !== 'none') {
        cellWidth = $that.css('width');
        // Exception for IE7.
        if (cellWidth === 'auto') {
          cellWidth = $that[0].clientWidth + 'px';
        }
        $stickyCell.css({'width': cellWidth, 'display': display});
      }
      else {
        $stickyCell.css('display', 'none');
      }
    }
    this.stickyTable.css('width', this.originalTable.outerWidth());
  }
};

})(jQuery);
;
(function ($) {

Drupal.toolbar = Drupal.toolbar || {};

/**
 * Attach toggling behavior and notify the overlay of the toolbar.
 */
Drupal.behaviors.toolbar = {
  attach: function(context) {

    // Set the initial state of the toolbar.
    $('#toolbar', context).once('toolbar', Drupal.toolbar.init);

    // Toggling toolbar drawer.
    $('#toolbar a.toggle', context).once('toolbar-toggle').click(function(e) {
      Drupal.toolbar.toggle();
      // Allow resize event handlers to recalculate sizes/positions.
      $(window).triggerHandler('resize');
      return false;
    });
  }
};

/**
 * Retrieve last saved cookie settings and set up the initial toolbar state.
 */
Drupal.toolbar.init = function() {
  // Retrieve the collapsed status from a stored cookie.
  var collapsed = $.cookie('Drupal.toolbar.collapsed');

  // Expand or collapse the toolbar based on the cookie value.
  if (collapsed == 1) {
    Drupal.toolbar.collapse();
  }
  else {
    Drupal.toolbar.expand();
  }
};

/**
 * Collapse the toolbar.
 */
Drupal.toolbar.collapse = function() {
  var toggle_text = Drupal.t('Show shortcuts');
  $('#toolbar div.toolbar-drawer').addClass('collapsed');
  $('#toolbar a.toggle')
    .removeClass('toggle-active')
    .attr('title',  toggle_text)
    .html(toggle_text);
  $('body').removeClass('toolbar-drawer').css('paddingTop', Drupal.toolbar.height());
  $.cookie(
    'Drupal.toolbar.collapsed',
    1,
    {
      path: Drupal.settings.basePath,
      // The cookie should "never" expire.
      expires: 36500
    }
  );
};

/**
 * Expand the toolbar.
 */
Drupal.toolbar.expand = function() {
  var toggle_text = Drupal.t('Hide shortcuts');
  $('#toolbar div.toolbar-drawer').removeClass('collapsed');
  $('#toolbar a.toggle')
    .addClass('toggle-active')
    .attr('title',  toggle_text)
    .html(toggle_text);
  $('body').addClass('toolbar-drawer').css('paddingTop', Drupal.toolbar.height());
  $.cookie(
    'Drupal.toolbar.collapsed',
    0,
    {
      path: Drupal.settings.basePath,
      // The cookie should "never" expire.
      expires: 36500
    }
  );
};

/**
 * Toggle the toolbar.
 */
Drupal.toolbar.toggle = function() {
  if ($('#toolbar div.toolbar-drawer').hasClass('collapsed')) {
    Drupal.toolbar.expand();
  }
  else {
    Drupal.toolbar.collapse();
  }
};

Drupal.toolbar.height = function() {
  var $toolbar = $('#toolbar');
  var height = $toolbar.outerHeight();
  // In modern browsers (including IE9), when box-shadow is defined, use the
  // normal height.
  var cssBoxShadowValue = $toolbar.css('box-shadow');
  var boxShadow = (typeof cssBoxShadowValue !== 'undefined' && cssBoxShadowValue !== 'none');
  // In IE8 and below, we use the shadow filter to apply box-shadow styles to
  // the toolbar. It adds some extra height that we need to remove.
  if (!boxShadow && /DXImageTransform\.Microsoft\.Shadow/.test($toolbar.css('filter'))) {
    height -= $toolbar[0].filters.item("DXImageTransform.Microsoft.Shadow").strength;
  }
  return height;
};

})(jQuery);
;
/*
 * Google CSE glue module. Uses cse js API
 *
 */
var aetv = aetv || {};

aetv.googleSearch2 = (function (module, $, LAB) {

  var debugMode,
      currentlySearching,
      searchOpen,
      searchWrapper,
      searchButton,
      searchArrow,
      searchResumeButton,
      gso,
      clearButton,
      resultsCloseButton,
      blackOut,
      socialNav;

  module.init = function () {
    LAB.script('http://www.google.com/jsapi').wait(function () {
      dlog('jsapi loaded');
      var loadOptions = {
        "callback": function() { dlog('search callback'); }
      };
      google.load('search', '1', loadOptions);
      google.setOnLoadCallback(function () {
        dlog('set on load callback');

        dlog('google cse js version 2 loaded');
        debugMode = true;
        searchOpen = false;
        currentlySearching = false;
        setVars();
        bindControls('onload');
        createWidget();

        //create a structure that I can manipulate by wrapping the box in MY div
        $('.gssb_c').wrap('<div class="aetv-cse-wrapper"></div>');
        $('#search-wrapper').appendTo('.aetv-cse-wrapper');
        $('<div class="search-icon"></div>').prependTo('td.gsc-input');
        $('input.gsc-input').attr('placeholder','Search');
        //$('.aetv-cse-wrapper').appendTo('#wrapper');

        checkForQuery();

        //$(window).click(function (e) { dlog('searchOpen='+searchOpen+':'+e.srcElement); });
      });
    });
  }

  function setVars() {
    searchButton = $('#google-search .search'),
    searchResumeButton = $('#google-search .search-resume'),
    searchWrapper = $('#google-search .search-wrapper'),
    searchArrow = $('.search-arrow'),
    clearButton = $('.gsc-clear-button'),
    blackOut = '<div class="gsc-modal-background-image gsc-modal-background-image-visible" tabindex="0"></div>', 
    resultsCloseButton = '<div class="gsc-results-close-btn gsc-results-close-btn-visible" tabindex="0"></div>',
    searchWrapperMask = '<a class="search-wrapper-mask"></a>';
    socialNav = $("#header_social");
  }

  function searchComplete() {
    var acBox = $('.gsc-results-wrapper-nooverlay');
    acBox.show();
    acBox.css('border', '3px solid #ccc');
//    showArrow();

    var term = gso.getInputQuery();
    if (term != "") {
      saveSearchTerm(term);
    }
    showResultHeader(term);

    //add in the results close button
    if ($('.gsc-results-close-btn').length < 1) {
      $(resultsCloseButton).appendTo('.gsc-results-wrapper-nooverlay');
    }
    
    //add in thickbox overlay
    if ($('.gsc-modal-background-image').length < 1) {
      $(blackOut).insertAfter('.gsc-results-wrapper-nooverlay');
    }

    //mask the input field separately so we can target a click there
    if ($('.search-wrapper-mask').length < 1) {
      $(searchWrapperMask).insertAfter('.gsc-results-wrapper-nooverlay');
      bindControls('searchDisplayed');
    }

    //make all the links go to target _top
    $('.gsc-table-result a').each(
      function () { 
        if (typeof($(this).attr('target')) != 'undefined') { 
          $(this).attr('target', '_top'); 
        } 
      }
    );
  }

  function adjustResultsBoxHeight() {
    var adjHeight = Math.round($('#wrapper').height() * .80);
    $('.gsc-results-wrapper-nooverlay').css('height', adjHeight+'px');
    $('.gsc-wrapper').css('height', adjHeight+'px');
  }

  function searchStarting() {
    //dlog('search starting event');
    //decide how tall the result box will be
    adjustResultsBoxHeight();
  }

  function saveSearchTerm(term, src) {
    //dlog('SAVING '+term);
    $.cookie('gst', term, {'domain':document.location.hostname});
  };

  function hideArrow() {
    $('.search-arrow').hide();
  }

  function showArrow() {
    if ($('.gsc-results-wrapper-nooverlay').length > 0) {
      $('.search-arrow').show();
    }
  };

  function showResultHeader(term) {
    if ($('.gsc-result-info').length > 0) {
      term = 'Search Results for "'+term+'"';
      if ($('.gsc-search-result-hdr').length < 1) {
        $('<div class="gsc-search-result-hdr"><strong>'+term+'</strong></div>').insertBefore('.gsc-result-info');
      }
      else {
        $('.gsc-search-result-hdr strong').text(term);
      }
    }
  }

  function createWidget() {
  	dlog('attempting to create widget');
    var cseId = Drupal.settings.aetv_google_cse.googleSearchCseId;
    var drawOptions = new google.search.DrawOptions();
    drawOptions.setAutoComplete(true);
    gso = new google.search.CustomSearchControl(cseId, {});
    dlog(gso);

    gso.setSearchCompleteCallback({}, searchComplete);
    gso.setSearchStartingCallback({}, searchStarting);
    gso.draw('google-cse', drawOptions);
  }

  function bindControls(which) { 
    switch (which) {
      case 'onload':

          try {
              console.log('attempting to fix any isotope');
              setTimeout(function(){jQuery('.isotope').isotope( 'reloadItems' ).isotope();},0);
          }
          catch(err) {
              //no isotope
          }


        searchButton.click(function (e) {
          dlog('search button clicked');
          $('.aetv-cse-wrapper').addClass('open');
          // nasty fix for unclickable scroller buttons
          $('#header #header_tools').removeClass('search-closed');
          dlog('searchOpen='+searchOpen);
          if (!searchOpen) {
            openSearch();
          }
          else {
            closeSearch();
          }
          e.preventDefault();
          return false;
        });

        searchResumeButton.click(function (e) {
          dlog('search resume button clicked');
          // nasty fix for unclickable scroller buttons
          $('#header #header_tools').removeClass('search-closed');
          var term = getLastSearch();
          if (term) {
            openSearch(term);
            e.preventDefault();
          }
          return false;
        });

        $(window).resize(_.debounce(function () {
          adjustResultsBoxHeight();
        }, 500));
        break;

      case 'searchDisplayed':
        $('.gsc-modal-background-image').bind('click', function () {
          clearSearchResults();
        });

        $('.gsc-results-close-btn').bind('click', function () {
          clearSearchResults();
        });

        $('.search-wrapper-mask').bind('click', function () { 
          clearSearchResults(true);
        });
//        $('.aetv-cse-wrapper').css('z-index', 0);
        $('#google-search .search-wrapper').css('top','6px').css('right','50%');
        break; 
      
      case 'typingSearch':
        var searchInput = $('#gsc-i-id1');
        var clearButton = $('.gsc-clear-button');
        var acBox = $('.gsc-completion-container');

        //acBox.css('border', '1px solid #bbb');
        //acBox.addClass('gs-rounded-corners');

        //move the autocomplete box 
        searchInput.bind('keyup', function (e) {
          var acBox = $('.gsc-completion-container');
          var acRcCRows = $('.gssb_a');
          var adjWidth;
          currentlySearching = true;

          //$('.aetv-cse-wrapper').css('z-index', 1002);
//          $('.aetv-cse-wrapper').css('z-index', 9999);
//          $('#google-search .search-wrapper').css('top','75px').css('right','10px');

          dlog('currentlySearching = true');
          dlog('acBox width='+acRcCRows.width());
//          if (acRcCRows.width()) {
//            adjWidth = acRcCRows.width() + 20;
//          }
//          else {
//            adjWidth = 100;
//          }
          dlog('adjusted width = '+adjWidth);
//          var styles = "width: "+adjWidth+"px !important; display: block; left:auto; right:10px; position: absolute;"
//          $('.gssb_c').get(0).setAttribute('style', "");
          //$('.gssb_c').get(0).setAttribute('style', styles);

//          $('.gssb_c').addClass('gs-rounded-corners');

//          acBox.css('width', adjWidth+'px');

          focusField();
        });

        acBox.bind('mouseleave', function () {
//          $('.aetv-cse-wrapper').css('z-index', -1);
          currentlySearching = false;
        });

//        searchWrapper.bind('mouseleave', function () {
//          if (!currentlySearching && searchOpen || searchInput.val() == "") {
//            closeSearch();
//          }
//        });
        clearButton.bind('click touch', function() {
		  closeSearch();
		});
        break;
    }

    dlog('controls loaded for '+which);
  };

  function clearSearchResults(keepSearching) {
    //dlog('background clicked!');
    $('.gsc-results-wrapper-nooverlay').hide();
    $('.gsc-modal-background-image').remove();
    hideArrow();
    gso.clearAllResults();

    if (!keepSearching) {
      currentlySearching = false;
      closeSearch();
    }
    else {
      currentlySearching = true;
      focusField();
    }
    $('.search-wrapper-mask').unbind('click');
    $('.search-wrapper-mask').remove();
    $('#google-search .search-wrapper').css('top','75px').css('right','10px');
  }

  function showResumeSearch() {
    dlog('show resume search button');
    searchButton.css('left', '-27px');
    searchWrapper.hide();
    searchResumeButton.show();
//    $('.aetv-cse-wrapper').css('z-index', 0);
  };

  function getLastSearch() {
    if ($.cookie('gst') != null) {
      return $.cookie('gst');
    }
    else {
      return false;
    }
  };

  function termIsSaved() {
    return getLastSearch();
  };

  function checkForQuery() {
    var termFound = termIsSaved();
    dlog('termfound='+termFound);
    if (termFound) {
      showResumeSearch();
    }
  };

 function focusField() {
    var searchInput = $('#gsc-i-id1');
    searchInput.focus();
  };

  function executeSearch(term) {
    if (typeof(term) != 'undefined') {
      dlog('term was provided. it is: '+term);
      currentlySearching = true;
      gso.prefillQuery(term);
      focusField();
      dlog('attempting to trigger search');
      gso.execute(term, 1, '');
      searchOpen = true;
    }
  };

  function openSearch(term) {
    dlog('open search. term = '+term);
    var searchInput = $('#gsc-i-id1');
    var clearButton = $('div.gsc-clear-button');
//    clearButton.hide();
	
    searchOpen = true;

    searchInput.hide();
    searchWrapper.show();
    searchResumeButton.hide();
    //searchResumeButton.css('left', '4px'); 
    
    //socialNav.removeClass('open');
    searchWrapper.animate(
      {width: '500px'}, 
      1000, 
      function () {
        //searchInput.css('width','180px');
        searchInput.show();
//        clearButton.fadeIn(1000, function () {
//          $(this).css('display','inline');
//        });

        bindControls('typingSearch');

//        $('.aetv-cse-wrapper').css('z-index', 0);

        if (term) {
          executeSearch(term); 
        }
        else {
          focusField();
        }
      }
    );
  };

  function closeSearch() {
    dlog('close search');
    // nasty fix for unclickable scroller buttons
//    $('#header #header_tools').addClass('search-closed');
    var btnWidth = '0';
    var searchInput = $('#gsc-i-id1');
    var clearButton = $('.gsc-clear-button');
    currentlySearching = false;

//    $('.aetv-cse-wrapper').css('z-index', -1);
	$('.aetv-cse-wrapper').removeClass('open');

//    clearButton.hide();
    if (!_.isUndefined(gso) && gso.getInputQuery() != "") {
      gso.clearAllResults();
    }
    searchOpen = false;
    searchInput.unbind('keypress');
    clearButton.unbind('click');

    if (!_.isUndefined(searchWrapper)) {
      searchWrapper.unbind('mouseleave');
    }
    searchInput.hide();

    if (termIsSaved()) {
      btnWidth = '27';
    }
    else {
      btnWidth = '1';
    }

    if (!_.isUndefined(searchWrapper)) {
      searchWrapper.animate(
        //{ width : btnWidth+'px' },
        //1000,
        function () {
          checkForQuery();
        }
      );
    }
  };

  function dlog(msg) {
    if (typeof(console) != 'undefined' && debugMode) {
      console.log('CSE: '+msg);
    }
  };
  
  module.CloseSearchBar = function(){
	  closeSearch();
  };

  return module;

}(aetv.googleSearch2 || {}, jQuery, $LAB));

(function () {
  aetv.googleSearch2.init();
})();
;
