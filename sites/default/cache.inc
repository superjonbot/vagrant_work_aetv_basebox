<?php
# Default cache'ing configuration.

$memcache_key_prefix = '';

/*
 * This should read the mobile header info from AKAMAI, not do a local user-agent check.
 * If the AKA-DEVICE is not specified, it is assumed to be 'DESKTOP'
 */

// When running Drush, make sure it does not fail
if (function_exists('getallheaders')) {
  $request_headers = getallheaders();
}

// Debug
// error_log("From " .basename(__FILE__). " line " .__LINE__. " \$request_headers['AKA-DEVICE']: " .  print_r($request_headers['AKA-DEVICE'], 1));

if (isset($request_headers['AKA-DEVICE']) && strtoupper($request_headers['AKA-DEVICE']) == 'MOBILE') {
  $memcache_key_prefix .= 'ph-';  // Memcache will also pad it at the right side with '-'
  // Add it to response header
  drupal_add_http_header('Device-Type', $request_headers['AKA-DEVICE']);
} elseif (isset($request_headers['AKA-DEVICE']) && strtoupper($request_headers['AKA-DEVICE']) == 'TABLET') {
  $memcache_key_prefix .= 'tabl-';  // Memcache will also pad it at the right side with '-'
  // Add it to response header
  drupal_add_http_header('Device-Type', $request_headers['AKA-DEVICE']);
}


/*
 * Check for retina 'DISPLAY-TYPE' cookie which is set on javascript front end code.
 * Check for 'cache_key_prefix' cookie
 *
 */
/*
if (isset($_COOKIE['DISPLAY-TYPE']) && $_COOKIE['DISPLAY-TYPE'] == 'RETINA') {
  // For Debugging, output next line as a response header
  drupal_add_http_header('Resolution-Type', 'Retina');
  $memcache_key_prefix .= 'ret-';   // Memcache will also pad it at the right side with '-'
}
*/
if (isset($_COOKIE['cache_key_prefix'])) {
  $cache_key_prefix = $_COOKIE['cache_key_prefix'];
  // For Debugging, output next line as a response header
  drupal_add_http_header('Cache-Key-Prefix', $cache_key_prefix);
  $memcache_key_prefix .= $cache_key_prefix;
}

/*
 * Check for the Canada header
 *
 */
if (isset($request_headers['AKA-COUNTRY']) && strtoupper($request_headers['AKA-COUNTRY']) == 'CA') {
  $memcache_key_prefix .= 'ca-';
  // Add it to response header
  drupal_add_http_header('Country-Code', $request_headers['AKA-COUNTRY']);
}

// Remove extra '-' at right. Memcache will pad it at the right side with '-'
$memcache_key_prefix = rtrim($memcache_key_prefix, '-');

// debug
// error_log("From " .basename(__FILE__). " line " .__LINE__. ":  \$memcache_key_prefix:  " .  print_r($memcache_key_prefix, 1));


$conf['memcache_key_prefix'] = $memcache_key_prefix;

$conf['cache_backends'][] = 'profiles/aetv/modules/contrib/memcache/memcache.inc';
$conf['session_inc']      = 'profiles/aetv/modules/contrib/memcache/unstable/memcache-session.inc';
$conf['cache_class_cache_form'] = 'DrupalDatabaseCache';

$conf['cache_default_class'] = 'MemCacheDrupal';

// $conf['memcache_bins'] = array(
//     'cache'         => 'default',
//     'cache_page'    => 'cluster4',
//     'cache_block'   => 'cluster4',
//     'cache_lock'    => 'cluster3',
//     'session'       => 'cluster3',
//     'users'         => 'cluster3',
//     'cache_menu'    => 'cluster2',
//     'cache_form'    => 'cluster2',
//     'cache_node'    => 'cluster2',
//   );

$conf['memcache_bins'] = array(
    'cache'         => 'default',
    'cache_page'    => 'cluster2',
    'cache_block'   => 'cluster3',
    'cache_lock'    => 'cluster4',
    'session'       => 'cluster5',
    'users'         => 'cluster6',
    'cache_menu'    => 'cluster7',
    'cache_form'    => 'cluster8',
    'cache_node'    => 'cluster9',
  );
